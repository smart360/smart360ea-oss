/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.webtests.poc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;


/**
 *
 */
public class SeleniumAssertions extends SeleniumUtils {

  //private final static Pattern IDMATCHER = Pattern.compile("\\([0-9]+\\)$");

  /**
   * Default constructor.
   * @param driv
   */
  public SeleniumAssertions(WebDriver driv) {
    super(driv);
  }

  /**
   * Compares the imput-String with the String gotten from the PageTitle.
   * When calling this method please remember to KEEP THE ID in the "expected"-String
   * ( !! assertPageTitle("ABC (123) XY) will be true, even if the pageTitle is "ABC (456) XY",
   * because the number in round brackets will be assumed to be an ID and ignored
   * (since IDs change with every new inizialisation of the DB) )
   *  
   */
  public void assertPageTitle(String expected) {
    String actual = driver.getTitle();
    assertTrue("Comparing \"" + actual + "\" to expected \"" + expected + "\" failed.", comparePageTitle(expected, actual));
  }

  //does have its problems with such titles as "InfoSystemXY (150) - iteraplan"
  //TODO get it to work with the " - iteraplan", " : Overview - iteraplan", etc at the end of the title
  /*public boolean comparePageTitleAlt(String expected, String gotten) {
    String expectedWithoutId = new String(expected.replaceAll(IDMATCHER.pattern(), ""));
    String gottenWithoutId = new String(gotten.replaceAll(IDMATCHER.pattern(), ""));
    return gottenWithoutId.equals(expectedWithoutId);
  } */

  /**
   * method to compare two titles, while ignoring the IDs.
   * When calling this method please remember to keep the ID in both Strings, even if they are different
   *( !! comparePageTitle("ABC (123) XY", "ABC (456) XY") will return true, because the number in round brackets will be assumed as an ID an ignored)
   * @param exp - the expected title
   * @param got - title gotten (e.g. with the getTitle() method)
   * @return - if the two titles are identical, after excluding individual IDs
   */
  public boolean comparePageTitle(String exp, String got) {
    String titleExpected;
    String titleGotten;
    String expectedBeforeBracket;
    String expectedMiddle;
    String expectedAfterBracket;
    String gottenBeforeBracket;
    String gottenMiddle;
    String gottenAfterBracket;
    String combinedExpected;
    String combinedGotten;

    titleExpected = exp;
    titleGotten = got;

    if (exp == null || got == null) {
      return false;
    }

    if (exp.isEmpty() || got.isEmpty()) {
      return false;
    }

    /*
     * To make sure, that only the ID-Part of the Name get's ignored while comparing,
     * the title must be cut around the last pair of brackets,
     * since the ID always get's displayed last (so "Hello(World) (16)" cut's the ID (16) and not "world") 
     */
    if (exp.contains("(") && exp.contains(")") && got.contains("(") && got.contains(")")) {
      //gets the index of the last appearance of "(" and ")" in both the expected and the gotten title
      int i = titleExpected.lastIndexOf('(');
      int j = titleExpected.lastIndexOf(')');
      int k = titleGotten.lastIndexOf('(');
      int l = titleGotten.lastIndexOf(')');

      if (i < j && k < l) {
        /*
         * only when the "(" appears before the ")",
         * so that e.g. ")101(" won't break the following code 
         */
        expectedBeforeBracket = titleExpected.substring(0, i);
        expectedMiddle = titleExpected.substring(i + 1, j);
        expectedAfterBracket = titleExpected.substring(j + 1);

        gottenBeforeBracket = titleGotten.substring(0, k);
        gottenMiddle = titleGotten.substring(k + 1, l);
        gottenAfterBracket = titleGotten.substring(l + 1);

        combinedExpected = expectedBeforeBracket + expectedAfterBracket;
        combinedGotten = gottenBeforeBracket + gottenAfterBracket;

        if (isInteger(expectedMiddle) && isInteger(gottenMiddle)) {
          /*
           * if the part between the brackets is an integer,
           * it's considered an ID and ignored, while the rest gets compared
           */
          return combinedExpected.equals(combinedGotten);
        }
        else {
          //if the string between the brackets is no integer it gets compared, too
          return (expectedMiddle.equals(gottenMiddle) && combinedExpected.equals(combinedGotten));
        }
      }
    }
    else {
      //compare everything, when no brackets are to be found in the string
      return exp.equals(got);
    }
    //if every other "if" fails, it just tries to compare both strings normally
    return exp.equals(got);
  }

  /**
   * checks if s is an Integer by trying to parse it to Int.
   * @param s
   * @return true if s was successfully paresed to Int.
   */
  private static boolean isInteger(String s) {
    try {
      Integer.parseInt(s);
    } catch (NumberFormatException e) {
      return false;
    }
    // only got here if we didn't return false
    return true;
  }

  /**
   * asserts that an element, identified by CSS, is present right now.
   * @param cssSelector
   */
  public void assertExistsByCss(String cssSelector) {
    assertTrue(isElementPresent(By.cssSelector(cssSelector)));
  }

  /**
   * asserts that an element, identified by XPath, is present right now.
   * @param xpathSelector
   */
  public void assertExistsByXpath(String xpathSelector) {
    assertTrue(isElementPresent(By.xpath(xpathSelector)));
  }

  /**
   * Asserts that an element with a specified name exists.
   * (calls the nameToXPath-method from SeleniumUtils and uses it with assertExitsByXpath)
   * @param nameSelector
   */
  public void assertExistsByName(String nameSelector) {
    assertExistsByXpath(nameToXpath(nameSelector));
  }

  /**
   * asserts that an element, identified by CSS, is not present right now.
   * Takes 5 seconds.
   * @param cssSelector
   */
  public void assertExistsNotByCss(String cssSelector) {
    assertFalse(isElementPresent(By.cssSelector(cssSelector)));
    //waiting for a exception to be thrown is probably not the best option here
  }

  /**
   * asserts that an element, identified by XPath, is not present right now.
   * Takes 5 seconds.
   * @param xpathSelector
   */
  public void assertExistsNotByXpath(String xpathSelector) {
    assertFalse(isElementPresent(By.xpath(xpathSelector)));
  }

  /**
   * Asserts that an element with a specified name is not present right now.
   * (calls the nameToXPath-method from SeleniumUtils and uses it with assertExitsNotByXpath)
   * @param nameSelector
   */
  public void assertExistsNotByName(String nameSelector) {
    assertExistsNotByXpath(nameToXpath(nameSelector));
  }

  /**
   * checks if element is present
   * @param by
   * @return false - if the element was not found within 5 seconds
   */
  public boolean isElementPresent(By by) {
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  /**
   * asserts if a String, identified by CSS, contains the expected String
   * @param cssSelector
   * @param expected
   */
  public void assertTextContainsByCss(String cssSelector, String expected) {
    String help = driver.findElement(By.cssSelector(cssSelector)).getText();
    assertTrue(help.contains(expected));
  }

  /**
   * asserts if a String, identified by XPath, contains the expected String
   * @param xpathSelector
   * @param expected
   */
  public void assertTextContainsByXpath(String xpathSelector, String expected) {
    String help = driver.findElement(By.xpath(xpathSelector)).getText();
    assertTrue(help.contains(expected));
  }

  /**
   * asserts if a String, identified by CSS, equals the expected String
   * @param cssSelector
   * @param expected
   */
  public void assertTextEqualsByCss(String cssSelector, String expected) {
    String help = driver.findElement(By.cssSelector(cssSelector)).getText();
    assertTrue(help.equals(expected));
  }

  /**
   * asserts if a String, identified by XPath, equals the expected String
   * @param xpathSelector
   * @param expected
   */
  public void assertTextEqualsByXpath(String xpathSelector, String expected) {
    String help = driver.findElement(By.xpath(xpathSelector)).getText();
    assertTrue(help.equals(expected));
  }

  /**
   * asserts if a String, identified by its ID, equals the expected String
   * @param idSelector
   * @param expected
   */
  public void assertTextEqualsById(String idSelector, String expected) {
    String help = driver.findElement(By.id(idSelector)).getText();
    assertTrue(help.equals(expected));
  }

}
