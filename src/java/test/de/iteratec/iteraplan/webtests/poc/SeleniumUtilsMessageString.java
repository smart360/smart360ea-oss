/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.webtests.poc;

import java.util.Locale;

import org.openqa.selenium.WebDriver;

import de.iteratec.iteraplan.common.MessageAccess;


/**
 *
 */
public class SeleniumUtilsMessageString extends SeleniumUtils {

  /**
   * Default constructor.
   * @param driv
   */
  public SeleniumUtilsMessageString(WebDriver driv) {
    super(driv);
  }

  /**
   * Needed before calling assertPageTitle when the expected string is to be called directly
   * from the .properties-file.
   * Only works for the german version right now
   * @param messageId .properties-ID of the desired message
   * @return German message as a string and with " - iteraplan" attached at the end
   */
  public String getGermanMessageString(String messageId) {
    return getLanguageMessageString(messageId, Locale.GERMAN);
    /* TODO get it to automatically choose the correct localization
     * UserContext.getCurrentLocale() doesn't work when not logged in
     */
  }

  /**
   * For calling assertPageTitle with English messages
   * @param messageId
   * @return English message as a string with " - iteraplan" attached at the end 
   */
  public String getEnglishMessageString(String messageId) {
    return getLanguageMessageString(messageId, Locale.ENGLISH);
  }

  /**
   * For assertPageTitle with different languages
   * @param messageId
   * @param local
   * @return message string in specified language and with " - iteraplan" attached at the end
   */
  public String getLanguageMessageString(String messageId, Locale locale) {
    String s = MessageAccess.getString(messageId, locale);
    return addGlobalApplicationName(s, locale);
  }

  /**
   * Only for BBs!
   * Adds the " : Overview" and the " - iteraplan" (example for a returned title: "Business Domains : Overview - iteraplan")
   * @param BbId - BuildingBlock messageID
   * @param local
   * @return message string for Building Blocks with " : Overview - iteraplan" attached
   */
  public String getLanguageBuildingBlockMessageString(String BbId, Locale locale) {
    String overviewAdded = (MessageAccess.getString(BbId, locale) + " : " + MessageAccess.getString("global.overview", locale));
    return addGlobalApplicationName(overviewAdded, locale);
  }

  /**
   * Only use with BBs!
   * @param BbId
   * @return German message string for specified BB
   */
  public String getGermanBuildingBlockMessageString(String BbId) {
    return getLanguageBuildingBlockMessageString(BbId, Locale.GERMAN);
  }

  /**
   * 
   * @param s
   * @param locale
   * @return the string with " - iteraplan" at the end
   */
  private String addGlobalApplicationName(String s, Locale locale) {
    return s + " - " + MessageAccess.getString("global.applicationname", locale);
  }

}
