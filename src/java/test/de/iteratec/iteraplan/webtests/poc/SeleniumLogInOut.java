/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.webtests.poc;

import org.openqa.selenium.WebDriver;


public class SeleniumLogInOut {


  private final SeleniumUtils      util;
  private final SeleniumAssertions utilAssert;
  private final WebDriver          driver;

  public SeleniumLogInOut(WebDriver driv) {
    driver = driv;
    util = new SeleniumUtils(driv);
    utilAssert = new SeleniumAssertions(driv);
  }

  /**
   *Default-login with "system" and "password" 
   */
  public void login() {
    login("system", "password");
  }

  /**
   * inserts the given username and password into the corresponding fields and clicks the
   * login button.
   * @param user
   * @param pw
   */
  public void login(String user, String pw) {
    util.clearById("j_username");
    util.clearById("j_password");
    util.sendKeysById("j_username", user);
    util.sendKeysById("j_password", pw);
    util.clickById("loginButtonCursor");

    if (!utilAssert.comparePageTitle("iteratec - iteraplan", driver.getTitle())) {
      switchLanguageToGerman();
    }
  }

  /**
   * logs the user out
   */
  public void logout() {
    util.clickByXpath("//li[@id='user']/a");
    util.clickById("menu.Logout");
    util.clickById("modalFooterOK");
  }

  /**
   * this method is called after the login to switch to the German Locale
   */
  private void switchLanguageToGerman() {
    util.clickByXpath("//a[contains(@href, '#language')]");
    util.clickByXpath("//a[contains(@href, \"javascript:changeLanguage('de'); \")]");
  }
}