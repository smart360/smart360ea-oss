/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.webtests.poc;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SeleniumUtils {

  protected final WebDriver        driver;
  private final WebDriverWait wait;


  public SeleniumUtils(WebDriver driv) {
    driver = driv;
    wait = new WebDriverWait(driv, 30);
  }

  /**
   * clears the selected field (does not work with buttons or anything not writable)
   * (use for deleting the standard-text from a textfield (e.g. "-") )
   * @param linkId
   */
  public void clearById(String linkId) {
    driver.findElement(By.id(linkId)).clear();
  }

  /**
   * clears the selected field (does not work with buttons or anything not writable)
   * (use for deleting the standard-text from a textfield (e.g. "-") )
   * @param linkName
   */
  public void clearByName(String linkName) {
    driver.findElement(By.name(linkName)).clear();
  }

  /**
   * clears the selected field (does not work with buttons or anything not writable)
   * (use for deleting the standard-text from a textfield (e.g. "-") )
   * @param xpath
   */
  public void clearByXpath(String xpath) {
    driver.findElement(By.xpath(xpath)).clear();
  }

  /**
   * clicks an element identified by a cssSelector (e.g. ok-buttons in pop-up-windows)
   * @param linkCss
   */
  public void clickByCss(String linkCss) {
    driver.findElement(By.cssSelector(linkCss)).click();
  }

  /**
   * clicks a link identified by its ID (e.g. the header-image)
   * @param linkId
   */
  public void clickById(String linkId) {
    driver.findElement(By.id(linkId)).click();
  }

  /**
   * clicks an item identified by its name.
   * @param linkName
   */
  public void clickByName(String linkName) {
    driver.findElement(By.name(linkName)).click();
  }

  /**
   * clicks a link identified by its text.
   * @param linkText
   */
  public void clickByText(String linkText) {
    driver.findElement(By.linkText(linkText)).click();
  }

  /**
   * clicks an item identified by XPath.
   * @param itemname
   */
  public void clickByXpath(String itemname) {
    if (itemname.startsWith("//") || itemname.charAt(0) == '(') {
      driver.findElement(By.xpath(itemname)).click();
    }
    else {
      driver.findElement(By.xpath("//a[@id='menu." + itemname + "']")).click();
    }
  }

  /**
   * clicks an object after transforming it's name to xpath and using clickByXpath
   * @param nameOfObject
   */
  public void clickByNameToXpath(String nameOfObject) {
    clickByXpath(nameToXpath(nameOfObject));
  }

  /**
   * sends the specified String to the given location 
   * @param linkId - where the text is supposed to be written
   * @param keys - what is supposed to be written
   */
  public void sendKeysById(String linkId, String keys) {
    driver.findElement(By.id(linkId)).sendKeys(keys);
  }

  /**
   * sends the String to the specified location 
   * @param linkName - where the text is supposed to be written
   * @param keys - what is supposed to be written
   */
  public void sendKeysByName(String linkName, String keys) {
    driver.findElement(By.name(linkName)).sendKeys(keys);
  }

  /**
   * sends the String to the specified location 
   * @param linkXpath - where the text is supposed to be written
   * @param keys - what is supposed to be written
   */
  public void sendKeysByXpath(String linkXpath, String keys) {
      driver.findElement(By.xpath(linkXpath)).sendKeys(keys);
  }

  /**
   * sends the keystroke (e.g. Keys.DOWN) to the specified location 
   * @param linkXpath - where the text is supposed to be written
   * @param key - Keys.[WantedKeystroke]
   */
  public void sendKeysByXPath(String linkXpath, Keys key) {
    driver.findElement(By.xpath(linkXpath)).sendKeys(key);
  }

  /**
   * clicks an menu-entry, identified by ID, from a menu-dropdown-list
   * @param menuname : the name of the menu
   * @param itemname : the ID of the menu-entry
   */
  public void selectFromMenuById(String menuname, String itemname) {
    clickByText(menuname);
    if (itemname.startsWith("menu.")) {
      clickById(itemname);
    }
    else {
      clickById("menu." + itemname);
    }
  }

  /**
   * clicks an menu-entry, identified by its linktext, from a menu-dropdown-list
   * @param menuname : name of the menu
   * @param itemname : linkText of the menu-entry
   */
  public void selectFromMenuByText(String menuname, String itemname) {
    clickByText(menuname);
    clickByText(itemname);
  }

  /**
  * clicks an menu-entry, identified by xpath, from a menu-dropdown-list
  * @param menuname : the name of the menu
  * @param itemname : the xpath of the menu-entry
  * (The itemname can be either the whole path-address ("//a[@id='menu.GraphicalReporting']")
  * or just the name of the menu-entry ("GraphicalReporting")
  */
  public void selectFromMenuByXpath(String menuname, String itemname) {
    clickByText(menuname);
    clickByXpath(itemname);
  }

  /**
   * selects the elementToSelect from the specified combobox and clicks the specified addButton
   * @param comboboxXpath
   * @param elementToSelect
   * @param addButtonXpath
   * @throws InterruptedException
   */
  public void selectFromCombobox(String comboboxXpath, String elementToSelect, String addButtonXpath) throws InterruptedException {
    selectFromCombobox(comboboxXpath, elementToSelect);
    synchronized (driver) {
      driver.wait(1000);
    }
    clickByXpath(addButtonXpath);
  }

  /**
   * selects the elementToSelect from the specified combobox
   * @param comboboxXpath
   * @param elementToSelect
   * @throws InterruptedException
   */
  public void selectFromCombobox(String comboboxXpath, String elementToSelect) throws InterruptedException {
    clearByXpath(comboboxXpath);
    clickByXpath(comboboxXpath);
    sendKeysByXpath(comboboxXpath, elementToSelect);
    synchronized (driver) {
      driver.wait(2000);
    }
    sendKeysByXPath(comboboxXpath, Keys.DOWN);
    clickById("ui-active-menuitem");
  }

  /**
   * for using when in need to select an element from a static dropdown
   * (e.g. to add to a list or selecting the BB-type for a bulk update)
   * @param dropdownId
   * @param textToSelect
   * @throws InterruptedException 
   */
  public void selectFromDropdown(String dropdownId, String textToSelect) {
    Select help = new Select(driver.findElement(By.id(dropdownId)));
    help.selectByVisibleText(textToSelect);
  }

  /**
   * checks if an instance of a BBT (e.g. an IS) is already there by searching for the name.
   * If the instance is found, it will be deleted.
   * Takes up to 5 seconds.
   * @param name
   */
  public void deleteEaDataObjectIfPresent(String name) {
    SeleniumAssertions utilAssert = new SeleniumAssertions(driver);
    if (utilAssert.isElementPresent(By.linkText(name))) {
      clickByText(name);
      clickByCss("a.btn.dropdown-toggle");
      clickById("transactionDelete");
      clickById("modalFooterOK");
    }
  }

  public void waitForElementToDisappear(String css) {
    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(css)));
  }

  /**
   * converts a given name to XPath-format (e.g. "name" becomes "//a[(text()='name')]" )
   * @param nameOfObject
   * @return XPath-version if the name
   */
  public String nameToXpath(String nameOfObject) {
    return "//a[(text()='" + nameOfObject + "')]";
  }

}