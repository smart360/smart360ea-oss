package de.iteratec.iteraplan.presentation;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

import com.google.common.collect.Lists;

public class LeanLdapUserDetailsServiceTest {

	public static final String URL = "ldap://192.168.178.17:389";
	public static final String BASE_DN = "dc=maxcrc,dc=com";
	public static final String USER_DN = "cn=Manager,dc=maxcrc,dc=com";
	public static final String PASSWORD = "secret";
	public static final String FOLLOW = "follow";
	
	
	
	private DefaultSpringSecurityContextSource ctxSource;
	private FilterBasedLdapUserSearch ldapSearch;
	
	@Before
	public void setUp() throws Exception {
		ctxSource = new DefaultSpringSecurityContextSource(Lists.newArrayList(URL), BASE_DN);
		ctxSource.setUserDn(USER_DN);
		ctxSource.setPassword(PASSWORD);
		ctxSource.setReferral(FOLLOW);
		ctxSource.afterPropertiesSet();
		
		ldapSearch = new FilterBasedLdapUserSearch("ou=people", "uid={0}", ctxSource);
		ldapSearch.setDerefLinkFlag(true);
	}
	
	@Test
	public void testContext() {
		Assert.assertNotNull(ctxSource);
		System.out.println("PATH: " +ctxSource.getBaseLdapPathAsString());
	}
	
	@Test
	public void testConnection() throws NamingException {
		DirContext dirContext = ctxSource.getReadWriteContext();
		Assert.assertNotNull(dirContext);		
	}
	
	@Test
	public void testSearch() {
		DirContextOperations ops = ldapSearch.searchForUser("markus.bauer");
		Assert.assertNotNull(ops);
		
		IteraplanLdapUserDetails.Essence ess = new IteraplanLdapUserDetails.Essence(ops);
		
		ess.setUsername("markus.bauer");		
		
		LdapUserDetails dets = ess.createUserDetails();
		Assert.assertNotNull(dets);
		
		
	}
	
	
	
}
