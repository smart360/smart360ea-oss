/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.derived.AbstractInstanceStore;
import de.iteratec.iteraplan.elasticeam.metamodel.PropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipEndExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.builtin.BuiltinPrimitiveProperty;
import de.iteratec.iteraplan.elasticeam.model.BindingSet;
import de.iteratec.iteraplan.elasticeam.model.InstanceExpression;
import de.iteratec.iteraplan.elasticeam.model.LinkExpression;
import de.iteratec.iteraplan.elasticeam.model.UniversalModelExpression;
import de.iteratec.iteraplan.elasticmi.model.Model;
import de.iteratec.iteraplan.elasticmi.model.ObjectExpression;


public class ModelWrapper
    extends
    AbstractInstanceStore<InstanceWrapper, SubstantialTypeWrapper, LinkWrapper, RelationshipTypeWrapper, PropertyExpressionWrapper<?>, RelationshipEndWrapper> {

  private final Model miModel;

  public ModelWrapper(MetamodelWrapper metamodel, Model miModel) {
    super(metamodel);
    this.miModel = miModel;
  }

  @Override
  protected Collection<InstanceExpression> findAllInstances(ElasticeamContext context, SubstantialTypeWrapper type) {
    Set<InstanceExpression> result = Sets.newHashSet();
    for (ObjectExpression oe : type.apply(miModel)) {
      result.add(new InstanceWrapper(type, oe, this));
    }
    return result;
  }

  @Override
  protected Collection<LinkExpression> findAllLinks(RelationshipTypeWrapper type) {
    Set<LinkExpression> result = Sets.newHashSet();
    for (ObjectExpression oe : type.apply(miModel)) {
      result.add(new LinkWrapper(type, oe, this));
    }
    return result;
  }

  @Override
  protected BindingSet findAll(ElasticeamContext ctx, RelationshipEndWrapper via) {
    BindingSet result = new BindingSet();
    result.setFromType(via.getHolder());
    result.setToType(via.getType());
    for (UniversalModelExpression from : findAll(ctx, via.getHolder())) {
      for (UniversalModelExpression to : from.getConnecteds(via)) {
        result.addBinding(from, to);
      }
    }
    return result;
  }

  private Collection<UniversalModelExpression> findAll(ElasticeamContext ctx, UniversalTypeExpression ute) {
    Set<UniversalModelExpression> result = Sets.newHashSet();
    if (ute instanceof SubstantialTypeWrapper) {
      result.addAll(findAllInstances(ctx, (SubstantialTypeWrapper) ute));
    }
    else if (ute instanceof RelationshipTypeWrapper) {
      result.addAll(findAllLinks((RelationshipTypeWrapper) ute));
    }
    return result;
  }

  @Override
  protected Object getValue(ElasticeamContext ctx, UniversalModelExpression expression, BuiltinPrimitiveProperty property) {
    return ((UMEWrapper) expression).getValues(property);
  }

  @Override
  protected Object getValue(ElasticeamContext ctx, UniversalModelExpression expression, PropertyExpression<?> property) {
    return ((UMEWrapper) expression).getValue(property);
  }

  @Override
  protected Object getValue(ElasticeamContext ctx, UniversalModelExpression expression, RelationshipEndExpression relationshipEnd) {
    return ((UMEWrapper) expression).getConnecteds(relationshipEnd);
  }

  @Override
  protected boolean canCreate(UniversalTypeExpression universalType) {
    return false;
  }

  @Override
  protected boolean canDelete(LinkExpression linkExpression) {
    return false;
  }

  @Override
  protected boolean canDelete(UniversalTypeExpression typeExpression) {
    return false;
  }

  @Override
  protected boolean canDelete(UniversalModelExpression instance) {
    return false;
  }

  @Override
  protected boolean canEdit(RelationshipEndExpression relationshipEnd) {
    return false;
  }

  @Override
  protected boolean canEdit(UniversalModelExpression expression, PropertyExpression<?> property) {
    return false;
  }

  @Override
  protected boolean canEdit(UniversalModelExpression from, UniversalModelExpression to, RelationshipExpression relationship) {
    return false;
  }

  @Override
  protected InstanceExpression createInstance(SubstantialTypeWrapper type) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected LinkExpression createLink(RelationshipTypeWrapper type) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void deleteInstance(InstanceWrapper instance) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void deleteLink(LinkWrapper link) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void link(ElasticeamContext ctx, UniversalModelExpression from, RelationshipEndWrapper via, UniversalModelExpression to) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void unlink(ElasticeamContext ctx, UniversalModelExpression from, RelationshipEndExpression via, UniversalModelExpression to) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void setValue(ElasticeamContext ctx, UniversalModelExpression expression, BuiltinPrimitiveProperty property, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void setValue(ElasticeamContext ctx, UniversalModelExpression expression, PropertyExpression<?> property, Object value) {
    throw new UnsupportedOperationException();
  }

}
