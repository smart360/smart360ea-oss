/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Set;

import com.google.common.collect.Sets;

import de.iteratec.iteraplan.elasticeam.metamodel.ComparisonOperatorExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.common.AtomicDataTypeExpression;
import de.iteratec.iteraplan.elasticmi.model.ValueExpression;
import de.iteratec.iteraplan.elasticmi.util.ElasticValue;


public class CompOpWrapper extends NamedExpressionWrapper<de.iteratec.iteraplan.elasticmi.metamodel.common.ComparisonOperatorExpression> implements
    ComparisonOperatorExpression {

  CompOpWrapper(MetamodelWrapper mmWrapper, de.iteratec.iteraplan.elasticmi.metamodel.common.ComparisonOperatorExpression wrapped) {
    super(mmWrapper, wrapped);
  }

  @Override
  public Class<? extends NamedExpression> getMetaType() {
    return ComparisonOperatorExpression.class;
  }

  @Override
  public boolean compare(Object o1, Object o2) {

    return wrapped().compare(ElasticValue.one(ValueExpression.create(o1)), ElasticValue.one(ValueExpression.create(o2)));
  }

  @Override
  public Set<Class<?>> getSupportedTypes() {
    Set<Class<?>> types = Sets.newHashSet();
    for (AtomicDataTypeExpression<?> vt : mmWrapper().rMetamodel().getAtomicDataTypes()) {
      if (vt.getComparisonOperators().contains(wrapped())) {
        types.add(vt.getEncapsulatedType());
      }
    }
    return types;
  }

  @Override
  public boolean supportsType(Class<?> type) {
    return getSupportedTypes().contains(type);
  }

}
