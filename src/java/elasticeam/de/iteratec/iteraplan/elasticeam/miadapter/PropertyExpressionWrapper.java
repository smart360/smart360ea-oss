/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Map;

import com.google.common.collect.Maps;

import de.iteratec.iteraplan.elasticeam.derived.InstanceStoreImplProperty;
import de.iteratec.iteraplan.elasticeam.metamodel.DataTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.EnumerationExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.EnumerationPropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.PrimitivePropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.PrimitiveTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.PropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.builtin.BuiltinPrimitiveType;
import de.iteratec.iteraplan.elasticeam.miadapter.MetamodelWrapper.WrapEnumerationFunction;
import de.iteratec.iteraplan.elasticmi.metamodel.common.impl.atomic.AtomicDataType;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RAtomicDataTypeExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNominalEnumerationExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RPropertyExpression;


public abstract class PropertyExpressionWrapper<T extends DataTypeExpression> extends FeatureWrapper<T, RPropertyExpression> implements
    PropertyExpression<T>, InstanceStoreImplProperty {

  private PropertyExpressionWrapper(MetamodelWrapper mmWrapper, RPropertyExpression feature, UniversalTypeWrapper holder) {
    super(mmWrapper, feature, holder);
  }

  public static PrimitivePropertyExpression createPrimitive(MetamodelWrapper mmWrapper, RPropertyExpression feature, UniversalTypeWrapper holder) {
    return new PPWraper(mmWrapper, feature, holder);
  }

  public static EnumerationPropertyExpression createEnum(MetamodelWrapper mmWrapper, RPropertyExpression feature, UniversalTypeWrapper holder) {
    return new EPWraper(mmWrapper, feature, holder);
  }

  private static class PPWraper extends PropertyExpressionWrapper<PrimitiveTypeExpression> implements PrimitivePropertyExpression {

    private static Map<RAtomicDataTypeExpression<?>, PrimitiveTypeExpression> PTIMITIVE_TYPES = pTypes();

    private PPWraper(MetamodelWrapper mmWrapper, RPropertyExpression feature, UniversalTypeWrapper holder) {
      super(mmWrapper, feature, holder);
    }

    @Override
    public PrimitiveTypeExpression getType() {
      return PTIMITIVE_TYPES.get(wrapped().getType());
    }

    @Override
    public Class<? extends NamedExpression> getMetaType() {
      return EnumerationPropertyExpression.class;
    }

    private static Map<RAtomicDataTypeExpression<?>, PrimitiveTypeExpression> pTypes() {
      Map<RAtomicDataTypeExpression<?>, PrimitiveTypeExpression> map = Maps.newHashMap();
      map.put(AtomicDataType.BOOLEAN.type(), BuiltinPrimitiveType.BOOLEAN);
      map.put(AtomicDataType.DATE.type(), BuiltinPrimitiveType.DATE);
      map.put(AtomicDataType.DATE_TIME.type(), BuiltinPrimitiveType.DATE);
      map.put(AtomicDataType.COLOR.type(), BuiltinPrimitiveType.STRING);
      map.put(AtomicDataType.DECIMAL.type(), BuiltinPrimitiveType.DECIMAL);
      map.put(AtomicDataType.DURATION.type(), BuiltinPrimitiveType.DURATION);
      map.put(AtomicDataType.EMAIL.type(), BuiltinPrimitiveType.STRING);
      map.put(AtomicDataType.GEO.type(), BuiltinPrimitiveType.STRING);
      map.put(AtomicDataType.INTEGER.type(), BuiltinPrimitiveType.INTEGER);
      map.put(AtomicDataType.RICH_TEXT.type(), BuiltinPrimitiveType.STRING);
      map.put(AtomicDataType.STRING.type(), BuiltinPrimitiveType.STRING);
      return map;
    }

  }

  static class EPWraper extends PropertyExpressionWrapper<EnumerationExpression> implements EnumerationPropertyExpression {
    private EPWraper(MetamodelWrapper mmWrapper, RPropertyExpression feature, UniversalTypeWrapper holder) {
      super(mmWrapper, feature, holder);
    }

    @Override
    public EnumerationExpression getType() {
      return new WrapEnumerationFunction(mmWrapper()).apply((RNominalEnumerationExpression) wrapped().getType()).orNull();
    }

    @Override
    public Class<? extends NamedExpression> getMetaType() {
      return PrimitivePropertyExpression.class;
    }

  }

}
