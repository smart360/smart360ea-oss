/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Locale;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNamedExpression;


public abstract class NamedExpressionWrapper<R extends RNamedExpression> implements NamedExpression {

  private final MetamodelWrapper mmWrapper;
  private final R                wrapped;

  NamedExpressionWrapper(MetamodelWrapper mmWrapper, R wrapped) {
    this.mmWrapper = mmWrapper;
    this.wrapped = wrapped;
  }

  R wrapped() {
    return wrapped;
  }

  MetamodelWrapper mmWrapper() {
    return mmWrapper;
  }

  @Override
  public String getAbbreviation() {
    return wrapped.getAbbreviation();
  }

  @Override
  public String getAbbreviation(Locale locale) {
    return wrapped.getAbbreviation();
  }

  @Override
  public void setAbbreviation(String abbreviation) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAbbreviation(String abbreviation, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getPersistentName() {
    return wrapped.getPersistentName();
  }

  @Override
  public String getName() {
    return wrapped.getName();
  }

  @Override
  public String getName(Locale locale) {
    return wrapped.getName();
  }

  @Override
  public void setName(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setName(String name, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getDescription() {
    return wrapped.getDescription();
  }

  @Override
  public String getDescription(Locale locale) {
    return wrapped.getDescription();
  }

  @Override
  public void setDescription(String description) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setDescription(String description, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getAbbreviation(ElasticeamContext ctx) {
    return wrapped.getAbbreviation();
  }

  @Override
  public void setAbbreviation(ElasticeamContext ctx, String abbreviation) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getName(ElasticeamContext ctx) {
    return wrapped.getName();
  }

  @Override
  public void setName(ElasticeamContext ctx, String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getDescription(ElasticeamContext ctx) {
    return wrapped.getDescription();
  }

  @Override
  public void setDescription(ElasticeamContext ctx, String description) {
    throw new UnsupportedOperationException();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof NamedExpression)) {
      return false;
    }
    NamedExpression other = (NamedExpression) obj;
    return getPersistentName().equals(other.getPersistentName()) && getMetaType().equals(other.getMetaType());
  }

  public int hashCode() {
    return NamedExpressionWrapper.class.hashCode() ^ wrapped.hashCode();
  }

  public String toString() {
    return getClass().getSimpleName() + "(" + wrapped() + ")";
  }
}
