/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;

import de.iteratec.iteraplan.elasticeam.derived.QueryableModel;
import de.iteratec.iteraplan.elasticeam.metamodel.PropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipEndExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.builtin.BuiltinPrimitiveProperty;
import de.iteratec.iteraplan.elasticeam.miadapter.PropertyExpressionWrapper.EPWraper;
import de.iteratec.iteraplan.elasticeam.model.UniversalModelExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.REnumerationLiteralExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RRelationshipEndExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RStructuredTypeExpression.OriginalWType;
import de.iteratec.iteraplan.elasticmi.model.ObjectExpression;
import de.iteratec.iteraplan.elasticmi.model.ValueExpression;
import de.iteratec.iteraplan.elasticmi.util.ElasticValue;


public class UMEWrapper implements UniversalModelExpression {

  private final UniversalTypeWrapper type;
  private final ObjectExpression     oe;
  private final ModelWrapper         model;

  UMEWrapper(UniversalTypeWrapper type, ObjectExpression oe, ModelWrapper model) {
    this.type = type;
    this.oe = oe;
    this.model = model;
  }

  @Override
  public Object getValue(PropertyExpression<?> property) {
    if (!(property instanceof PropertyExpressionWrapper || property instanceof BuiltinPrimitiveProperty)) {
      return new QueryableModel(model).getValue(this, property);
    }
    ElasticValue<ValueExpression> val = type.wrapped().findPropertyByPersistentName(property.getPersistentName()).apply(oe);
    if (property instanceof EPWraper) {
      return val.isNone() ? null : ((EPWraper) property).getType().findLiteral(
          ((REnumerationLiteralExpression) val.getMany().iterator().next().getValue()).getPersistentName());
    }
    else {
      return val.isNone() ? null : val.getMany().iterator().next().getValue();
    }
  }

  @Override
  public Collection<Object> getValues(PropertyExpression<?> property) {
    if (!(property instanceof PropertyExpressionWrapper || property instanceof BuiltinPrimitiveProperty)) {
      Object result = new QueryableModel(model).getValue(this, property);
      if (result instanceof Collection) {
        return (Collection<Object>) result;
      }
      Set<Object> r = Sets.newHashSet();
      if (result != null) {
        r.add(result);
      }
      return r;
    }
    ElasticValue<ValueExpression> val = type.wrapped().findPropertyByPersistentName(property.getPersistentName()).apply(oe);
    Set<Object> vals = Sets.newHashSet();
    for (ValueExpression ve : val.getMany()) {
      if (property instanceof EPWraper) {
        vals.add(((EPWraper) property).getType().findLiteral(
            ((REnumerationLiteralExpression) val.getMany().iterator().next().getValue()).getPersistentName()));
      }
      else {
        vals.add(ve.getValue());
      }
    }
    return vals;
  }

  @Override
  public UniversalModelExpression getConnected(RelationshipEndExpression relationshipEnd) {
    if (!(relationshipEnd instanceof RelationshipEndWrapper)) {
      return (UniversalModelExpression) new QueryableModel(model).getValue(this, relationshipEnd);
    }
    RRelationshipEndExpression relEnd = type.wrapped().findRelationshipEndByPersistentName(relationshipEnd.getPersistentName());
    ElasticValue<ObjectExpression> connected = relEnd.apply(oe);
    if (connected.isNone()) {
      return null;
    }
    else {
      return wrap((UniversalTypeWrapper) relationshipEnd.getType(), connected.getMany().iterator().next());
    }
  }

  @Override
  public Collection<UniversalModelExpression> getConnecteds(RelationshipEndExpression relationshipEnd) {
    if (!(relationshipEnd instanceof RelationshipEndWrapper)) {
      Object result = new QueryableModel(model).getValue(this, relationshipEnd);
      if (result instanceof Collection) {
        return (Collection<UniversalModelExpression>) result;
      }
      Set<UniversalModelExpression> r = Sets.newHashSet();
      if (result != null) {
        r.add((UniversalModelExpression) result);
      }
      return r;
    }
    RRelationshipEndExpression relEnd = type.wrapped().findRelationshipEndByPersistentName(relationshipEnd.getPersistentName());
    ElasticValue<ObjectExpression> connected = relEnd.apply(oe);
    Set<UniversalModelExpression> result = Sets.newHashSet();
    for (ObjectExpression c : connected) {
      result.add(wrap((UniversalTypeWrapper) relationshipEnd.getType(), c));
    }
    return result;
  }

  private UMEWrapper wrap(UniversalTypeWrapper type, ObjectExpression oe) {
    if (OriginalWType.CLASS.equals(type.wrapped().getOriginalWType())) {
      return new InstanceWrapper(type, oe, model);
    }
    else {
      return new LinkWrapper(type, oe, model);
    }
  }

  @Override
  public void setValue(PropertyExpression<?> property, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void connect(RelationshipEndExpression relationshipEnd, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void disconnect(RelationshipEndExpression relationshipEnd, Object value) {
    throw new UnsupportedOperationException();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof UMEWrapper)) {
      return false;
    }
    UMEWrapper other = (UMEWrapper) obj;
    return oe.getId().equals(other.oe.getId());
  }

  public int hashCode() {
    return UMEWrapper.class.getSimpleName().hashCode() ^ oe.getId().hashCode();
  }

  public String toString() {
    return UMEWrapper.class.getSimpleName() + "(" + oe + ")";
  }
}
