/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.List;
import java.util.Observable;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.metamodel.FeatureExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.PropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipEndExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.miadapter.MetamodelWrapper.WrapCollectionFunction;
import de.iteratec.iteraplan.elasticeam.util.CompareUtil;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RAtomicDataTypeExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNominalEnumerationExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RPropertyExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RRelationshipEndExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RStructuredTypeExpression;
import de.iteratec.iteraplan.elasticmi.model.Model;
import de.iteratec.iteraplan.elasticmi.model.ObjectExpression;
import de.iteratec.iteraplan.elasticmi.util.ElasticValue;


public abstract class UniversalTypeWrapper extends TypeExpressionWrapper<RStructuredTypeExpression, ObjectExpression> implements
    UniversalTypeExpression {

  UniversalTypeWrapper(MetamodelWrapper mmWrapper, RStructuredTypeExpression wrapped) {
    super(mmWrapper, wrapped);
  }

  @Override
  public void update(Observable o, Object arg) {
    //NOOP
  }

  public ElasticValue<ObjectExpression> apply(Model miModel) {
    return wrapped().apply(miModel);
  }

  @Override
  public List<PropertyExpression<?>> getProperties() {
    return new WrapCollectionFunction(new WrapPropertyFunction()).apply(wrapped().getAllProperties());
  }

  @Override
  public PropertyExpression<?> findPropertyByName(String name) {
    return MetamodelWrapper.findByName(name, getProperties()).orNull();
  }

  @Override
  public List<RelationshipEndExpression> getRelationshipEnds() {
    return new WrapCollectionFunction(new WrapRelEndFunction(this)).apply(wrapped().getAllRelationshipEnds());
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByName(String name) {
    return MetamodelWrapper.findByName(name, getRelationshipEnds()).orNull();
  }

  @Override
  public PropertyExpression<?> findPropertyByPersistentName(String persistentName) {
    return MetamodelWrapper.findByPersistentName(persistentName, getProperties()).orNull();
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByPersistentName(String persistentName) {
    return MetamodelWrapper.findByPersistentName(persistentName, getRelationshipEnds()).orNull();
  }

  @Override
  public List<FeatureExpression<?>> getFeatures() {
    List<FeatureExpression<?>> fs = Lists.newArrayList();
    fs.addAll(getProperties());
    fs.addAll(getRelationshipEnds());
    return fs;
  }

  @Override
  public FeatureExpression<?> findFeatureByName(String name) {
    return MetamodelWrapper.findByName(name, getFeatures()).orNull();
  }

  @Override
  public FeatureExpression<?> findFeatureByPersistentName(String persistentName) {
    return MetamodelWrapper.findByPersistentName(persistentName, getFeatures()).orNull();
  }

  @Override
  public List<PropertyExpression<?>> getProperties(ElasticeamContext ctx) {
    return getProperties();
  }

  @Override
  public PropertyExpression<?> findPropertyByName(ElasticeamContext ctx, String name) {
    return findPropertyByName(name);
  }

  @Override
  public List<RelationshipEndExpression> getRelationshipEnds(ElasticeamContext ctx) {
    return getRelationshipEnds();
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByName(ElasticeamContext ctx, String name) {
    return findRelationshipEndByName(name);
  }

  /**{@inheritDoc}**/
  public final int compareTo(UniversalTypeExpression o) {
    return CompareUtil.compareTo(this, o);
  }

  class WrapPropertyFunction implements Function<RPropertyExpression, Option<PropertyExpression>> {

    @Override
    public Option<PropertyExpression> apply(RPropertyExpression rProp) {
      if (rProp.getType() instanceof RAtomicDataTypeExpression<?>) {
        PropertyExpression wp = PropertyExpressionWrapper.createPrimitive(UniversalTypeWrapper.this.mmWrapper(), rProp, UniversalTypeWrapper.this);
        return Option.some(wp);
      }
      else if (rProp.getType() instanceof RNominalEnumerationExpression) {
        PropertyExpression wp = PropertyExpressionWrapper.createEnum(UniversalTypeWrapper.this.mmWrapper(), rProp, UniversalTypeWrapper.this);
        return Option.some(wp);
      }
      return Option.none();
    }
  }

  static class WrapRelEndFunction implements Function<RRelationshipEndExpression, Option<RelationshipEndWrapper>> {

    private final UniversalTypeWrapper holder;

    public WrapRelEndFunction(UniversalTypeWrapper holder) {
      this.holder = holder;
    }

    @Override
    public Option<RelationshipEndWrapper> apply(RRelationshipEndExpression arg0) {
      return Option.some(new RelationshipEndWrapper(holder.mmWrapper(), arg0, holder));
    }

  }

}
