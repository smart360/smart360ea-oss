/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.Collection;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.metamodel.DataTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.EnumerationExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.Metamodel;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.PrimitiveTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.SubstantialTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.TypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.builtin.BuiltinPrimitiveType;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RAtomicDataTypeExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RMetamodel;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNamedExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNominalEnumerationExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RRelationshipExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RStructuredTypeExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RStructuredTypeExpression.OriginalWType;
import de.iteratec.iteraplan.elasticmi.util.NamedUtil;


public class MetamodelWrapper implements Metamodel {

  private final RMetamodel           rMetamodel;
  private final Option<List<String>> defaultColors;

  public MetamodelWrapper(RMetamodel rMetamodel, List<String> defaultColors) {
    this.rMetamodel = rMetamodel;
    this.defaultColors = Option.some(defaultColors);
  }

  RMetamodel rMetamodel() {
    return rMetamodel;
  }

  Option<List<String>> defaultColors() {
    return defaultColors;
  }

  @Override
  public String getName() {
    return "WrappingEamMetamodel";
  }

  @Override
  public List<TypeExpression> getTypes() {
    List<TypeExpression> tes = Lists.newArrayList();
    tes.addAll(getDataTypes());
    tes.addAll(getUniversalTypes());
    return tes;
  }

  @Override
  public List<SubstantialTypeExpression> getSubstantialTypes() {
    return new WrapCollectionFunction(new WrapStFunction(this)).apply(rMetamodel.getStructuredTypes());
  }

  @Override
  public List<RelationshipTypeExpression> getRelationshipTypes() {
    return new WrapCollectionFunction(new WrapRtFunction(this)).apply(rMetamodel.getStructuredTypes());
  }

  @Override
  public List<EnumerationExpression> getEnumerationTypes() {
    return new WrapCollectionFunction(new WrapEnumerationFunction(this)).apply(rMetamodel.getEnumerationTypes());
  }

  @Override
  public List<PrimitiveTypeExpression> getPrimitiveTypes() {
    List<PrimitiveTypeExpression> builtins = Lists.newArrayList();
    builtins.addAll(BuiltinPrimitiveType.BUILTIN_PRIMITIVE_TYPES);
    return builtins;
  }

  @Override
  public List<DataTypeExpression> getDataTypes() {
    List<DataTypeExpression> dtes = Lists.newArrayList();
    dtes.addAll(getPrimitiveTypes());
    dtes.addAll(getEnumerationTypes());
    return dtes;
  }

  @Override
  public List<UniversalTypeExpression> getUniversalTypes() {
    List<UniversalTypeExpression> utes = Lists.newArrayList();
    utes.addAll(getSubstantialTypes());
    utes.addAll(getRelationshipTypes());
    return utes;
  }

  @Override
  public List<RelationshipExpression> getRelationships() {
    List<RelationshipExpression> rels = Lists.newArrayList();
    for (RRelationshipExpression rRel : rMetamodel.getRelationships()) {
      rels.add(new RelationshipWrapper(this, rRel));
    }
    return rels;
  }

  @Override
  public TypeExpression findTypeByName(String name) {
    return findByName(name, getTypes()).orNull();
  }

  @Override
  public TypeExpression findTypeByPersistentName(String persistentName) {
    return findByPersistentName(persistentName, getTypes()).orNull();
  }

  @Override
  public DataTypeExpression findDataTypeByName(String name) {
    return findByName(name, getDataTypes()).orNull();
  }

  @Override
  public DataTypeExpression findDataTypeByPersistentName(String persistentName) {
    return findByPersistentName(persistentName, getDataTypes()).orNull();
  }

  @Override
  public UniversalTypeExpression findUniversalTypeByName(String name) {
    return findByName(name, getUniversalTypes()).orNull();
  }

  @Override
  public UniversalTypeExpression findUniversalTypeByPersistentName(String persistentName) {
    return findByPersistentName(persistentName, getUniversalTypes()).orNull();
  }

  @Override
  public List<TypeExpression> getTypes(ElasticeamContext ctx) {
    return getTypes();
  }

  @Override
  public List<SubstantialTypeExpression> getSubstantialTypes(ElasticeamContext ctx) {
    return getSubstantialTypes();
  }

  @Override
  public List<RelationshipTypeExpression> getRelationshipTypes(ElasticeamContext ctx) {
    return getRelationshipTypes();
  }

  @Override
  public List<EnumerationExpression> getEnumerationTypes(ElasticeamContext ctx) {
    return getEnumerationTypes();
  }

  @Override
  public List<PrimitiveTypeExpression> getPrimitiveTypes(ElasticeamContext ctx) {
    return getPrimitiveTypes();
  }

  @Override
  public List<DataTypeExpression> getDataTypes(ElasticeamContext ctx) {
    return getDataTypes();
  }

  @Override
  public List<UniversalTypeExpression> getUniversalTypes(ElasticeamContext ctx) {
    return getUniversalTypes();
  }

  @Override
  public TypeExpression findTypeByName(ElasticeamContext ctx, String name) {
    return findTypeByName(name);
  }

  @Override
  public List<RelationshipExpression> getRelationships(ElasticeamContext ctx) {
    return getRelationships();
  }

  static <T extends NamedExpression> Option<T> findByName(String name, Collection<T> candidates) {
    for (T candidate : candidates) {
      if (NamedUtil.areSame(name, candidate.getName())) {
        return Option.some(candidate);
      }
    }
    return Option.none();
  }

  static <T extends NamedExpression> Option<T> findByPersistentName(String pName, Collection<T> candidates) {
    for (T candidate : candidates) {
      if (NamedUtil.areSame(pName, candidate.getPersistentName())) {
        return Option.some(candidate);
      }
    }
    return Option.none();
  }

  static class WrapCollectionFunction<R extends RNamedExpression, T extends NamedExpression> implements Function<Collection<R>, List<T>> {

    private final Function<R, Option<T>> innerWrapperFunction;

    WrapCollectionFunction(Function<R, Option<T>> innerWrapperFunction) {
      this.innerWrapperFunction = innerWrapperFunction;
    }

    @Override
    public List<T> apply(Collection<R> arg0) {
      List<T> result = Lists.newArrayList();
      for (R element : arg0) {
        Option<T> res = innerWrapperFunction.apply(element);
        if (res.isDefined()) {
          result.add(res.get());
        }
      }
      return result;
    }

  }

  static class WrapUtFunction implements Function<RStructuredTypeExpression, Option<UniversalTypeWrapper>> {

    private final MetamodelWrapper wrapper;

    WrapUtFunction(MetamodelWrapper wrapper) {
      this.wrapper = wrapper;
    }

    @Override
    public Option<UniversalTypeWrapper> apply(RStructuredTypeExpression arg0) {
      if (OriginalWType.CLASS.equals(arg0.getOriginalWType())) {
        return Option.some((UniversalTypeWrapper) new WrapStFunction(wrapper).apply(arg0).orNull());
      }
      else if (OriginalWType.RELATIONSHIP.equals(arg0.getOriginalWType())) {
        return Option.some((UniversalTypeWrapper) new WrapRtFunction(wrapper).apply(arg0).orNull());
      }
      return Option.none();
    }

  }

  static class WrapStFunction implements Function<RStructuredTypeExpression, Option<SubstantialTypeWrapper>> {

    private final MetamodelWrapper wrapper;

    WrapStFunction(MetamodelWrapper wrapper) {
      this.wrapper = wrapper;
    }

    @Override
    public Option<SubstantialTypeWrapper> apply(RStructuredTypeExpression arg0) {
      if (OriginalWType.CLASS.equals(arg0.getOriginalWType())) {
        return Option.some(new SubstantialTypeWrapper(wrapper, arg0));
      }
      return Option.none();
    }

  }

  static class WrapRtFunction implements Function<RStructuredTypeExpression, Option<RelationshipTypeWrapper>> {

    private final MetamodelWrapper wrapper;

    WrapRtFunction(MetamodelWrapper wrapper) {
      this.wrapper = wrapper;
    }

    @Override
    public Option<RelationshipTypeWrapper> apply(RStructuredTypeExpression arg0) {
      if (OriginalWType.RELATIONSHIP.equals(arg0.getOriginalWType())) {
        return Option.some(new RelationshipTypeWrapper(wrapper, arg0));
      }
      return Option.none();
    }

  }

  static class WrapEnumerationFunction implements Function<RNominalEnumerationExpression, Option<EnumerationExpressionWrapper>> {

    private final MetamodelWrapper wrapper;

    WrapEnumerationFunction(MetamodelWrapper wrapper) {
      this.wrapper = wrapper;
    }

    @Override
    public Option<EnumerationExpressionWrapper> apply(RNominalEnumerationExpression arg0) {
      return Option.some(new EnumerationExpressionWrapper(wrapper, arg0));
    }

  }

  static class WrapPrimitiveDtFunction implements Function<RAtomicDataTypeExpression<?>, Option<PrimitiveTypeExpressionWrapper>> {

    private final MetamodelWrapper wrapper;

    WrapPrimitiveDtFunction(MetamodelWrapper wrapper) {
      this.wrapper = wrapper;
    }

    @Override
    public Option<PrimitiveTypeExpressionWrapper> apply(RAtomicDataTypeExpression<?> arg0) {
      return Option.some(new PrimitiveTypeExpressionWrapper(wrapper, arg0));
    }

  }

}
