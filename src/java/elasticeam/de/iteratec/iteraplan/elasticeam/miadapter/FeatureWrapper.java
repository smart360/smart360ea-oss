/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import de.iteratec.iteraplan.elasticeam.metamodel.FeatureExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.TypeExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.util.CompareUtil;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RFeatureExpression;


public abstract class FeatureWrapper<T extends TypeExpression, F extends RFeatureExpression<?, ?>> extends NamedExpressionWrapper<F> implements
    FeatureExpression<T> {

  private final UniversalTypeWrapper holder;

  FeatureWrapper(MetamodelWrapper mmWrapper, F wrapped, UniversalTypeWrapper holder) {
    super(mmWrapper, wrapped);
    this.holder = holder;
  }

  /**{@inheritDoc}**/
  public final int compareTo(FeatureExpression<T> o) {
    return CompareUtil.compareTo(this, o);
  }

  @Override
  public int getLowerBound() {
    return wrapped().getLowerBound();
  }

  @Override
  public int getUpperBound() {
    return wrapped().getUpperBound();
  }

  @Override
  public UniversalTypeExpression getSource() {
    return getOrigin();
  }

  @Override
  public UniversalTypeExpression getOwner() {
    return getHolder();
  }

  @Override
  public UniversalTypeExpression getOrigin() {
    return holder;
  }

  @Override
  public UniversalTypeExpression getHolder() {
    return holder;
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof FeatureExpression<?>)) {
      return false;
    }
    FeatureExpression<?> other = (FeatureExpression<?>) obj;
    return super.equals(other) && getHolder().getPersistentName().equals(other.getHolder().getPersistentName());
  }

}
