/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.awt.Color;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.metamodel.EnumerationExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.EnumerationLiteralExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.REnumerationLiteralExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RNominalEnumerationExpression;


public class EnumerationExpressionWrapper extends DataTypeExpressionWrapper<RNominalEnumerationExpression> implements EnumerationExpression {

  EnumerationExpressionWrapper(MetamodelWrapper mmWrapper, RNominalEnumerationExpression wrapped) {
    super(mmWrapper, wrapped);
  }

  @Override
  public Class<? extends NamedExpression> getMetaType() {
    return EnumerationExpression.class;
  }

  @Override
  public List<EnumerationLiteralExpression> getLiterals() {
    List<String> defaultColors = mmWrapper().defaultColors().orNull();
    List<EnumerationLiteralExpression> result = Lists.newArrayList();
    for (REnumerationLiteralExpression lit : wrapped().getLiterals()) {
      Color lColor = defaultColors != null ? Color.decode("#" + defaultColors.get(result.size() % defaultColors.size())) : null;
      result.add(new WrapLiteralFunction().apply(new LiteralWithColor(lit, lColor)).get());
    }
    return result;

    //TODO use this to add proper default colors for enum literals
    //    List<String> defaultColors = SpringGuiFactory.getInstance().getVbbClusterColors();
    //    for (Enum<?> literal : enumm.getEnumConstants()) {
    //      EnumerationLiteralExpression ele = getEMFMetamodel().createEnumerationLiteral(enumeration, literal.name(),
    //          Color.decode("#" + defaultColors.get(cntLiterals % defaultColors.size())));
    //      add(ele, literal);
    //      ele.setName(literal.name());
    //      cntLiterals += 1;
    //    }
    //return new WrapCollectionFunction(new WrapLiteralFunction()).apply(wrapped().getLiterals());
  }

  @Override
  public EnumerationLiteralExpression findLiteral(String name) {
    return MetamodelWrapper.findByName(name, getLiterals()).orNull();
  }

  @Override
  public EnumerationLiteralExpression findLiteralByPersistentName(String persistentName) {
    return MetamodelWrapper.findByPersistentName(persistentName, getLiterals()).orNull();
  }

  @Override
  public List<EnumerationLiteralExpression> getLiterals(ElasticeamContext ctx) {
    return getLiterals();
  }

  @Override
  public EnumerationLiteralExpression findLiteral(ElasticeamContext ctx, String name) {
    return findLiteral(name);
  }

  class WrapLiteralFunction implements Function<LiteralWithColor, Option<EnumerationLiteralExpression>> {

    @Override
    public Option<EnumerationLiteralExpression> apply(LiteralWithColor arg0) {
      return Option.some((EnumerationLiteralExpression) new EnumerationLiteralExpressionWrapper(EnumerationExpressionWrapper.this.mmWrapper(),
          arg0.literal, EnumerationExpressionWrapper.this, Option.some(arg0.color)));
    }

  }

  static class LiteralWithColor {
    final REnumerationLiteralExpression literal;
    final Color                         color;

    LiteralWithColor(REnumerationLiteralExpression literal, Color color) {
      this.literal = literal;
      this.color = color;
    }

  }

}
