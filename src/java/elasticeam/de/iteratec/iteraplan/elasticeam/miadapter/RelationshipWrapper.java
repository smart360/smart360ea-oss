/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.elasticeam.miadapter;

import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;

import de.iteratec.iteraplan.elasticeam.ElasticeamContext;
import de.iteratec.iteraplan.elasticeam.metamodel.NamedExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipEndExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.miadapter.MetamodelWrapper.WrapUtFunction;
import de.iteratec.iteraplan.elasticeam.miadapter.UniversalTypeWrapper.WrapRelEndFunction;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RRelationshipEndExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RRelationshipExpression;
import de.iteratec.iteraplan.elasticmi.metamodel.read.RStructuredTypeExpression;


public class RelationshipWrapper implements RelationshipExpression {

  private final MetamodelWrapper        mmWrapper;
  private final RRelationshipExpression wrapped;

  RelationshipWrapper(MetamodelWrapper mmWrapper, RRelationshipExpression wrapped) {
    this.mmWrapper = mmWrapper;
    this.wrapped = wrapped;
  }

  @Override
  public String getAbbreviation() {
    return wrapped.getRelationshipEnds().get(0).getAbbreviation() + "-" + wrapped.getRelationshipEnds().get(1).getAbbreviation();
  }

  @Override
  public String getAbbreviation(Locale locale) {
    return getAbbreviation();
  }

  @Override
  public void setAbbreviation(String abbreviation) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAbbreviation(String abbreviation, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getPersistentName() {
    return wrapped.getRelationshipEnds().get(0).getPersistentName() + "-" + wrapped.getRelationshipEnds().get(1).getPersistentName();
  }

  @Override
  public String getName() {
    return wrapped.getRelationshipEnds().get(0).getName() + "-" + wrapped.getRelationshipEnds().get(1).getName();
  }

  @Override
  public String getName(Locale locale) {
    return getName();
  }

  @Override
  public void setName(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setName(String name, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getDescription() {
    return wrapped.getRelationshipEnds().get(0).getDescription() + "-" + wrapped.getRelationshipEnds().get(1).getDescription();
  }

  @Override
  public String getDescription(Locale locale) {
    return getDescription();
  }

  @Override
  public void setDescription(String description) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setDescription(String description, Locale locale) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Class<? extends NamedExpression> getMetaType() {
    return RelationshipExpression.class;
  }

  @Override
  public String getAbbreviation(ElasticeamContext ctx) {
    return getAbbreviation();
  }

  @Override
  public void setAbbreviation(ElasticeamContext ctx, String abbreviation) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getName(ElasticeamContext ctx) {
    return getName();
  }

  @Override
  public void setName(ElasticeamContext ctx, String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getDescription(ElasticeamContext ctx) {
    return getDescription();
  }

  @Override
  public void setDescription(ElasticeamContext ctx, String description) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<RelationshipEndExpression> getRelationshipEnds() {
    RRelationshipEndExpression end0 = wrapped.getRelationshipEnds().get(0);
    RRelationshipEndExpression end1 = wrapped.getRelationshipEnds().get(1);
    RStructuredTypeExpression end0holder = end1.getType();
    RStructuredTypeExpression end1holder = end0.getType();

    List<RelationshipEndExpression> relEnds = Lists.newArrayList();
    relEnds.add(new WrapRelEndFunction(new WrapUtFunction(mmWrapper).apply(end0holder).get()).apply(end0).get());
    relEnds.add(new WrapRelEndFunction(new WrapUtFunction(mmWrapper).apply(end1holder).get()).apply(end1).get());
    return relEnds;
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByName(String name) {
    return MetamodelWrapper.findByName(name, getRelationshipEnds()).orNull();
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByPersistentName(String name) {
    return MetamodelWrapper.findByPersistentName(name, getRelationshipEnds()).orNull();
  }

  @Override
  public RelationshipEndExpression getEndLeadingTo(UniversalTypeExpression type) {
    for (RelationshipEndExpression relEnd : getRelationshipEnds()) {
      if (relEnd.getType().equals(type)) {
        return relEnd;
      }
    }
    return null;
  }

  @Override
  public RelationshipEndExpression getOppositeEndFor(RelationshipEndExpression relationshipEnd) {
    List<RelationshipEndExpression> relEnds = getRelationshipEnds();
    if (relEnds.get(0).equals(relationshipEnd)) {
      return relEnds.get(1);
    }
    return relEnds.get(0);
  }

  @Override
  public boolean isAcyclic() {
    return wrapped.isAcyclic();
  }

  @Override
  public List<RelationshipEndExpression> getRelationshipEnds(ElasticeamContext ctx) {
    return getRelationshipEnds();
  }

  @Override
  public RelationshipEndExpression findRelationshipEndByName(ElasticeamContext ctx, String name) {
    return findRelationshipEndByName(name);
  }

}
