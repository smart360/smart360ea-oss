/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.presentation.rest.representation;

import java.util.Map;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import de.iteratec.iteraplan.common.collections.BBTLocalizedNameFunction;
import de.iteratec.iteraplan.model.BuildingBlockType;
import de.iteratec.iteraplan.model.TypeOfBuildingBlock;
import de.iteratec.iteraplan.model.attribute.AttributeType;
import de.iteratec.iteraplan.model.attribute.ResponsibilityAT;
import de.iteratec.iteraplan.model.attribute.ResponsibilityAV;
import de.iteratec.iteraplan.model.user.PermissionAttrTypeGroup;
import de.iteratec.iteraplan.model.user.PermissionFunctional;
import de.iteratec.iteraplan.model.user.Role;
import de.iteratec.iteraplan.model.user.Role2BbtPermission.EditPermissionType;
import de.iteratec.iteraplan.model.user.TypeOfFunctionalPermission;
import de.iteratec.iteraplan.model.user.User;
import de.iteratec.iteraplan.model.user.UserEntity;
import de.iteratec.iteraplan.model.user.UserGroup;
import de.iteratec.iteraplan.persistence.dao.AttributeTypeDAO;
import de.iteratec.iteraplan.persistence.dao.PermissionFunctionalDAO;
import de.iteratec.iteraplan.persistence.dao.RoleDAO;
import de.iteratec.iteraplan.persistence.dao.UserDAO;
import de.iteratec.iteraplan.persistence.dao.UserGroupDAO;
import de.iteratec.iteraplan.presentation.rest.ResourceType;
import de.iteratec.iteraplan.presentation.rest.RestUtils;

public class JsonResponsibilityRepresentationHandler implements
		RepresentationHandler {

	public static final String KEY_PERM_FUNCT = "functionalPermissions";
	public static final String KEY_ROLES = "roles";
	public static final String KEY_USER_GROUPS = "userGroups";
	public static final String KEY_USERS = "users";
	public static final String KEY_RESP_VALUES = "responsibilityValues";

	public static final String KEY_PERM_FUNCT_ID = "id";
	public static final String KEY_PERM_FUNCT_TYPE = "type";

	public static final String KEY_ROLE_ID = "id";
	public static final String KEY_ROLE_NAME = "name";
	public static final String KEY_ROLE_USERS = "users";
	public static final String KEY_ROLE_PERMISSIONS = "permissions";
	public static final String KEY_ROLE_TYPE_PERMISSIONS = "typePermissions";

	public static final String KEY_GROUP_ID = "id";
	public static final String KEY_GROUP_NAME = "name";
	public static final String KEY_GROUP_MEMBERS = "members";

	public static final String KEY_USER_ID = "id";
	public static final String KEY_USER_LOGIN_NAME = "loginName";
	public static final String KEY_USER_FIRST_NAME = "firstName";
	public static final String KEY_USER_LAST_NAME = "lastName";
	public static final String KEY_USER_EMAIL = "email";
	public static final String KEY_USER_ROLES = "roles";
	
	public static final String KEY_ATGS_WITH_READ_ACCESS = "explicitReadAccessATGs";
	public static final String KEY_ATGS_WITH_WRITE_ACCESS = "explicitWriteAccessATGs";
	

	private PermissionFunctionalDAO permissionDao;
	private RoleDAO roleDao;
	private UserGroupDAO userGroupDao;
	private UserDAO userDao;
	private AttributeTypeDAO atDao;
	
	@Override
	public Representation process(Request request, Response response,
			Map<String, Object> arguments) {
				
		Representation result = new JsonRepresentation(
				RestUtils.formatToJson(getRespJson()));

		response.setEntity(result);
		response.setStatus(Status.SUCCESS_OK);

		return result;
	}
	
	protected JsonObject getRespJson() {
		JsonObject jObject = new JsonObject();

		jObject.add(KEY_PERM_FUNCT, getFunctionalPermissions());
		jObject.add(KEY_ROLES, getRoles());
		jObject.add(KEY_USER_GROUPS, getUserGroups());
		jObject.add(KEY_USERS, getUsers());
		jObject.add(KEY_RESP_VALUES, getRespValues());
		
		return jObject;
	}

	@Override
	public boolean supports(ResourceType resourceType) {
		return ResourceType.RESPONSIBILITY.equals(resourceType);
	}
	
	private JsonArray getRespValues() {
		JsonArray vals = new JsonArray();
		for (AttributeType at : atDao.loadElementList(null)) {
			if (ResponsibilityAT.class.isInstance(at)) {
				for (ResponsibilityAV av : ((ResponsibilityAT)at).getSortedAttributeValues()) {
					vals.add(new JsonPrimitive(av.getName()));
				}
				return vals;
			}
		}
		return vals;
	}

	private JsonArray getFunctionalPermissions() {
		JsonArray permissions = new JsonArray();
		for (PermissionFunctional pf : permissionDao.loadElementList(null)) {
			JsonObject perm = new JsonObject();
			perm.add(KEY_PERM_FUNCT_ID, new JsonPrimitive(pf.getId()));
			perm.add(KEY_PERM_FUNCT_TYPE,
					new JsonPrimitive(pf.getIdentityString()));
			permissions.add(perm);
		}
		return permissions;
	}

	private JsonArray getRoles() {
		JsonArray roles = new JsonArray();
		for (Role role : roleDao.loadElementList(null)) {
			JsonObject ro = new JsonObject();
			ro.add(KEY_ROLE_ID, new JsonPrimitive(role.getId()));
			ro.add(KEY_ROLE_NAME, new JsonPrimitive(role.getRoleName()));
			
			JsonArray users = new JsonArray();
			for (User u : role.getUsers()) {
				users.add(new JsonPrimitive(u.getId()));
			}			
			ro.add(KEY_ROLE_USERS, users);
			
			JsonArray permissions = new JsonArray();			
			for (PermissionFunctional perm : role.getPermissionsFunctionalAggregated()) {
				permissions.add(new JsonPrimitive(perm.getId()));				
			}			
			ro.add(KEY_ROLE_PERMISSIONS, permissions);			
			
			ro.add(KEY_ROLE_TYPE_PERMISSIONS, typePermissions(role));			
			
			JsonArray readATGs = new JsonArray();
			JsonArray writeATGs = new JsonArray();
			for (PermissionAttrTypeGroup pAtg : role.getPermissionsAttrTypeGroupAggregated()) {
				if (pAtg.isReadPermission()) {
					readATGs.add(new JsonPrimitive(pAtg.getAttrTypeGroup().getId()));
				}
				if (pAtg.isWritePermission()) {
					writeATGs.add(new JsonPrimitive(pAtg.getAttrTypeGroup().getId()));
				}
			}
			ro.add(KEY_ATGS_WITH_READ_ACCESS, readATGs);
			ro.add(KEY_ATGS_WITH_WRITE_ACCESS, writeATGs);
			
			roles.add(ro);
		}

		return roles;
	}

	private JsonObject typePermissions(Role role) {
		JsonObject typePermissions = new JsonObject();
		
		
		for (TypeOfBuildingBlock tobb : TypeOfBuildingBlock.ALL) {
			Class<?> clazz = tobb.getAssociatedClass();
			JsonArray perms = new JsonArray();
			for (PermissionFunctional pf : role.getPermissionsFunctional()) {
				Class<?> readableClass = TypeOfFunctionalPermission.PERMISSION_TO_CLASS_MAP.get(pf.getTypeOfFunctionalPermission());
				if (readableClass == null || !clazz.equals(readableClass)) {
					continue;
				}				
				perms.add(new JsonPrimitive("R"));
				
				if (hasPermission(role, EditPermissionType.CREATE, tobb)) {
					perms.add(new JsonPrimitive("C"));
				}
				if (hasPermission(role, EditPermissionType.UPDATE, tobb)) {
					perms.add(new JsonPrimitive("U"));
				}
				if (hasPermission(role, EditPermissionType.DELETE, tobb)) {
					perms.add(new JsonPrimitive("D"));
				}
				
				break;
				
			}
			
			
			typePermissions.add(clazz.getSimpleName(), perms);
		}
		
		return typePermissions;
		
	}
	
	private boolean hasPermission(Role role, EditPermissionType ept, TypeOfBuildingBlock tobb) {
		for (BuildingBlockType bbt : role.getBbtForPermissionTypeAggregated(ept)) {
			if (tobb.equals(bbt.getTypeOfBuildingBlock())) {
				return true;
			}
		}
		return false;
	}
	
	
	
	

	private JsonArray getUserGroups() {
		JsonArray groups = new JsonArray();
		for (UserGroup group : userGroupDao.loadElementList(null)) {
			JsonObject gr = new JsonObject();
			gr.add(KEY_GROUP_ID, new JsonPrimitive(group.getId()));
			gr.add(KEY_GROUP_NAME, new JsonPrimitive(group.getName()));
			
			JsonArray members = new JsonArray();
			
			for (UserEntity u : group.getMembers()) {
				members.add(new JsonPrimitive(u.getId()));
			}
			
			gr.add(KEY_GROUP_MEMBERS, members);
			
			groups.add(gr);			
		}

		return groups;
	}

	private JsonArray getUsers() {
		JsonArray users = new JsonArray();
		for (User user : userDao.loadElementList(null)) {
			JsonObject us = new JsonObject();
			us.add(KEY_USER_ID, new JsonPrimitive(user.getId()));
			us.add(KEY_USER_LOGIN_NAME, new JsonPrimitive(user.getLoginName()));
			
			if (user.getFirstName() != null) {
				us.add(KEY_USER_FIRST_NAME, new JsonPrimitive(user.getFirstName()));				
			}
			if (user.getLastName()!= null) {
				us.add(KEY_USER_LAST_NAME, new JsonPrimitive(user.getLastName()));				
			}
			if (user.getEmail() != null) {
				us.add(KEY_USER_EMAIL, new JsonPrimitive(user.getEmail()));				
			}
			JsonArray roles = new JsonArray();
			for (Role role : user.getRoles()) {
				roles.add(new JsonPrimitive(role.getId()));
			}
			
			us.add(KEY_USER_ROLES, roles);
			users.add(us);
		}

		return users;
	}

	public void setPermissionDao(PermissionFunctionalDAO permissionDao) {
		this.permissionDao = permissionDao;
	}

	public void setRoleDao(RoleDAO roleDao) {
		this.roleDao = roleDao;
	}

	public void setUserGroupDao(UserGroupDAO userGroupDao) {
		this.userGroupDao = userGroupDao;
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

	public void setAtDao(AttributeTypeDAO atDao) {
		this.atDao = atDao;
	}

}
