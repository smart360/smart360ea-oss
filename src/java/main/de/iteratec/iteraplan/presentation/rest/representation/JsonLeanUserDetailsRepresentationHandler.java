package de.iteratec.iteraplan.presentation.rest.representation;

import java.util.Iterator;
import java.util.Map;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.search.LdapUserSearch;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import de.iteratec.iteraplan.common.UserContext;
import de.iteratec.iteraplan.common.error.IteraplanBusinessException;
import de.iteratec.iteraplan.common.error.IteraplanErrorMessages;
import de.iteratec.iteraplan.common.util.IteraplanProperties;
import de.iteratec.iteraplan.model.user.Role;
import de.iteratec.iteraplan.model.user.User;
import de.iteratec.iteraplan.persistence.dao.UserDAO;
import de.iteratec.iteraplan.presentation.rest.ResourceType;
import de.iteratec.iteraplan.presentation.rest.RestUtils;

public class JsonLeanUserDetailsRepresentationHandler implements RepresentationHandler {

	private LdapUserSearch userSearch;
	private UserDetailsService userService;
	private UserDAO userDao;
	
	private String firstName;
	private String lastName;
	private String email;
	
	public static final String FIRST_NAME ="firstName";
	public static final String LAST_NAME ="lastName";
	public static final String EMAIL ="email";
	public static final String ROLES ="roles";
	
	private void init() {
		IteraplanProperties props = IteraplanProperties.getProperties();
		firstName = props.getProperty(IteraplanProperties.LDAP_FIELDNAME_FIRSTNAME);
		lastName = props.getProperty(IteraplanProperties.LDAP_FIELDNAME_LASTNAME);
		email = props.getProperty(IteraplanProperties.LDAP_FIELDNAME_EMAIL);		
	}
	
	@Override
	public Representation process(Request request, Response response,
			Map<String, Object> arguments) {
		
		String username = (String)arguments.get("userName");
		
		if (!isUserSuperUser() && (username == null || username.trim().isEmpty() || !UserContext.getCurrentUserContext().getLoginName().equals(username))) {
			throw new IteraplanBusinessException(IteraplanErrorMessages.ACCESS_NOT_ALLOWED);
		}
		
		Representation result = new JsonRepresentation(
				RestUtils.formatToJson(getUserDetailJson(username)));

		response.setEntity(result);
		response.setStatus(Status.SUCCESS_OK);
		
		return result;
	}

	private JsonElement getUserDetailJson(String username) {
		JsonObject jObject = new JsonObject();
		
		if (userDao == null) {
			//LDAP mode
			init();
			DirContextOperations ops = userSearch.searchForUser(username);
			UserDetails details = userService.loadUserByUsername(username);
			
			jObject.add(ops.getStringAttribute("uid"), getUserDetails(ops, details));
		}
		else {
			//DAO mode
			User user = userDao.getUserByLoginIfExists(username);
			if (user != null) {
				jObject.add(username, getUserDetails(user));
			}			
		}
		
		return jObject;
	}
	
	private JsonObject getUserDetails(User user) {
		JsonObject ud = new JsonObject();

		if (user.getFirstName() != null) {
			ud.add(FIRST_NAME, new JsonPrimitive(user.getFirstName()));
		}
		
		if (user.getLastName() != null) {
			ud.add(LAST_NAME, new JsonPrimitive(user.getLastName()));
		}
		
		if (user.getEmail() != null) {
			ud.add(EMAIL, new JsonPrimitive(user.getEmail()));
		}
		
		JsonArray roles = new JsonArray();
		
		for (Role role : user.getRoles()) {
			roles.add(new JsonPrimitive(role.getRoleName()));
		}
		
		ud.add(ROLES, roles);
		
		return ud;
	}

	private JsonObject getUserDetails(DirContextOperations ops, UserDetails details) {
		
		JsonObject ud = new JsonObject();
		
		if (ops.attributeExists(firstName)) {
			ud.add(FIRST_NAME, new JsonPrimitive(ops.getStringAttribute(firstName)));			
		}
		
		if (ops.attributeExists(lastName)) {
			ud.add(LAST_NAME, new JsonPrimitive(ops.getStringAttribute(lastName)));			
		}
		
		if (ops.attributeExists(email)) {
			ud.add(EMAIL, new JsonPrimitive(ops.getStringAttribute(email)));			
		}
		
		JsonArray auths = new JsonArray();		
		
		for(GrantedAuthority a : details.getAuthorities()) {
			auths.add(new JsonPrimitive(a.getAuthority()));
		}
		
		ud.add(ROLES, auths);
		
		return ud;
	}

	@Override
	public boolean supports(ResourceType resourceType) {
		return ResourceType.USER_DETAILS.equals(resourceType);
	}

	public void setUserSearch(LdapUserSearch userSearch) {
		this.userSearch = userSearch;
	}

	public void setUserService(UserDetailsService userService) {
		this.userService = userService;
	}
	
	protected boolean isUserSuperUser() {
		boolean supervisor = false;
		Iterator<Role> roles = UserContext.getCurrentUserContext().getRoles().iterator(); 
		while(roles.hasNext()) {
			Role role = roles.next();
			if (Role.SUPERVISOR_ROLE_NAME.equals(role.getRoleName())) {
				supervisor = true;
				break;
			}
		}
		
		return supervisor;
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

}
