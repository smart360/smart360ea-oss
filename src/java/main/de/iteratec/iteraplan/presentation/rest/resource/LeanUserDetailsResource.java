package de.iteratec.iteraplan.presentation.rest.resource;

import static de.iteratec.iteraplan.presentation.rest.IteraplanRestApplication.KEY_FORMAT;
import static de.iteratec.iteraplan.presentation.rest.IteraplanRestApplication.VALUE_DEFAULT_FORMAT;

import java.util.Map;

import org.restlet.representation.Representation;

import com.google.common.collect.Maps;

import de.iteratec.iteraplan.common.Logger;
import de.iteratec.iteraplan.common.error.IteraplanBusinessException;
import de.iteratec.iteraplan.common.error.IteraplanErrorMessages;
import de.iteratec.iteraplan.presentation.rest.ResourceType;

public class LeanUserDetailsResource extends LeanRestResource {

	private static final Logger LOGGER = Logger.getIteraplanLogger(LeanUserDetailsResource.class);
	
	@Override
	protected Representation doGetResource() {
				
		String format = (String) getArgument(KEY_FORMAT);
		if (format == null || format.isEmpty()
				|| VALUE_DEFAULT_FORMAT.equals(format) || "json".equals(format)) {
			return getData();
		}
		return getErrorMessage(new IteraplanBusinessException(
				IteraplanErrorMessages.FORMAT_NOT_SUPPORTED));
	}
	
	private Representation getData() {
		Representation resultRepresentation = null;

		try {
			resultRepresentation = process("json", ResourceType.USER_DETAILS);
		} catch (Exception e) {
			LOGGER.error(e);
			return getErrorMessage(e);
		}

		return resultRepresentation;
	}
	
	@Override
	protected Map<String, Object> getInitialArguments() {
		Map<String, Object> args = Maps.newHashMap();
		String userName = (String)getRequest().getAttributes().get("userName");
		if (userName != null && !userName.trim().isEmpty()) {
			args.put("userName", userName);
		}
		return args;
	}
}
