/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.presentation.rest.representation;

import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_DESCRIPTION;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_DIRECTION;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_ID;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_LMT;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_LMU;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_NAME;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_PARENT;
import static de.iteratec.iteraplan.presentation.rest.representation.JsonLeanMetamodelRepresentationHandler.KEY_POSITION;

import java.lang.reflect.ParameterizedType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import de.iteratec.iteraplan.model.AbstractHierarchicalEntity;
import de.iteratec.iteraplan.model.ArchitecturalDomain;
import de.iteratec.iteraplan.model.BuildingBlock;
import de.iteratec.iteraplan.model.BusinessDomain;
import de.iteratec.iteraplan.model.BusinessFunction;
import de.iteratec.iteraplan.model.BusinessMapping;
import de.iteratec.iteraplan.model.BusinessObject;
import de.iteratec.iteraplan.model.BusinessProcess;
import de.iteratec.iteraplan.model.BusinessUnit;
import de.iteratec.iteraplan.model.InformationSystem;
import de.iteratec.iteraplan.model.InformationSystemDomain;
import de.iteratec.iteraplan.model.InformationSystemInterface;
import de.iteratec.iteraplan.model.InformationSystemRelease;
import de.iteratec.iteraplan.model.InfrastructureElement;
import de.iteratec.iteraplan.model.Isr2BoAssociation;
import de.iteratec.iteraplan.model.Product;
import de.iteratec.iteraplan.model.Project;
import de.iteratec.iteraplan.model.RuntimePeriod;
import de.iteratec.iteraplan.model.Tcr2IeAssociation;
import de.iteratec.iteraplan.model.TechnicalComponent;
import de.iteratec.iteraplan.model.TechnicalComponentRelease;
import de.iteratec.iteraplan.model.Transport;
import de.iteratec.iteraplan.model.attribute.AttributeType;
import de.iteratec.iteraplan.model.attribute.AttributeTypeGroup;
import de.iteratec.iteraplan.model.attribute.AttributeValue;
import de.iteratec.iteraplan.model.attribute.AttributeValueAssignment;
import de.iteratec.iteraplan.model.attribute.DateAT;
import de.iteratec.iteraplan.model.attribute.DateAV;
import de.iteratec.iteraplan.model.attribute.DateInterval;
import de.iteratec.iteraplan.model.attribute.MultiassignementType;
import de.iteratec.iteraplan.model.attribute.NumberAV;
import de.iteratec.iteraplan.persistence.dao.ArchitecturalDomainDAO;
import de.iteratec.iteraplan.persistence.dao.AssociationDAOTemplate;
import de.iteratec.iteraplan.persistence.dao.BusinessDomainDAO;
import de.iteratec.iteraplan.persistence.dao.BusinessFunctionDAO;
import de.iteratec.iteraplan.persistence.dao.BusinessMappingDAO;
import de.iteratec.iteraplan.persistence.dao.BusinessObjectDAO;
import de.iteratec.iteraplan.persistence.dao.BusinessProcessDAO;
import de.iteratec.iteraplan.persistence.dao.BusinessUnitDAO;
import de.iteratec.iteraplan.persistence.dao.DAOTemplate;
import de.iteratec.iteraplan.persistence.dao.DateIntervalDAO;
import de.iteratec.iteraplan.persistence.dao.InformationSystemDAO;
import de.iteratec.iteraplan.persistence.dao.InformationSystemDomainDAO;
import de.iteratec.iteraplan.persistence.dao.InformationSystemInterfaceDAO;
import de.iteratec.iteraplan.persistence.dao.InformationSystemReleaseDAO;
import de.iteratec.iteraplan.persistence.dao.InfrastructureElementDAO;
import de.iteratec.iteraplan.persistence.dao.Isr2BoAssociationDAO;
import de.iteratec.iteraplan.persistence.dao.ProductDAO;
import de.iteratec.iteraplan.persistence.dao.ProjectDAO;
import de.iteratec.iteraplan.persistence.dao.Tcr2IeAssociationDAO;
import de.iteratec.iteraplan.persistence.dao.TechnicalComponentDAO;
import de.iteratec.iteraplan.persistence.dao.TechnicalComponentReleaseDAO;
import de.iteratec.iteraplan.persistence.dao.TransportDAO;
import de.iteratec.iteraplan.presentation.rest.ResourceType;
import de.iteratec.iteraplan.presentation.rest.RestUtils;

public class JsonLeanModelRepresentationHandler implements
		RepresentationHandler {

	private ArchitecturalDomainDAO architecturalDomainDao;
	private BusinessDomainDAO businessDomainDao;
	private BusinessFunctionDAO businessFunctionDao;
	private BusinessObjectDAO businessObjectDao;
	private BusinessProcessDAO businessProcessDao;
	private BusinessMappingDAO businessMappingDao;
	private BusinessUnitDAO businessUnitDao;
	
	private InformationSystemDAO informationSystemDao;
	private InformationSystemDomainDAO informationSystemDomainDao;
	private InformationSystemInterfaceDAO informationSystemInterfaceDao;
	private InformationSystemReleaseDAO informationSystemReleaseDao;
	private InfrastructureElementDAO infrastructureElementDao;
	private ProductDAO productDao;
	private ProjectDAO projectDao;
	private Tcr2IeAssociationDAO tcr2IeAssociationDao;
	private Isr2BoAssociationDAO isr2BoAssociationDao;
	
	private TechnicalComponentDAO technicalComponentDao;
	private TechnicalComponentReleaseDAO technicalComponentReleaseDao;
	private TransportDAO transportDao;
	
	private DateIntervalDAO dateIntervalDao;
	
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	private Map<DateAT, DateInterval> customStartDates;
	private Set<DateAT> customEndDates;
	
	@Override
	public Representation process(Request request, Response response,
			Map<String, Object> arguments) {
		
		Representation result = new JsonRepresentation(
				RestUtils.formatToJson(getJson((String)arguments.get("type"))));

		response.setEntity(result);
		response.setStatus(Status.SUCCESS_OK);

		return result;
	}

	
	private JsonObject getJson(String typeName) {
		
		loadCustomDateIntervals();

		JsonObject res = new JsonObject();

		res.add("model", getTypes(typeName));

		return res;
	}
	
	private void loadCustomDateIntervals() {
		
		customStartDates = Maps.newHashMap();
		customEndDates = Sets.newHashSet();
		
		for (DateInterval di : dateIntervalDao.loadElementList(null)) {
			
			customStartDates.put(di.getStartDate(), di);
			customEndDates.add(di.getEndDate());
		}			
		
	}

	private JsonObject getTypes(String typeName) {

		JsonObject types = new JsonObject();

		for (DAOTemplate<? extends BuildingBlock, ?> dao : getDaos()) {
			JsonArray array = new JsonArray();
			Class<?> bbtClass = ((Class<?>)((ParameterizedType)dao.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
			
			if (typeName != null && !bbtClass.getSimpleName().equals(typeName)) {
				continue; //only export requested types
			}
			
			if (AssociationDAOTemplate.class.isInstance(dao)) {
				bbtClass = ((Class<?>)((ParameterizedType)dao.getClass().getGenericSuperclass()).getActualTypeArguments()[2]);
			}
			for (BuildingBlock bb : dao.loadElementList(null)) {				

				if (!isTopLevelElement(bb)) {
					JsonObject jbb = new JsonObject();
					jbb.add("_______default", defaultFeatureGroup(bb));
					attributeTypes(bb, jbb);
					array.add(jbb);
				}				
			}
			types.add(bbtClass.getSimpleName(), array);
		}

		return types;
	}
	
	private void attributeTypes(BuildingBlock bb, JsonObject jbb) {
		
		for (AttributeType at : bb.getBuildingBlockType().getAttributeTypesAsList()) {
			AttributeTypeGroup atg = at.getAttributeTypeGroup();
			JsonObject jAtg = null;
			if (jbb.has(atg.getName())) {
				jAtg = jbb.get(atg.getName()).getAsJsonObject();
			}
			else {
				jAtg = new JsonObject();
				jbb.add(atg.getName(), jAtg);
			}
			if (customStartDates.containsKey(at)) {
				DateInterval di = customStartDates.get(at);
				DateAT startAT = (DateAT)at;
				DateAT endAT = di.getEndDate();
				AttributeValueAssignment startAva = bb.getAssignmentForId(startAT.getId());
				AttributeValueAssignment endAva = bb.getAssignmentForId(endAT.getId());				
				Date start = null;
				Date end = null;
				
				if (startAva != null && startAva.getAttributeValue() != null) {
					start = ((DateAV)startAva.getAttributeValue()).getValue();
					
				}
				if (endAva != null && endAva.getAttributeValue() != null) {
					end = ((DateAV)endAva.getAttributeValue()).getValue();
				}
				
				jAtg.add(di.getName(), interval(new RuntimePeriod(start, end)));
				
			}
			else if (customEndDates.contains(at)) {
				//ignore me
			}
			else {
				JsonElement jAvs = avsAsJson(at, bb.getAssignmentsForId(at.getId()));
				if (jAvs != null) {
					jAtg.add(at.getName(), jAvs);				
				}				
			}
		}
	}

	private JsonElement avsAsJson(AttributeType at,
			Set<AttributeValueAssignment> avs) {
		if (MultiassignementType.class.isInstance(at)) {
			JsonArray array = new JsonArray();
			for (AttributeValueAssignment ava : avs) {
				array.add(avAsJson(ava.getAttributeValue()));
			}
			return array;
		}
		else {
			if (avs.isEmpty()) {
				return null;
			}
			return avAsJson(avs.iterator().next().getAttributeValue());
		}
	}
	
	private JsonPrimitive avAsJson(AttributeValue av) {
		if (av == null) {
			return null;
		}
		
		if (NumberAV.class.isInstance(av)) {
			return new JsonPrimitive(((NumberAV)av).getValue());
		}
		if (DateAV.class.isInstance(av)) {
			return new JsonPrimitive(DATE_FORMAT.format(((DateAV)av).getValue()));
		}
		
		return new JsonPrimitive(av.getValueString());
		
	}

	private boolean isTopLevelElement(BuildingBlock bb) {
		return (AbstractHierarchicalEntity.class.isInstance(bb) && AbstractHierarchicalEntity.TOP_LEVEL_NAME.equals(((AbstractHierarchicalEntity<?>)bb).getName()));
	}

	private JsonObject defaultFeatureGroup(BuildingBlock bb) {
		JsonObject jbb = new JsonObject();
		jbb.add(KEY_ID, new JsonPrimitive(bb.getId()));
		if (AbstractHierarchicalEntity.class.isInstance(bb)) {
			AbstractHierarchicalEntity<?> ahe = (AbstractHierarchicalEntity<?>) bb;
			jbb.add(KEY_NAME, new JsonPrimitive(ahe.getName()));
			if (ahe.getDescription() != null) {
				jbb.add(KEY_DESCRIPTION, new JsonPrimitive(ahe.getDescription()));
			}
			if (ahe.getPosition() == null) {
				jbb.add(KEY_POSITION, new JsonPrimitive(0));
			} else {
				jbb.add(KEY_POSITION, new JsonPrimitive(ahe.getPosition()));
			}
			AbstractHierarchicalEntity<?> parent = (AbstractHierarchicalEntity<?>) ahe.getParent();
			if (parent != null && !isTopLevelElement(parent)) {
				jbb.add(KEY_PARENT, new JsonPrimitive(parent.getId()));
			}
		}
		else if (InformationSystemInterface.class.isInstance(bb)) {
			InformationSystemInterface isi = (InformationSystemInterface)bb;
			if (isi.getName() != null) {
				jbb.add(KEY_NAME, new JsonPrimitive(isi.getName()));
			}
			if (isi.getDirection() != null) {
				jbb.add(KEY_DIRECTION, new JsonPrimitive(isi.getDirection()));
			}
		}		
		
		if (bb.getLastModificationTime() != null) {
			jbb.add(KEY_LMT, new JsonPrimitive(DATE_TIME_FORMAT.format(bb.getLastModificationTime())));			
		}
		if (bb.getLastModificationUser() != null) {
			jbb.add(KEY_LMU, new JsonPrimitive(bb.getLastModificationUser()));			
		}
		
		if (InformationSystemRelease.class.isInstance(bb)) {
			InformationSystemRelease isr = (InformationSystemRelease)bb;
			if (isr.getVersion() != null) {
				jbb.add("release", new JsonPrimitive(isr.getVersion()));
			}
			if (isr.getDescription() != null) {
				jbb.add(KEY_DESCRIPTION, new JsonPrimitive(isr.getDescription()));				
			}
			jbb.add("runtimePeriod", interval(isr.getRuntimePeriod()));
			jbb.add("typeOfStatus", new JsonPrimitive(isr.getTypeOfStatus().getValue()));
			jbb.add("informationSystem", new JsonPrimitive(isr.getInformationSystemId()));

			if (isr.getParent() != null) {
				jbb.add("parent", new JsonPrimitive(isr.getParent().getId()));				
			}
			
			jbb.add("businessMappings", bbIds(isr.getBusinessMappings()));
			jbb.add("isr2BoAssociations", bbIds(isr.getBusinessObjectAssociations()));
			jbb.add("interfacesReleaseA", bbIds(isr.getInterfacesReleaseA()));
			jbb.add("interfacesReleaseB", bbIds(isr.getInterfacesReleaseB()));
			jbb.add("parentComponents", bbIds(isr.getParentComponents()));
			jbb.add("baseComponents", bbIds(isr.getBaseComponents()));
			jbb.add("predecessors", bbIds(isr.getPredecessors()));
			jbb.add("successors", bbIds(isr.getSuccessors()));

			jbb.add("businessFunctions", bbIds(isr.getBusinessFunctions()));
			jbb.add("infrastructureElements", bbIds(isr.getInfrastructureElements()));
			jbb.add("informationSystemDomains", bbIds(isr.getInformationSystemDomains()));
			jbb.add("projects", bbIds(isr.getProjects()));
			jbb.add("technicalComponentReleases", bbIds(isr.getTechnicalComponentReleases()));
		}
		if (InformationSystem.class.isInstance(bb)) {
			InformationSystem is = (InformationSystem)bb;
			jbb.add(KEY_NAME, new JsonPrimitive(is.getName()));
			jbb.add("releases", bbIds(is.getReleases()));
		}
		if (TechnicalComponentRelease.class.isInstance(bb)) {
			TechnicalComponentRelease tcr = (TechnicalComponentRelease)bb;
			if (tcr.getDescription() != null) {
				jbb.add(KEY_DESCRIPTION, new JsonPrimitive(tcr.getDescription()));				
			}
			jbb.add("runtimePeriod", interval(tcr.getRuntimePeriod()));
			jbb.add("typeOfStatus", new JsonPrimitive(tcr.getTypeOfStatus().getValue()));
			jbb.add("technicalComponent", new JsonPrimitive(tcr.getTechnicalComponent().getId()));
			jbb.add("parentComponents", bbIds(tcr.getParentComponents()));
			jbb.add("baseComponents", bbIds(tcr.getBaseComponents()));
			jbb.add("predecessors", bbIds(tcr.getPredecessors()));
			jbb.add("successors", bbIds(tcr.getSuccessors()));
			
			jbb.add("informationSystemReleases", bbIds(tcr.getInformationSystemReleases()));
			if (tcr.isAvailableForInterfaces()) {
				jbb.add("informationSystemInterfaces", bbIds(tcr.getInformationSystemInterfaces()));				
			}
			jbb.add("architecturalDomains", bbIds(tcr.getArchitecturalDomains()));
			jbb.add("tcr2IeAssociations", bbIds(tcr.getInfrastructureElementAssociations()));
			
		}
		if (TechnicalComponent.class.isInstance(bb)) {
			TechnicalComponent tc = (TechnicalComponent)bb;
			jbb.add(KEY_NAME, new JsonPrimitive(tc.getName()));
			jbb.add("releases", bbIds(tc.getReleases()));
			jbb.add("availableForInterfaces", new JsonPrimitive(tc.isAvailableForInterfaces()));
		}
		
		if (Project.class.isInstance(bb)) {
			jbb.add("runtimePeriod", interval(((Project)bb).getRuntimePeriod()));
		}
		
		if (BusinessMapping.class.isInstance(bb)) {
			
			BusinessMapping bm = (BusinessMapping)bb;
			
			BusinessUnit bu = bm.getBusinessUnit();
			BusinessProcess bp = bm.getBusinessProcess();
			InformationSystemRelease isr = bm.getInformationSystemRelease();			
			Product prod = bm.getProduct();
			
			if (bu != null) {
				jbb.add("businessUnit", new JsonPrimitive(bu.getId()));
			}
			if (bp != null) {
				jbb.add("businessProcess", new JsonPrimitive(bp.getId()));
			}
			if (isr != null) {
				jbb.add("informationSystemRelease", new JsonPrimitive(isr.getId()));
			}
			if (prod != null) {
				jbb.add("product", new JsonPrimitive(prod.getId()));
			}
		}
		
		if (Isr2BoAssociation.class.isInstance(bb)) {
			Isr2BoAssociation isr2bo = (Isr2BoAssociation)bb;
			BusinessObject bo = isr2bo.getBusinessObject();
			InformationSystemRelease isr = isr2bo.getInformationSystemRelease();
			
			if (isr != null) {
				jbb.add("informationSystemRelease", new JsonPrimitive(isr.getId()));
			}
			
			if (bo != null) {
				jbb.add("businessObject", new JsonPrimitive(bo.getId()));
			}
		}
		
		if (Tcr2IeAssociation.class.isInstance(bb)) {
			Tcr2IeAssociation tcr2ie = (Tcr2IeAssociation)bb;
			TechnicalComponentRelease tcr = tcr2ie.getTechnicalComponentRelease();
			InfrastructureElement ie = tcr2ie.getInfrastructureElement();
			
			if (tcr != null) {
				jbb.add("technicalComponentRelease", new JsonPrimitive(tcr.getId()));
			}
			
			if (ie != null) {
				jbb.add("infrastructureElement", new JsonPrimitive(ie.getId()));
			}
		}
		
		if (ArchitecturalDomain.class.isInstance(bb)) {
			jbb.add("technicalComponentReleases", bbIds(((ArchitecturalDomain)bb).getTechnicalComponentReleases()));
		}
		
		if (BusinessDomain.class.isInstance(bb)) {
			BusinessDomain bd = (BusinessDomain)bb;
			jbb.add("businessUnits", bbIds(bd.getBusinessUnits()));
			jbb.add("businessFunctions", bbIds(bd.getBusinessFunctions()));
			jbb.add("businessProcesses", bbIds(bd.getBusinessProcesses()));
			jbb.add("products", bbIds(bd.getProducts()));
		}
		
		if (BusinessFunction.class.isInstance(bb)) {
			BusinessFunction bf = (BusinessFunction)bb;
			jbb.add("businessDomains", bbIds(bf.getBusinessDomains()));
			jbb.add("businessObjects", bbIds(bf.getBusinessObjects()));
			jbb.add("informationSystemReleases", bbIds(bf.getInformationSystems()));
		}
		
		if (BusinessUnit.class.isInstance(bb)) {
			BusinessUnit bu = (BusinessUnit)bb;
			jbb.add("businessDomains", bbIds(bu.getBusinessDomains()));
			jbb.add("businessMappings", bbIds(bu.getBusinessMappings()));
		}
		
		if (BusinessObject.class.isInstance(bb)) {
			BusinessObject bo = (BusinessObject)bb;
			BusinessObject generalisation = bo.getGeneralisation();
			jbb.add("isr2BoAssociations", bbIds(bo.getInformationSystemReleaseAssociations()));
			if (generalisation != null) {
				jbb.add("generalisation", new JsonPrimitive(generalisation.getId()));				
			}			
			jbb.add("businessFunctions", bbIds(bo.getBusinessFunctions()));
			jbb.add("transports", bbIds(bo.getTransports()));
		}
		
		if (BusinessProcess.class.isInstance(bb)) {
			BusinessProcess bp = (BusinessProcess)bb;
			jbb.add("businessMappings", bbIds(bp.getBusinessMappings()));
			jbb.add("businessDomains", bbIds(bp.getBusinessDomains()));
		}
		
		if (InformationSystemDomain.class.isInstance(bb)) {
			InformationSystemDomain isd = (InformationSystemDomain)bb;
			jbb.add("informationSystemReleases", bbIds(isd.getInformationSystemReleases()));
		}
		
		if (InformationSystemInterface.class.isInstance(bb)) {
			InformationSystemInterface isi = (InformationSystemInterface)bb;
			jbb.add(KEY_DIRECTION, new JsonPrimitive(isi.getInterfaceDirection().getValue()));
			InformationSystemRelease isrA = isi.getInformationSystemReleaseA();
			InformationSystemRelease isrB = isi.getInformationSystemReleaseB();
			
			if (isrA != null) {
				jbb.add("informationSystemReleaseA", new JsonPrimitive(isrA.getId()));				
			}
			if (isrB != null) {
				jbb.add("informationSystemReleaseB", new JsonPrimitive(isrB.getId()));				
			}
			jbb.add("technicalComponentReleases", bbIds(isi.getTechnicalComponentReleases()));
			jbb.add("transports", bbIds(isi.getTransports()));
		}
		
		if (InfrastructureElement.class.isInstance(bb)) {
			InfrastructureElement ie = (InfrastructureElement)bb;
			jbb.add("tcr2IeAssociations", bbIds(ie.getTechnicalComponentReleaseAssociations()));
			jbb.add("parentComponents", bbIds(ie.getParentComponents()));
			jbb.add("baseComponents", bbIds(ie.getBaseComponents()));
			jbb.add("informationSystemReleases", bbIds(ie.getInformationSystemReleases()));
		}
		
		if (Product.class.isInstance(bb)) {
			Product prod = (Product)bb;
			jbb.add("businessMappings", bbIds(prod.getBusinessMappings()));
			jbb.add("businessDomains", bbIds(prod.getBusinessDomains()));
		}
		
		if (Project.class.isInstance(bb)) {
			jbb.add("informationSystemReleases", bbIds(((Project)bb).getInformationSystemReleases()));
		}
		
		if (Transport.class.isInstance(bb)) {
			Transport transport = (Transport)bb;
			jbb.add(KEY_DIRECTION, new JsonPrimitive(transport.getDirection().getValue()));
			BusinessObject bo = transport.getBusinessObject();
			InformationSystemInterface isi = transport.getInformationSystemInterface();			
			if (bo != null) {
				jbb.add("businessObject", new JsonPrimitive(bo.getId()));				
			}
			if (isi != null) {
				jbb.add("informationSystemInterface", new JsonPrimitive(isi.getId()));
			}
		}
		
		return jbb;
	}
	
	private JsonElement bbIds(Iterable<? extends BuildingBlock> bbs) {
		JsonArray ids = new JsonArray();
		for (BuildingBlock bb : bbs) {
			ids.add(new JsonPrimitive(bb.getId()));
		}
		return ids;
	}

	private static JsonObject interval(RuntimePeriod rp) {
		JsonObject jrp = new JsonObject();
		if (rp != null) {
			if (rp.getStart() != null) {
				jrp.add("from", new JsonPrimitive(DATE_FORMAT.format(rp.getStart())));
			}
			if (rp.getEnd() != null) {
				jrp.add("to", new JsonPrimitive(DATE_FORMAT.format(rp.getEnd())));
			}
		}
		return jrp;
	}

	@Override
	public boolean supports(ResourceType resourceType) {
		return ResourceType.LEAN_MODEL.equals(resourceType);
	}

	public void setArchitecturalDomainDao(
			ArchitecturalDomainDAO architecturalDomainDao) {
		this.architecturalDomainDao = architecturalDomainDao;
	}

	public void setBusinessDomainDao(BusinessDomainDAO businessDomainDao) {
		this.businessDomainDao = businessDomainDao;
	}

	public void setBusinessFunctionDao(BusinessFunctionDAO businessFunctionDao) {
		this.businessFunctionDao = businessFunctionDao;
	}

	public void setBusinessObjectDao(BusinessObjectDAO businessObjectDao) {
		this.businessObjectDao = businessObjectDao;
	}

	public void setBusinessProcessDao(BusinessProcessDAO businessProcessDao) {
		this.businessProcessDao = businessProcessDao;
	}

	public void setBusinessMappingDao(BusinessMappingDAO businessMappingDao) {
		this.businessMappingDao = businessMappingDao;
	}

	public void setBusinessUnitDao(BusinessUnitDAO businessUnitDao) {
		this.businessUnitDao = businessUnitDao;
	}

	private List<DAOTemplate<? extends BuildingBlock, Integer>> getDaos() {
		List<DAOTemplate<? extends BuildingBlock, Integer>> daos = Lists
				.newArrayList();

		daos.add(businessFunctionDao);
		daos.add(businessMappingDao);
		daos.add(businessProcessDao);
		daos.add(businessUnitDao);
		daos.add(productDao);
		
		daos.add(businessObjectDao);
		daos.add(informationSystemInterfaceDao);
		daos.add(informationSystemDao);
		daos.add(informationSystemReleaseDao);
		daos.add(transportDao);
		
		daos.add(projectDao);
		
		daos.add(infrastructureElementDao);
		
		daos.add(technicalComponentDao);
		daos.add(technicalComponentReleaseDao);
		
		daos.add(architecturalDomainDao);
		daos.add(businessDomainDao);
		daos.add(informationSystemDomainDao);
		
		daos.add(isr2BoAssociationDao);
		daos.add(tcr2IeAssociationDao);
		
		return daos;
	}

	public void setInformationSystemDao(InformationSystemDAO informationSystemDao) {
		this.informationSystemDao = informationSystemDao;
	}

	public void setInformationSystemDomainDao(
			InformationSystemDomainDAO informationSystemDomainDao) {
		this.informationSystemDomainDao = informationSystemDomainDao;
	}

	public void setInformationSystemInterfaceDao(
			InformationSystemInterfaceDAO informationSystemInterfaceDao) {
		this.informationSystemInterfaceDao = informationSystemInterfaceDao;
	}

	public void setInformationSystemReleaseDao(
			InformationSystemReleaseDAO informationSystemReleaseDao) {
		this.informationSystemReleaseDao = informationSystemReleaseDao;
	}

	public void setInfrastructureElementDao(
			InfrastructureElementDAO infrastructureElementDao) {
		this.infrastructureElementDao = infrastructureElementDao;
	}

	public void setProductDao(ProductDAO productDao) {
		this.productDao = productDao;
	}

	public void setProjectDao(ProjectDAO projectDao) {
		this.projectDao = projectDao;
	}

	public void setTcr2IeAssociationDao(Tcr2IeAssociationDAO tcr2IeAssociationDao) {
		this.tcr2IeAssociationDao = tcr2IeAssociationDao;
	}

	public void setIsr2BoAssociationDao(Isr2BoAssociationDAO isr2BoAssociationDao) {
		this.isr2BoAssociationDao = isr2BoAssociationDao;
	}

	public void setTechnicalComponentDao(TechnicalComponentDAO technicalComponentDao) {
		this.technicalComponentDao = technicalComponentDao;
	}

	public void setTechnicalComponentReleaseDao(
			TechnicalComponentReleaseDAO technicalComponentReleaseDao) {
		this.technicalComponentReleaseDao = technicalComponentReleaseDao;
	}

	public void setTransportDao(TransportDAO transportDao) {
		this.transportDao = transportDao;
	}

	public void setDateIntervalDao(DateIntervalDAO dateIntervalDao) {
		this.dateIntervalDao = dateIntervalDao;
	}

}
