/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.presentation.rest.representation;

import java.util.Map;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import de.iteratec.iteraplan.model.queries.SavedQuery;
import de.iteratec.iteraplan.persistence.dao.SavedQueryDAO;
import de.iteratec.iteraplan.presentation.rest.ResourceType;
import de.iteratec.iteraplan.presentation.rest.RestUtils;

public class JsonSavedQueryRepresentationHandler implements
		RepresentationHandler {

	public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_DESCRIPTION = "description";
	public static final String KEY_REPORT_TYPE = "type";
	public static final String KEY_RESULT_TYPE = "resultType";
	public static final String KEY_CONTENT = "query";
	public static final String KEY_EXECUTION_URL = "executionUrl";
	public static final String KEY_CONFIG_URL = "configUrl";
	

	private SavedQueryDAO savedQueryDao;

	@Override
	public Representation process(Request request, Response response,
			Map<String, Object> arguments) {

		Representation result = new JsonRepresentation(
				RestUtils.formatToJson(getSavedQueryJson()));

		response.setEntity(result);
		response.setStatus(Status.SUCCESS_OK);

		return result;
	}

	@Override
	public boolean supports(ResourceType resourceType) {
		return ResourceType.SAVED_QUERY.equals(resourceType);
	}

	public void setSavedQueryDao(SavedQueryDAO savedQueryDao) {
		this.savedQueryDao = savedQueryDao;
	}

	private JsonArray getSavedQueryJson() {
		JsonArray queries = new JsonArray();

		for (SavedQuery query : savedQueryDao.getSavedQueries(SavedQuery.class)) {
			JsonObject jq = new JsonObject();
			
			jq.add(KEY_ID, new JsonPrimitive(query.getId()));
			jq.add(KEY_NAME, new JsonPrimitive(query.getName()));
			if (query.getDescription() != null) {
				jq.add(KEY_DESCRIPTION, new JsonPrimitive(query.getDescription()));				
			}
			if (query.getType() != null) {
				jq.add(KEY_REPORT_TYPE, new JsonPrimitive(query.getType().getValue()));				
			}
			if (query.getResultBbType() != null) {
				jq.add(KEY_RESULT_TYPE, new JsonPrimitive(query.getResultBbType().getName()));				
			}
			JsonPrimitive p = new JsonPrimitive("/show/fastexport/generateSavedQuery.do?id=" + query.getId() + "&savedQueryType=" + query.getType().getValue() + "&outputMode=attachment");
		
			jq.add(KEY_CONFIG_URL, new JsonPrimitive("/show/fastexport/generateSavedQuery.do?id=" + query.getId() + "&savedQueryType=" + query.getType().getValue() + "&outputMode=attachment"));
			jq.add(KEY_EXECUTION_URL, new JsonPrimitive("/show/" + query.getType().getFlowMapping() + "?_eventId=loadSavedQuery&savedQueryId=" + query.getId()));
			
			
			if (query.getContent() != null) {
				jq.add(KEY_CONTENT, new JsonPrimitive(query.getContent()));				
			}
			queries.add(jq);
		}

		return queries;
	}

}
