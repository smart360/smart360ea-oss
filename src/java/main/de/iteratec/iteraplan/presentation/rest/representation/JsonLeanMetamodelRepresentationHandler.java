/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.presentation.rest.representation;

import java.awt.Color;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import de.iteratec.iteraplan.common.Logger;
import de.iteratec.iteraplan.common.MessageAccess;
import de.iteratec.iteraplan.elasticmi.metamodel.common.ElasticMiConstants;
import de.iteratec.iteraplan.model.ArchitecturalDomain;
import de.iteratec.iteraplan.model.BuildingBlockType;
import de.iteratec.iteraplan.model.BusinessDomain;
import de.iteratec.iteraplan.model.BusinessFunction;
import de.iteratec.iteraplan.model.BusinessMapping;
import de.iteratec.iteraplan.model.BusinessObject;
import de.iteratec.iteraplan.model.BusinessProcess;
import de.iteratec.iteraplan.model.BusinessUnit;
import de.iteratec.iteraplan.model.Direction;
import de.iteratec.iteraplan.model.InformationSystem;
import de.iteratec.iteraplan.model.InformationSystemDomain;
import de.iteratec.iteraplan.model.InformationSystemInterface;
import de.iteratec.iteraplan.model.InformationSystemRelease;
import de.iteratec.iteraplan.model.InformationSystemRelease.TypeOfStatus;
import de.iteratec.iteraplan.model.InfrastructureElement;
import de.iteratec.iteraplan.model.Isr2BoAssociation;
import de.iteratec.iteraplan.model.Product;
import de.iteratec.iteraplan.model.Project;
import de.iteratec.iteraplan.model.TechnicalComponent;
import de.iteratec.iteraplan.model.TechnicalComponentRelease;
import de.iteratec.iteraplan.model.Transport;
import de.iteratec.iteraplan.model.TypeOfBuildingBlock;
import de.iteratec.iteraplan.model.attribute.AttributeType;
import de.iteratec.iteraplan.model.attribute.AttributeTypeGroup;
import de.iteratec.iteraplan.model.attribute.DateAT;
import de.iteratec.iteraplan.model.attribute.DateInterval;
import de.iteratec.iteraplan.model.attribute.EnumAT;
import de.iteratec.iteraplan.model.attribute.EnumAV;
import de.iteratec.iteraplan.model.attribute.NumberAT;
import de.iteratec.iteraplan.model.attribute.ResponsibilityAT;
import de.iteratec.iteraplan.model.attribute.TextAT;
import de.iteratec.iteraplan.persistence.dao.AttributeTypeDAO;
import de.iteratec.iteraplan.persistence.dao.AttributeTypeGroupDAO;
import de.iteratec.iteraplan.persistence.dao.BuildingBlockTypeDAO;
import de.iteratec.iteraplan.persistence.dao.DateIntervalDAO;
import de.iteratec.iteraplan.presentation.rest.ResourceType;
import de.iteratec.iteraplan.presentation.rest.RestUtils;

public class JsonLeanMetamodelRepresentationHandler implements RepresentationHandler {
	
	private static final Logger LOGGER = Logger.getIteraplanLogger(JsonLeanMetamodelRepresentationHandler.class);

	public static final String KEY_ENUMS = "enumerations";
	public static final String KEY_ATTRIBUTE_GROUPS = "attributeGroups";
	public static final String KEY_TYPE_GROUPS = "typeGroups";
	public static final String KEY_TYPES = "types";
	
	public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_NAMES = "names";
	public static final String KEY_PLURAL_NAMES = "pluralNames";
	public static final String KEY_DESCRIPTION = "description";
	public static final String KEY_DESCRIPTIONS = "descriptions";
	public static final String KEY_ABBR = "abbreviation";
	public static final String KEY_ABBRS = "abbreviations";
	public static final String KEY_LMU = ElasticMiConstants.PERSISTENT_NAME_LAST_MODIFICATION_USER;
	public static final String KEY_LMT = ElasticMiConstants.PERSISTENT_NAME_LAST_MODIFICATION_TIME;
	public static final String KEY_POSITION = ElasticMiConstants.PERSISTENT_NAME_HIERARCHY_POSITION;
	public static final String KEY_PARENT = ElasticMiConstants.PERSISTENT_NAME_HIERARCHY_PARENT;
	public static final String KEY_CHILDREN = ElasticMiConstants.PERSISTENT_NAME_HIERARCHY_CHILDREN;
	public static final String KEY_DIRECTION = "direction";
	public static final String KEY_TYPE = "type";
	public static final String KEY_OPPOSITE = "opposite";
	public static final String KEY_ORDINAL = "isOrdinal";
	public static final String KEY_LITERALS = "literals";
	public static final String KEY_INDEX = "index";
	public static final String KEY_COLOR = "color";
	public static final String KEY_IS_TOPLEVEL_ATG = "isToplevelAtg";
	public static final String KEY_ATTRIBUTE_TYPES = "attributeTypes";
	public static final String KEY_FEATURE_GROUPS = "featureGroups";
	public static final String KEY_READ_ROLES = "readableRoles";
	public static final String KEY_WRITE_ROLES = "writableRoles";
	public static final String DEFAULT_FEATURE_GROUP = "_______default";
	
	public static final String KEY_UNIQUE = "unique";
	public static final String KEY_DEFINING = "defining";
	public static final String KEY_SYSTEM_FEATURE = "systemFeature";
	public static final String KEY_MULTIPLICITY_MAND = "mandatory";
	public static final String KEY_MULTIPLICITY_MULT = "multiple";
	public static final String KEY_TIMESERIES = "timeseries";
	public static final String KEY_TOBB = "typeOfBuildingBlock";
	
	// Type Group Names
	public static final String TYPE_GROUP_BUSINESS_LAYER = "BusinessArchitecture";
	public static final String TYPE_GROUP_IS_LAYER = "IsArchitecture";
	public static final String TYPE_GROUP_IE_LAYER = "InfrastructureArchitecture";
	public static final String TYPE_GROUP_TC_LAYER = "TechnicalArchitecture";
	public static final String TYPE_GROUP_PROJECT_LAYER = "ProjectPortfolio";
	public static final String TYPE_GROUP_DOMAIN_LAYER = "DomainLayer";
	public static final String TYPE_GROUP_CROSS_LAYER = "Relations";
	
	// ValueTypes
	public static final String INTEGER = "integer";
	public static final String STRING = "string";
	public static final String BOOLEAN = "boolean";
	public static final String RICHTEXT = "richtext";
	public static final String DATE = "date";
	public static final String DATETIME = "date_time";
	public static final String DECIMAL = "decimal";
	public static final String RESPONSIBILITY = "responsibility";
	public static final String INTERVAL = "date_interval";

	private AttributeTypeDAO atDao;
	private AttributeTypeGroupDAO atGroupDao;
	private DateIntervalDAO dateIntervalDao;
	private BuildingBlockTypeDAO bbtDao;	
	
	private JsonResponsibilityRepresentationHandler respHandler;
	
	private Map<DateAT, DateInterval> customStartDates;
	private Set<DateAT> customEndDates;
	
	private static final List<TypeOfBuildingBlock> HIERARCHIC_ELEMENTS = Lists.newArrayList(
																	TypeOfBuildingBlock.ARCHITECTURALDOMAIN, 
																	TypeOfBuildingBlock.BUSINESSDOMAIN, 
																	TypeOfBuildingBlock.INFORMATIONSYSTEMDOMAIN, 
																	TypeOfBuildingBlock.BUSINESSPROCESS, 
																	TypeOfBuildingBlock.PRODUCT, 
																	TypeOfBuildingBlock.BUSINESSFUNCTION, 
																	TypeOfBuildingBlock.BUSINESSUNIT,
																	TypeOfBuildingBlock.PROJECT,
																	TypeOfBuildingBlock.BUSINESSOBJECT,
																	TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE,
																	TypeOfBuildingBlock.INFRASTRUCTUREELEMENT);
	
	private static final List<TypeOfBuildingBlock> RELEASE_ELEMENTS = Lists.newArrayList(TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE, TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE);
	private static final List<TypeOfBuildingBlock> RELEASABLE_ELEMENTS = Lists.newArrayList(TypeOfBuildingBlock.INFORMATIONSYSTEM, TypeOfBuildingBlock.TECHNICALCOMPONENT);
	private static final List<TypeOfBuildingBlock> DIRECTION_ELEMENTS = Lists.newArrayList(TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE, TypeOfBuildingBlock.TRANSPORT);
	
		
	@Override
	public Representation process(Request request, Response response,
			Map<String, Object> arguments) {
		Representation result = new JsonRepresentation(
				RestUtils.formatToJson(getJson()));

		response.setEntity(result);
		response.setStatus(Status.SUCCESS_OK);		

		return result;
	}

	private JsonObject getJson() {
		
		loadCustomDateIntervals();
		
		JsonObject struct = new JsonObject();
		
		struct.add(KEY_ENUMS, getEnums());
		struct.add(KEY_ATTRIBUTE_GROUPS, getAtgs());
		struct.add(KEY_TYPE_GROUPS, getTypeGroups());		
		
		JsonObject resp = respHandler.getRespJson();
		JsonObject result = new JsonObject();
		
		result.add("metamodel", struct);
		result.add("responsibilities", resp);
		return result;
	}
	
	private void loadCustomDateIntervals() {
		
		LOGGER.debug("Loading custom date intervals");
		
		customStartDates = Maps.newHashMap();
		customEndDates = Sets.newHashSet();
		
		for (DateInterval di : dateIntervalDao.loadElementList(null)) {
			
			customStartDates.put(di.getStartDate(), di);
			customEndDates.add(di.getEndDate());
		}			
		
		LOGGER.debug("Finished Loading of custom date intervals");
	}

	

	private JsonObject getTypeGroups() {
		JsonObject typeGroups = new JsonObject();
		
		LOGGER.debug("Serializing Type Groups");
		
		typeGroups.add(TYPE_GROUP_BUSINESS_LAYER, getBusinessArchitecture());
		typeGroups.add(TYPE_GROUP_PROJECT_LAYER, getProjectPortfolio());
		typeGroups.add(TYPE_GROUP_IS_LAYER, getIsArchitecture());
		typeGroups.add(TYPE_GROUP_IE_LAYER, getInfrastructureArchitecture());
		typeGroups.add(TYPE_GROUP_TC_LAYER, getTechnicalArchitecture());
		typeGroups.add(TYPE_GROUP_DOMAIN_LAYER, getDomainArchitecture());
		typeGroups.add(TYPE_GROUP_CROSS_LAYER, getCrossLayerRelations());
		
		LOGGER.debug("Finished Serialization of Type Groups");
		
		return typeGroups;
	}

	private JsonObject getBusinessArchitecture() {
		
		LOGGER.debug("Serializing Type Group 'Business Architecture'");
		
		JsonObject ba = new JsonObject();
		ba.add(KEY_NAMES, localize("global.businessArchitecture"));
		
		JsonObject types = new JsonObject();
		for (TypeOfBuildingBlock tobb : Lists.newArrayList(
											TypeOfBuildingBlock.BUSINESSFUNCTION,
											TypeOfBuildingBlock.BUSINESSMAPPING,
											TypeOfBuildingBlock.BUSINESSPROCESS,
											TypeOfBuildingBlock.BUSINESSUNIT,
											TypeOfBuildingBlock.PRODUCT
				)) {			
			types.add(tobb.getAssociatedClass().getSimpleName(), typeJson(tobb, !TypeOfBuildingBlock.BUSINESSMAPPING.equals(tobb)));
		}
		
		ba.add(KEY_TYPES, types);
		return ba;
	}
	
	private JsonObject getIsArchitecture() {
		
		LOGGER.debug("Serializing Type Group 'IS Architecture'");
		
		JsonObject ba = new JsonObject();
		ba.add(KEY_NAMES, localize("global.applicationArchitecture"));
		
		JsonObject types = new JsonObject();
		for (TypeOfBuildingBlock tobb : Lists.newArrayList(
											TypeOfBuildingBlock.BUSINESSOBJECT,
											TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE,
											TypeOfBuildingBlock.INFORMATIONSYSTEM,
											TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE,
											TypeOfBuildingBlock.TRANSPORT
				)) {
			types.add(tobb.getAssociatedClass().getSimpleName(), typeJson(tobb, !Lists.newArrayList(TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE, TypeOfBuildingBlock.TRANSPORT).contains(tobb)));
		}
		
		ba.add(KEY_TYPES, types);
		return ba;
	}
	
	private JsonObject getProjectPortfolio() {
		
		LOGGER.debug("Serializing Type Group 'Project Portfolio'");
		
		JsonObject ppf = new JsonObject();
		ppf.add(KEY_NAMES, localize("global.projectPortfolio"));
		
		JsonObject types = new JsonObject();
		types.add(Project.class.getSimpleName(), typeJson(TypeOfBuildingBlock.PROJECT, true));
		
		ppf.add(KEY_TYPES, types);
		return ppf;
	}
	
	private JsonObject getInfrastructureArchitecture() {
		
		LOGGER.debug("Serializing Type Group 'Infrastructure Architecture'");
		
		JsonObject ppf = new JsonObject();
		ppf.add(KEY_NAMES, localize("global.infrastructureArchitecture"));
		
		JsonObject types = new JsonObject();
		types.add(InfrastructureElement.class.getSimpleName(), typeJson(TypeOfBuildingBlock.INFRASTRUCTUREELEMENT, true));
		
		ppf.add(KEY_TYPES, types);
		return ppf;
	}
	
	private JsonObject getTechnicalArchitecture() {
		
		LOGGER.debug("Serializing Type Group 'Technical Architecture'");
		
		JsonObject ppf = new JsonObject();
		ppf.add(KEY_NAMES, localize("global.technicalArchitecture"));
		
		JsonObject types = new JsonObject();
		types.add(TechnicalComponent.class.getSimpleName(), typeJson(TypeOfBuildingBlock.TECHNICALCOMPONENT, true));
		types.add(TechnicalComponentRelease.class.getSimpleName(), typeJson(TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE, true));
		
		ppf.add(KEY_TYPES, types);
		return ppf;
	}
	
	private JsonObject getDomainArchitecture() {
		
		LOGGER.debug("Serializing Type Group 'Domain Architecture'");
		
		JsonObject ba = new JsonObject();
		ba.add(KEY_NAME, toJ("Domains"));
		
		JsonObject types = new JsonObject();
		for (TypeOfBuildingBlock tobb : Lists.newArrayList(
											TypeOfBuildingBlock.ARCHITECTURALDOMAIN,
											TypeOfBuildingBlock.BUSINESSDOMAIN,
											TypeOfBuildingBlock.INFORMATIONSYSTEMDOMAIN
				)) {			
			types.add(tobb.getAssociatedClass().getSimpleName(), typeJson(tobb, true));
		}
		
		
		ba.add(KEY_TYPES, types);
		return ba;
	}
	
	private JsonObject getCrossLayerRelations() {
		
		LOGGER.debug("Serializing Type Group 'Cross Layer Relations'");
		
		JsonObject ba = new JsonObject();
		ba.add(KEY_NAME, toJ("Cross-Layer relationship types"));
		
		JsonObject types = new JsonObject();
		for (TypeOfBuildingBlock tobb : Lists.newArrayList(
											TypeOfBuildingBlock.ISR2BOASSOCIATION,
											TypeOfBuildingBlock.TCR2IEASSOCIATION
											
				)) {			
			types.add(tobb.getAssociatedClass().getSimpleName(), typeJson(tobb, false));
		}
		
		ba.add(KEY_TYPES, types);
		return ba;
	}
	
	private JsonObject typeJson(TypeOfBuildingBlock tobb, boolean withNameAndDescription) {
		
		LOGGER.debug("Serializing type " + tobb.getValue());
		
		JsonObject type = new JsonObject();
		BuildingBlockType bbt = bbtDao.getBuildingBlockTypeByType(tobb);
		
		type.add(KEY_NAMES, localize(typeKey(tobb)));
		type.add(KEY_PLURAL_NAMES, localize(pluralTypeKey(tobb)));
		type.add(KEY_ABBRS, localize(abbrKey(tobb)));
		type.add(KEY_DESCRIPTIONS, localize(descKey(tobb)));
		type.add(KEY_TOBB, toJ(tobb.getValue()));
		JsonObject features = new JsonObject();
		features.add(DEFAULT_FEATURE_GROUP, defaultFeatures(bbt, withNameAndDescription));				
		addFeatureGroups(features, bbt);		
		type.add(KEY_FEATURE_GROUPS, features);		
		
		return type;
	}
	
	private static JsonObject localize(String key) {
		JsonObject loc = new JsonObject();
		
		for (String l : Lists.newArrayList("en", "de")) {
			loc.add(l, toJ(MessageAccess.getStringOrNull(key, new Locale(l))));
		}
		
		return loc;
	}
	
	private static String key(TypeOfBuildingBlock tobb) {
		if (tobb.getValue().startsWith("global.")) {
			return tobb.getValue().substring(tobb.getValue().indexOf('.')+1);
		}
		return  tobb.getValue().substring(0, tobb.getValue().indexOf('.'));
	}
	
	private void addFeatureGroups(JsonObject features, BuildingBlockType bbt) {
		
		for (AttributeType at : bbt.getAttributeTypesAsList()) {
			AttributeTypeGroup atg = at.getAttributeTypeGroup();
			JsonObject jAtg = null;
			if (features.has(atg.getName())) {
				jAtg = features.get(atg.getName()).getAsJsonObject();
			}
			else {
				jAtg = new JsonObject();
				features.add(atg.getName(), jAtg);
			}
			if (customStartDates.containsKey(at)) {
				DateInterval di = customStartDates.get(at);
				jAtg.add(di.getName(), dateIntervalATJson(di));
			}
			else if (customEndDates.contains(at)) {
				//just ignore it
			}
			else {
				jAtg.add(at.getName(), atJson(at));				
			}
		}
	}

	private JsonElement dateIntervalATJson(DateInterval di) {
		JsonObject jdi = new JsonObject();
		
		jdi.add(KEY_NAME, toJ(di.getName()));
		jdi.add(KEY_TYPE, toJ(INTERVAL));
		jdi.add(KEY_DEFINING, toJ(false));
		jdi.add(KEY_SYSTEM_FEATURE, toJ(false));
		jdi.add(KEY_UNIQUE, toJ(false));
		jdi.add(KEY_MULTIPLICITY_MAND, toJ(false));
		jdi.add(KEY_MULTIPLICITY_MULT, toJ(false));
		jdi.add(KEY_TIMESERIES, toJ(false));
		if (di.getStartDate() != null && di.getEndDate() != null && di.getStartDate().getDescription() != null && di.getEndDate().getDescription() != null) {
			jdi.add(KEY_DESCRIPTION, toJ(String.format("%s\n%s", di.getStartDate().getDescription(), di.getEndDate().getDescription())));			
		}
		else {
			jdi.add(KEY_DESCRIPTION, toJ("Description for custom date interval " + di.getName()));			
		}
		
		return jdi;
	}

	private JsonObject defaultFeatures(BuildingBlockType bbt, boolean substantial) {
		TypeOfBuildingBlock tobb = bbt.getTypeOfBuildingBlock();
		LOGGER.debug("Serializing default features for " + tobb.getValue());
		JsonObject features = new JsonObject();		
		features.add(KEY_ID, idProp(substantial));
		if (substantial || Lists.newArrayList(TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE, TypeOfBuildingBlock.TRANSPORT).contains(tobb)) {
			if (!RELEASE_ELEMENTS.contains(bbt.getTypeOfBuildingBlock()) && !TypeOfBuildingBlock.TRANSPORT.equals(tobb)) {
				features.add(KEY_NAME, nameProp());				
			}			
			if (DIRECTION_ELEMENTS.contains(tobb)) {
				JsonObject direction = localizedProp("global.direction", "global.direction", Direction.class.getCanonicalName(), false, false, false, true, false, false);
				features.add(KEY_DIRECTION, direction);
			}
			if (!RELEASABLE_ELEMENTS.contains(tobb)) {
				features.add(ElasticMiConstants.PERSISTENT_NAME_DESCRIPTION, descProp());				
			}
		}	
		features.add(KEY_LMT, lmtProp());
		features.add(KEY_LMU, lmuProp());
		
		if (RELEASE_ELEMENTS.contains(tobb)) {
			addReleaseFeatures(features, tobb);
		}
		
		if (RELEASABLE_ELEMENTS.contains(tobb)) {
			addReleasalbeFeatures(features, tobb);
			if (TypeOfBuildingBlock.TECHNICALCOMPONENT.equals(tobb)) {
				JsonObject a4Isis = localizedProp("technicalComponentRelease.availableForInterfaces", "technicalComponentRelease.availableForInterfaces", BOOLEAN, false, false, false, true, false, false);
				features.add("availableForInterfaces", a4Isis);
			}
		}
		
		if (TypeOfBuildingBlock.PROJECT.equals(tobb)) {
			JsonObject rp = localizedProp("global.lifeTime", "global.lifeTime", INTERVAL, false, false, false, false, false, false);
			features.add("runtimePeriod", rp);
		}
		
		if (HIERARCHIC_ELEMENTS.contains(tobb)) {
			addHierarchyFeatures(features, tobb);
		}
		
		if (TypeOfBuildingBlock.BUSINESSMAPPING.equals(tobb)) {
			addBmRelEnds(features);
		}
		
		if (TypeOfBuildingBlock.ISR2BOASSOCIATION.equals(tobb)) {
			addIsr2BoRelEnds(features);
		}
		
		if (TypeOfBuildingBlock.TCR2IEASSOCIATION.equals(tobb)) {
			addTcr2IeRelEnds(features);
		}
		
		if (TypeOfBuildingBlock.ARCHITECTURALDOMAIN.equals(tobb)) {
			features.add("technicalComponentReleases", localizedRelEnd("technicalComponent.plural", "architecturalDomain.to.technicalComponentReleases", TechnicalComponentRelease.class.getSimpleName(), "architecturalDomains", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.BUSINESSDOMAIN.equals(tobb)) {
			features.add("businessUnits", localizedRelEnd("businessUnit.plural", "businessDomain.to.businessUnits", BusinessUnit.class.getSimpleName(), "businessDomains", false, false, false, true));
			features.add("businessFunctions", localizedRelEnd("businessFunction.plural", "businessDomain.to.businessFunctions", BusinessFunction.class.getSimpleName(), "businessDomains", false, false, false, true));
			features.add("businessProcesses", localizedRelEnd("businessProcess.plural", "businessDomain.to.businessProcesses", BusinessProcess.class.getSimpleName(), "businessDomains", false, false, false, true));
			features.add("products", localizedRelEnd("product.plural", "businessDomain.to.products", Product.class.getSimpleName(), "businessDomains", false, false, false, true));
		}
			
		if (TypeOfBuildingBlock.BUSINESSFUNCTION.equals(tobb)) {
			features.add("businessDomains", localizedRelEnd("businessDomain.plural", "businessFunction.to.businessDomains", BusinessDomain.class.getSimpleName(), "businessFunctions", false, false, false, true));
			features.add("businessObjects", localizedRelEnd("businessObject.plural", "businessFunction.to.businessObjects", BusinessObject.class.getSimpleName(), "businessFunctions", false, false, false, true));
			features.add("informationSystemReleases", localizedRelEnd("informationSystemRelease.plural", "businessFunction.to.informationSystemRelease", InformationSystemRelease.class.getSimpleName(), "businessFunctions", false, false, false, true));
		}

		if (TypeOfBuildingBlock.BUSINESSUNIT.equals(tobb)) {
			features.add("businessDomains", localizedRelEnd("businessDomain.plural", "businessUnit.to.businessDomains", BusinessDomain.class.getSimpleName(), "businessUnits", false, false, false, true));
			features.add("businessMappings", bmRelEnd("businessUnit"));
		}
		
		if (TypeOfBuildingBlock.BUSINESSOBJECT.equals(tobb)) {
			features.add("isr2BoAssociations", relEnd("isr2Bos", Isr2BoAssociation.class.getSimpleName(), "businessObject", "isr2Bos", false, false, false, true));
			features.add("generalisation", localizedRelEnd("businessObject.generalisation", "businessObject.generalisation", BusinessObject.class.getSimpleName(), "specialisations", false, false, false, false));
			features.add("specialisations", localizedRelEnd("businessObject.specialisations", "businessObject.specialisations", BusinessObject.class.getSimpleName(), "generalisation", false, false, false, true));
			features.add("businessFunctions", localizedRelEnd("businessFunction.plural", "businessObject.to.businessDomains", BusinessFunction.class.getSimpleName(), "businessFunctions", false, false, false, true));
			features.add("transports", localizedRelEnd("global.transport", "global.transport", Transport.class.getSimpleName(), "businessObject", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.BUSINESSPROCESS.equals(tobb)) {
			features.add("businessMappings", bmRelEnd("businessProcess"));
			features.add("businessDomains", localizedRelEnd("businessDomain.plural", "businessProcess.to.businessDomains", BusinessDomain.class.getSimpleName(), "businessProcesses", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.INFORMATIONSYSTEMDOMAIN.equals(tobb)) {
			features.add("informationSystemReleases", localizedRelEnd("informationSystemRelease.plural", "informationSystemDomain.to.informationSystemReleases", InformationSystemRelease.class.getSimpleName(), "informationSystemDomains", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE.equals(tobb)) {
			features.add("informationSystemReleaseA", localizedRelEnd("interface.releaseA", "interface.releaseA", InformationSystemRelease.class.getSimpleName(), "interfacesReleaseA", false, false, true, false));
			features.add("informationSystemReleaseB", localizedRelEnd("interface.releaseB", "interface.releaseB", InformationSystemRelease.class.getSimpleName(), "interfacesReleaseB", false, false, true, false));
			features.add("technicalComponentReleases", localizedRelEnd("technicalComponentRelease.plural", "interface.to.technicalComponentReleases", TechnicalComponentRelease.class.getSimpleName(), "informationSystemInterfaces", false, false, false, true));
			features.add("transports", localizedRelEnd("global.transport", "global.transport", Transport.class.getSimpleName(), "informationSystemInterface", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE.equals(tobb)) {
			features.add("businessMappings", bmRelEnd("informationSystemRelease"));
			features.add("isr2BoAssociations", relEnd("isr2Bos", Isr2BoAssociation.class.getSimpleName(), "informationSystemRelease", "isr2Bos", false, false, false, true));
			features.add("interfacesReleaseA", relEnd("releases A", "releases A", InformationSystemInterface.class.getSimpleName(), "", false, false, false, true));
			features.add("interfacesReleaseB", relEnd("releases B", "releases B", InformationSystemInterface.class.getSimpleName(), "", false, false, false, true));
			features.add("parentComponents", localizedRelEnd("global.usedBy", "informationSystemRelease.parentComponents", tobb.getAssociatedClass().getSimpleName(), "baseComponents", false, false, false, true));
			features.add("baseComponents", localizedRelEnd("global.uses", "informationSystemRelease.baseComponents", tobb.getAssociatedClass().getSimpleName(), "parentComponents", false, false, false, true));
			features.add("businessFunctions", localizedRelEnd("businessFunction.plural", "informationSystemRelease.to.businessFunctions", BusinessFunction.class.getSimpleName(), "informationSystemReleases", false, false, false, true));
			features.add("infrastructureElements", localizedRelEnd("infrastructureElement.plural", "informationSystemRelease.to.infrastructureElements", InfrastructureElement.class.getSimpleName(), "informationSystemReleases", false, false, false, true));
			features.add("informationSystemDomains", localizedRelEnd("informationSystemDomain.plural", "informationSystemRelease.to.informationSystemDomains", InformationSystemDomain.class.getSimpleName(), "informationSystemReleases", false, false, false, true));
			features.add("projects", localizedRelEnd("project.plural", "informationSystemRelease.to.projects", Project.class.getSimpleName(), "informationSystemReleases", false, false, false, true));
			features.add("successors", localizedRelEnd("global.successors", "informationSystemRelease.successors", InformationSystemRelease.class.getSimpleName(), "predecessors", false, false, false, true));
			features.add("predecessors", localizedRelEnd("global.predecessors", "informationSystemRelease.predecessors", InformationSystemRelease.class.getSimpleName(), "successors", false, false, false, true));
			features.add("technicalComponentReleases", localizedRelEnd("technicalComponent.plural", "informationSystemRelease.to.technicalComponentReleases", TechnicalComponentRelease.class.getSimpleName(), "informationSystemReleases", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.INFRASTRUCTUREELEMENT.equals(tobb)) {
			features.add("tcr2IeAssociations", relEnd("tcr2Ies", Isr2BoAssociation.class.getSimpleName(), "infrastructureElement", "tcr2Ies", false, false, false, true));
			features.add("parentComponents", localizedRelEnd("global.usedBy", "infrastructureElement.parentComponents", InfrastructureElement.class.getSimpleName(), "baseComponents", false, false, false, true));
			features.add("baseComponents", localizedRelEnd("global.uses", "infrastructureElement.baseComponents", InfrastructureElement.class.getSimpleName(), "parentComponents", false, false, false, true));
			features.add("informationSystemReleases", localizedRelEnd("informationSystemRelease.plural", "infrastructureElement.to.informationSystemReleases", InfrastructureElement.class.getSimpleName(), "infrastructureElements", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.PRODUCT.equals(tobb)) {
			features.add("businessMappings", bmRelEnd("product"));
			features.add("businessDomains", localizedRelEnd("businessDomain.plural", "product.to.businessDomains", BusinessDomain.class.getSimpleName(), "products", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.PROJECT.equals(tobb)) {
			features.add("informationSystemReleases", localizedRelEnd("informationSystemRelease.plural", "project.to.informationSystemReleases", InformationSystemRelease.class.getSimpleName(), "projects", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE.equals(tobb)) {
			features.add("tcr2IeAssociations", relEnd("tcr2Ies", Isr2BoAssociation.class.getSimpleName(), "technicalComponentRelease", "tcr2Ies", false, false, false, true));
			features.add("informationSystemInterfaces", localizedRelEnd("interface.plural", "technicalComponentRelease.to.informationSystemInterfaces", TechnicalComponentRelease.class.getSimpleName(), "technicalComponentReleases", false, false, false, true));
			features.add("informationSystemReleases", localizedRelEnd("informationSystemRelease.plural", "technicalComponentRelease.to.informationSystemReleases", InformationSystemRelease.class.getSimpleName(), "technicalComponentReleases", false, false, false, true));
			features.add("architecturalDomains", localizedRelEnd("architecturalDomain.plural", "technicalComponentRelease.to.architecturalDomains", ArchitecturalDomain.class.getSimpleName(), "technicalComponentReleases", false, false, false, true));
			features.add("parentComponents", localizedRelEnd("global.usedBy", "technicalComponentRelease.parentComponents", TechnicalComponentRelease.class.getSimpleName(), "baseComponents", false, false, false, true));
			features.add("baseComponents", localizedRelEnd("global.uses", "technicalComponentRelease.baseComponents", TechnicalComponentRelease.class.getSimpleName(), "parentComponents", false, false, false, true));
			features.add("successors", localizedRelEnd("global.successors", "technicalComponentRelease.successors", TechnicalComponentRelease.class.getSimpleName(), "predecessors", false, false, false, true));
			features.add("predecessors", localizedRelEnd("global.predecessors", "technicalComponentRelease.predecessors", TechnicalComponentRelease.class.getSimpleName(), "successors", false, false, false, true));
		}
		
		if (TypeOfBuildingBlock.TRANSPORT.equals(tobb)) {
			features.add("businessObject", localizedRelEnd("businessObject.singular", "businessObject.singular", BusinessObject.class.getSimpleName(), "transports", true, false, true, false));
			features.add("informationSystemInterface", localizedRelEnd("interface.singular", "interface.singular", InformationSystemInterface.class.getSimpleName(), "transports", true, false, true, false));
		}
		
		for (AttributeType at : bbt.getAttributeTypesAsList()) {
			if (at.getAttributeTypeGroup().isToplevelATG()) {
				features.add(at.getName(), atJson(at));
			}
		}
		
		return features;
	}
	
	private JsonObject bmRelEnd(String oppositeName) {
		return localizedRelEnd("global.business_mappings", "global.business_mappings", BusinessMapping.class.getSimpleName(), oppositeName, false, false, false, true);
	}

	private void addBmRelEnds(JsonObject features) {
		JsonObject bp = localizedRelEnd("businessProcess.singular", "businessProcess.singular", BusinessProcess.class.getSimpleName(), "businessMappings", true, false, false, true);
		JsonObject isr = localizedRelEnd("informationSystem.singular", "informationSystem.singular", InformationSystemRelease.class.getSimpleName(), "businessMappings", true, false, false, true);
		JsonObject bu = localizedRelEnd("businessUnit.singular", "businessUnit.singular", BusinessUnit.class.getSimpleName(), "businessMappings", true, false, false, true);
		JsonObject prod = localizedRelEnd("product.singular", "product.singular", Product.class.getSimpleName(), "businessMappings", true, false, false, true);
		features.add("businessProcess", bp);
		features.add("informationSystemRelease", isr);
		features.add("businessUnit", bu);
		features.add("product", prod);
	}
	
	private void addIsr2BoRelEnds(JsonObject features) {
		JsonObject isr = localizedRelEnd("informationSystem.singular", "informationSystem.singular", InformationSystemRelease.class.getSimpleName(), "isr2BoAssociations", true, false, false, true);
		JsonObject bo = localizedRelEnd("businessObject.singular", "businessObject.singular", BusinessObject.class.getSimpleName(), "isr2BoAssociations", true, false, false, true);
		features.add("informationSystemRelease", isr);
		features.add("businessObject", bo);
	}
	
	private void addTcr2IeRelEnds(JsonObject features) {
		JsonObject tcr = localizedRelEnd("technicalComponent.singular", "technicalComponent.singular", TechnicalComponentRelease.class.getSimpleName(), "tcr2IeAssociations", true, false, false, true);
		JsonObject ie = localizedRelEnd("infrastructureElement.singular", "infrastructureElement.singular", InfrastructureElement.class.getSimpleName(), "tcr2IeAssociations", true, false, false, true);
		features.add("informationSystemRelease", tcr);
		features.add("businessObject", ie);
	}

	private void addReleasalbeFeatures(JsonObject features,
			TypeOfBuildingBlock tobb) {
		
		JsonObject releases = relEnd("releases", (TypeOfBuildingBlock.INFORMATIONSYSTEM.equals(tobb) ? InformationSystemRelease.class.getSimpleName() : TechnicalComponentRelease.class.getSimpleName()), (TypeOfBuildingBlock.INFORMATIONSYSTEM.equals(tobb) ? "informationSystem" : "technicalComponent"), "Releases of element", false, false, false, true);
		features.add("releases", releases);
	}

	private void addReleaseFeatures(JsonObject features, TypeOfBuildingBlock tobb) {

		JsonObject release = localizedProp("global.version", "global.version", STRING, false, false, false, false, false, false);
		features.add("release", release);
		
		JsonObject rp = localizedProp("global.lifeTime", "global.lifeTime", INTERVAL, false, false, false, false, false, false);
		features.add("runtimePeriod", rp);
		
		JsonObject status = localizedProp("global.type_of_status", "global.type_of_status", tobb.getAssociatedClass().getCanonicalName() + ".TypeOfStatus", false, false, false, true, false, false);
		features.add("typeOfStatus", status);
		
		if (TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE.equals(tobb)) {
			JsonObject is = relEnd("information System", InformationSystem.class.getSimpleName(), "releases", "Belongs to Informationsystem", false, false, true, false);
			features.add("informationSystem", is);
		}
		else if (TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE.equals(tobb)) {
			JsonObject tc = relEnd("technical Component", TechnicalComponent.class.getSimpleName(), "releases", "Belongs to TechnicalComponent", false, false, true, false);
			features.add("technicalComponent", tc);
		}
	}

	private JsonObject atJson(AttributeType at) {
		JsonObject jAt = new JsonObject();
		
		jAt.add(KEY_NAME, toJ(at.getName()));
		jAt.add(KEY_TYPE, toJ(typeString(at)));
		jAt.add(KEY_DEFINING, toJ(false));
		jAt.add(KEY_SYSTEM_FEATURE, toJ(false));
		jAt.add(KEY_UNIQUE, toJ(false));
		jAt.add(KEY_MULTIPLICITY_MAND, toJ(at.isMandatory()));
		jAt.add(KEY_MULTIPLICITY_MULT, toJ(isMulti(at)));
		jAt.add(KEY_TIMESERIES, toJ(timeseries(at)));
		jAt.add(KEY_DESCRIPTION, toJ(at.getDescription()));
		
		return jAt;
	}

	private boolean timeseries(AttributeType at) {
		if (NumberAT.class.isInstance(at) && ((NumberAT)at).isTimeseries()) {
			return true;
		}
		return false;
	}

	private Boolean isMulti(AttributeType at) {
		if (EnumAT.class.isInstance(at) && ((EnumAT)at).isMultiassignmenttype()) {
			return true;
		}
		else if (ResponsibilityAT.class.isInstance(at) && ((ResponsibilityAT)at).isMultiassignmenttype()) {
			return true;
		}
		
		return false;
	}

	private String typeString(AttributeType at) {
		if (EnumAT.class.isInstance(at)) {
			return "de.iteratec.iteraplan.model.attribute.EnumAT."+at.getName();
		}
		if (NumberAT.class.isInstance(at)) {
			return DECIMAL;
		}		
		if (ResponsibilityAT.class.isInstance(at)) {
			return RESPONSIBILITY;
		}
		if (DateAT.class.isInstance(at)) {
			return DATE;
		}
		if (TextAT.class.isInstance(at) && ((TextAT)at).isMultiline()) {
			return RICHTEXT;
		}
		return STRING;
	}

	private static JsonObject idProp(boolean substantial) {
		JsonObject id = localizedProp("global.id", "global.id", INTEGER, substantial, false, true, true, false, false);
		return id;
	}
	
	private static JsonObject nameProp() {
		JsonObject name = localizedProp("global.name", "global.name", STRING, false, false, true, true, false, false);
		return name;
	}
	
	private static JsonObject descProp() {
		JsonObject description = localizedProp("global.description", "global.description", RICHTEXT, false, false, false, false, false, false);
		return description;
	}
	
	private static JsonObject lmuProp() {
		JsonObject lmu = localizedProp("global.lastModificationUser", "global.lastModificationUser", STRING, false, true, false, true, false, false);
		return lmu;
	}
	
	private static JsonObject lmtProp() {
		JsonObject lmt = localizedProp("global.lastModificationTime", "global.lastModificationTime", DATETIME, false, true, false, true, false, false);		
		return lmt;
	}

	private static void addHierarchyFeatures(JsonObject features, TypeOfBuildingBlock tobb) {
		
		if (!TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE.equals(tobb)) {
			JsonObject pos = localizedProp("reports.position", "reports.position", INTEGER, false, false, false, true, false, false);		
			features.add(KEY_POSITION, pos);			
		}
		
		String pKey = key(tobb) + ".parent";
		String cKey = key(tobb) + ".children";
		if (TypeOfBuildingBlock.BUSINESSFUNCTION.equals(tobb)) {
			pKey = "graphicalReport.business_function.parent";
			cKey = "graphicalReport.business_function.children";
		}
		else if (TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE.equals(tobb)) {
			pKey = "graphicalReport.informationSystemRelease.parent";
			cKey = "graphicalReport.informationSystemRelease.children";
		}
		JsonObject parent = localizedRelEnd(pKey, pKey, tobb.getAssociatedClass().getSimpleName(), "children", false, false, false, false);		
		features.add(KEY_PARENT, parent);
		
		JsonObject children = localizedRelEnd(cKey, cKey, tobb.getAssociatedClass().getSimpleName(), "parent", false, false, false, true);
		features.add(KEY_CHILDREN, children);
	}
	
	@Override
	public boolean supports(ResourceType resourceType) {
		return ResourceType.LEAN_METMODEL.equals(resourceType);
	}

	private JsonObject getEnums() {
		
		LOGGER.debug("Exporting Enums");
		
		JsonObject enums = new JsonObject();
		enums.add(TypeOfStatus.class.getCanonicalName(), getIsrTosEnum());
		enums.add(de.iteratec.iteraplan.model.TechnicalComponentRelease.TypeOfStatus.class.getCanonicalName(), getTcrTosEnum());
		enums.add(Direction.class.getCanonicalName(), getDirectionEnum());
		for (AttributeType att : atDao.loadElementList(null)) {
			if (EnumAT.class.isInstance(att)) {
				LOGGER.debug("exporting custom EnumAT " + att.getName());
				EnumAT at = (EnumAT)att;
				enums.add("de.iteratec.iteraplan.model.attribute.EnumAT."+at.getName(), jsonObjectForEnumAT(at));
			}
		}
		
		LOGGER.debug("Finished Exporting of Enums");
		
		return enums;
	}

	private JsonObject jsonObjectForEnumAT(EnumAT at) {
		JsonObject enumm = new JsonObject();
		
		enumm.add(KEY_NAME, toJ(at.getName()));				
		enumm.add(KEY_DESCRIPTION, toJ(at.getDescription()));
		enumm.add(KEY_ORDINAL, toJ(false));
		
		JsonObject literals = new JsonObject();
		
		
		for (EnumAV av : at.getSortedAttributeValues()) {
			LOGGER.debug("Exporting literal " + av.getName());
			JsonObject jAv = new JsonObject();
			jAv.add(KEY_NAME, toJ(av.getName()));
			jAv.add(KEY_DESCRIPTION, toJ(av.getDescription()));
			jAv.add(KEY_INDEX, toJ(av.getPosition())); 
			Color c = Color.decode("#"+av.getDefaultColorHex());
			jAv.add(KEY_COLOR, toJ(String.format("rgb(%d,%d,%d)", c.getRed(), c.getGreen(), c.getBlue())));
			literals.add(av.getName(), jAv);
		}
		LOGGER.debug("finished serialization of literals");
		enumm.add(KEY_LITERALS, literals);
		
		return enumm;
	}

	private static JsonElement getIsrTosEnum() {
		JsonObject enumm = new JsonObject();
		
		enumm.add(KEY_NAME, toJ(TypeOfStatus.class.getCanonicalName()));
		enumm.add(KEY_DESCRIPTION, toJ("Enum to determine the lifecycle status of an Informationsystem"));
		enumm.add(KEY_ORDINAL, toJ(false));
		
		JsonObject literals = new JsonObject();
		
		JsonObject current = new JsonObject();
		current.add(KEY_NAME, toJ("Current"));
		current.add(KEY_DESCRIPTION, toJ("System is live"));
		current.add(KEY_INDEX, toJ(0));
		current.add(KEY_COLOR, toJ("rgb(175,206,168)"));
		literals.add("CURRENT", current);
		
		JsonObject planned = new JsonObject();
		planned.add(KEY_NAME, toJ("Planned"));
		planned.add(KEY_DESCRIPTION, toJ("System is planned"));
		planned.add(KEY_INDEX, toJ(1));
		planned.add(KEY_COLOR, toJ("rgb(246,223,149)"));
		literals.add("PLANNED", planned);
		
		JsonObject target = new JsonObject();
		target.add(KEY_NAME, toJ("Target"));
		target.add(KEY_DESCRIPTION, toJ("System is part of the target architecture"));
		target.add(KEY_INDEX, toJ(2));
		target.add(KEY_COLOR, toJ("rgb(215,157,173)"));
		literals.add("TARGET", target);
		
		JsonObject inactive = new JsonObject();
		inactive.add(KEY_NAME, toJ("Inactive"));
		inactive.add(KEY_DESCRIPTION, toJ("System is outdated"));
		inactive.add(KEY_INDEX, toJ(3));
		inactive.add(KEY_COLOR, toJ("rgb(136,174,217)"));
		literals.add("INACTIVE", inactive);		
		
		enumm.add(KEY_LITERALS, literals);
		
		return enumm;
	}
	
	private static JsonElement getTcrTosEnum() {
		JsonObject enumm = new JsonObject();
		
		enumm.add(KEY_NAME, toJ(de.iteratec.iteraplan.model.TechnicalComponentRelease.TypeOfStatus.class.getCanonicalName()));
		enumm.add(KEY_DESCRIPTION, toJ("Enum to determine the lifecycle status of an TechnicalComponent"));
		enumm.add(KEY_ORDINAL, toJ(false));
		
		JsonObject literals = new JsonObject();
		
		JsonObject current = new JsonObject();
		current.add(KEY_NAME, toJ("Current"));
		current.add(KEY_DESCRIPTION, toJ("Component is live"));
		current.add(KEY_INDEX, toJ(0));
		current.add(KEY_COLOR, toJ("rgb(175,206,168)"));
		literals.add("CURRENT", current);
		
		JsonObject planned = new JsonObject();
		planned.add(KEY_NAME, toJ("Planned"));
		planned.add(KEY_DESCRIPTION, toJ("Component is planned"));
		planned.add(KEY_INDEX, toJ(1));
		planned.add(KEY_COLOR, toJ("rgb(246,223,149)"));
		literals.add("PLANNED", planned);
		
		JsonObject target = new JsonObject();
		target.add(KEY_NAME, toJ("Target"));
		target.add(KEY_DESCRIPTION, toJ("Component is part of the target architecture"));
		target.add(KEY_INDEX, toJ(2));
		target.add(KEY_COLOR, toJ("rgb(215,157,173)"));
		literals.add("TARGET", target);
		
		JsonObject inactive = new JsonObject();
		inactive.add(KEY_NAME, toJ("Inactive"));
		inactive.add(KEY_DESCRIPTION, toJ("Component is outdated"));
		inactive.add(KEY_INDEX, toJ(3));
		inactive.add(KEY_COLOR, toJ("rgb(136,174,217)"));
		literals.add("INACTIVE", inactive);		
		
		JsonObject undefined = new JsonObject();
		undefined.add(KEY_NAME, toJ("Undefined"));
		undefined.add(KEY_DESCRIPTION, toJ("Status of Component not defined"));
		undefined.add(KEY_INDEX, toJ(4));
		undefined.add(KEY_COLOR, toJ("rgb(172,161,200)"));
		literals.add("UNDEFINED", undefined);	
		
		enumm.add(KEY_LITERALS, literals);
		
		return enumm;
	}
	
	private static JsonElement getDirectionEnum() {
		JsonObject enumm = new JsonObject();
		
		enumm.add(KEY_NAME, toJ(Direction.class.getCanonicalName()));
		enumm.add(KEY_DESCRIPTION, toJ("Enum to determine the direction of an InformationFlow"));
		enumm.add(KEY_ORDINAL, toJ(false));
		
		JsonObject literals = new JsonObject();
		
		JsonObject noDirection = new JsonObject();
		noDirection.add(KEY_NAME, toJ("NoDirection"));
		noDirection.add(KEY_DESCRIPTION, toJ("Information flow does not have a direction"));
		noDirection.add(KEY_INDEX, toJ(0));
		noDirection.add(KEY_COLOR, toJ("rgb(175,206,168)"));
		literals.add("NO_DIRECTION", noDirection);
		
		JsonObject f2s = new JsonObject();
		f2s.add(KEY_NAME, toJ("FirstToSecond"));
		f2s.add(KEY_DESCRIPTION, toJ("From first to second"));
		f2s.add(KEY_INDEX, toJ(1));
		f2s.add(KEY_COLOR, toJ("rgb(246,223,149)"));
		literals.add("FIRST_TO_SECOND", f2s);
		
		JsonObject s2f = new JsonObject();
		s2f.add(KEY_NAME, toJ("SecondToFirst"));
		s2f.add(KEY_DESCRIPTION, toJ("From second to first"));
		s2f.add(KEY_INDEX, toJ(2));
		s2f.add(KEY_COLOR, toJ("rgb(215,157,173)"));
		literals.add("SECOND_TO_FIRST", s2f);
		
		JsonObject both = new JsonObject();
		both.add(KEY_NAME, toJ("Both"));
		both.add(KEY_DESCRIPTION, toJ("Bidirectional"));
		both.add(KEY_INDEX, toJ(3));
		both.add(KEY_COLOR, toJ("rgb(136,174,217)"));
		literals.add("INACTIVE", both);		
				
		enumm.add(KEY_LITERALS, literals);
		
		return enumm;
	}
	
	private JsonObject getAtgs() {
		
		LOGGER.debug("Serializing AttributeTypeGroups");
		
		JsonObject atgs = new JsonObject();
		
		for (AttributeTypeGroup atg : atGroupDao.loadElementList(null)) {
			
			LOGGER.debug("Serializing ATG " + atg.getName());
			
			JsonObject jAtg = new JsonObject();
			jAtg.add(KEY_NAME, toJ(atg.getName())); 
			jAtg.add(KEY_INDEX, toJ(atg.getPosition())); 
			jAtg.add(KEY_IS_TOPLEVEL_ATG, toJ(atg.isToplevelATG()));
			
			JsonArray attributeTypes = new JsonArray();
			for (AttributeType at : atg.getAttributeTypes()) {
				LOGGER.debug("processing AT " + at.getName());
				if (customStartDates.containsKey(at)) {
					attributeTypes.add(toJ(customStartDates.get(at).getName()));
				}
				else if (customEndDates.contains(at)) {
					// ignore 
				}
				else {					
					attributeTypes.add(toJ(at.getName()));
				}
			}
			jAtg.add(KEY_ATTRIBUTE_TYPES, attributeTypes);
			
			JsonArray readRoles = new JsonArray();
			for (Integer roleId : atg.getRoleIdsWithReadPermissionNotAggregated()) {
				if (roleId != null) {
					readRoles.add(toJ(roleId)); 					
				}
			}
			jAtg.add(KEY_READ_ROLES, readRoles);
			JsonArray writeRoles = new JsonArray();
			for (Integer roleId : atg.getRoleIdsWithWritePermissionNotAggregated()) {
				if (roleId != null) {
					writeRoles.add(toJ(roleId));					
				}
			}
			jAtg.add(KEY_WRITE_ROLES, writeRoles);
			
			LOGGER.debug("finished serialization of ATG " + atg.getName());
			
			atgs.add(atg.getName(), jAtg);
		}
		
		LOGGER.debug("Finished serialization of AttributeTypeGroups");
		
		return atgs;
	}
	
	public void setAtGroupDao(AttributeTypeGroupDAO atGroupDao) {
		this.atGroupDao = atGroupDao;
	}

	public void setAtDao(AttributeTypeDAO atDao) {
		this.atDao = atDao;
	}

	public void setBbtDao(BuildingBlockTypeDAO bbtDao) {
		this.bbtDao = bbtDao;
	}

	public void setRespHandler(JsonResponsibilityRepresentationHandler respHandler) {
		this.respHandler = respHandler;
	}
	
	private static JsonObject relEnd(String name, String type, String oppositePersistentName, String description, boolean defining, boolean unique, boolean mandatory, boolean multivalue) {
		JsonObject feature = new JsonObject();
		feature.add(KEY_NAME, toJ(name));
		feature.add(KEY_TYPE, toJ(type));
		feature.add(KEY_OPPOSITE, toJ(oppositePersistentName));
		feature.add(KEY_DEFINING, toJ(defining));
		feature.add(KEY_SYSTEM_FEATURE, toJ(false));
		feature.add(KEY_UNIQUE, toJ(unique));
		feature.add(KEY_MULTIPLICITY_MAND, toJ(mandatory));
		feature.add(KEY_MULTIPLICITY_MULT, toJ(multivalue));
		feature.add(KEY_TIMESERIES, toJ(false));
		feature.add(KEY_DESCRIPTION, toJ(description));
		
		return feature;
	}
	
	private static JsonObject localizedProp(String nameKey, String descriptionKey, String type, boolean defining, boolean systemFeature, boolean unique, boolean mandatory, boolean multivalue, boolean timeseries) {
		JsonObject feature = new JsonObject();
		feature.add(KEY_NAMES, localize(nameKey));
		feature.add(KEY_TYPE, toJ(type));
		feature.add(KEY_DEFINING, toJ(defining));
		feature.add(KEY_SYSTEM_FEATURE, toJ(systemFeature));
		feature.add(KEY_UNIQUE, toJ(unique));
		feature.add(KEY_MULTIPLICITY_MAND, toJ(mandatory));
		feature.add(KEY_MULTIPLICITY_MULT, toJ(multivalue));
		feature.add(KEY_TIMESERIES, toJ(timeseries));
		feature.add(KEY_DESCRIPTION, localize(descriptionKey));
		
		return feature;
	}
	
	private static JsonObject localizedRelEnd(String nameKey, String descriptionKey, String type, String oppositePersistentName, boolean defining, boolean unique, boolean mandatory, boolean multivalue) {
		JsonObject feature = new JsonObject();
		feature.add(KEY_NAMES, localize(nameKey));
		feature.add(KEY_TYPE, toJ(type));
		feature.add(KEY_OPPOSITE, toJ(oppositePersistentName));
		feature.add(KEY_DEFINING, toJ(defining));
		feature.add(KEY_SYSTEM_FEATURE, toJ(false));
		feature.add(KEY_UNIQUE, toJ(unique));
		feature.add(KEY_MULTIPLICITY_MAND, toJ(mandatory));
		feature.add(KEY_MULTIPLICITY_MULT, toJ(multivalue));
		feature.add(KEY_TIMESERIES, toJ(false));
		feature.add(KEY_DESCRIPTION, localize(descriptionKey));
		
		return feature;
	}
	
	private static String typeKey(TypeOfBuildingBlock tobb) {
		String key = key(tobb) + ".singular";
		
		if (Lists.newArrayList(TypeOfBuildingBlock.BUSINESSFUNCTION, TypeOfBuildingBlock.PRODUCT, TypeOfBuildingBlock.TRANSPORT).contains(tobb)) {
			key = tobb.getValue();
		}		
		return key;
	}
	
	private static String pluralTypeKey(TypeOfBuildingBlock tobb) {
		String key = key(tobb) + ".plural";
		
		if (TypeOfBuildingBlock.PRODUCT.equals(tobb)) {
			key = "product.plural";
		}		
		else if (TypeOfBuildingBlock.TRANSPORT.equals(tobb)) {
			key = tobb.getValue(); // no plural name available
		}			
		else if (TypeOfBuildingBlock.BUSINESSFUNCTION.equals(tobb)) {
			key = "businessFunction.plural";
		}
		else if (TypeOfBuildingBlock.INFORMATIONSYSTEM.equals(tobb)) {
			key = "informationSystemRelease.plural";
		}
			
		return key;
	}
	
	
	private static String abbrKey(TypeOfBuildingBlock tobb) {
		String key = key(tobb) + ".abbr";
		
		if (TypeOfBuildingBlock.BUSINESSFUNCTION.equals(tobb)) {
			key = "businessFunction.abbr";
		}
		else if (TypeOfBuildingBlock.INFORMATIONSYSTEM.equals(tobb)) {
			key = "informationSystemRelease.abbr";
		}
		else if (TypeOfBuildingBlock.TECHNICALCOMPONENT.equals(tobb)) {
			key = "technicalComponentRelease.abbr";
		}		
		else if (TypeOfBuildingBlock.TRANSPORT.equals(tobb)) {
			key = "global.transport"; //no abbr, use name instead
		}
		
		return key;
	}

	private static String descKey(TypeOfBuildingBlock tobb) {
		String key = "glossary." + key(tobb);
		
		if (TypeOfBuildingBlock.BUSINESSFUNCTION.equals(tobb)) {
			key = "glossary.business_function.description";
		}
		else if (TypeOfBuildingBlock.PRODUCT.equals(tobb)) {
			key = "glossary.product.description";
		}
		else if (TypeOfBuildingBlock.BUSINESSMAPPING.equals(tobb)) {
			key = tobb.getValue(); //no desc available, use name instead
		}
		else if (TypeOfBuildingBlock.INFORMATIONSYSTEM.equals(tobb)) {
			return descKey(TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE);
		}
		else if (TypeOfBuildingBlock.TECHNICALCOMPONENT.equals(tobb)) {
			return descKey(TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE);
		}
		else if (TypeOfBuildingBlock.TRANSPORT.equals(tobb)) {
			key = "global.transport"; //no desc, use name instead
		}
		else if (TypeOfBuildingBlock.ARCHITECTURALDOMAIN.equals(tobb)) {
			key = "glossary.arch_domain.description";
		}		
		
		return key;
	}

	public void setDateIntervalDao(DateIntervalDAO dateIntervalDao) {
		this.dateIntervalDao = dateIntervalDao;
	}
	
	private static final JsonPrimitive toJ(String string) {
		
		if (string == null) {
			LOGGER.warn("Tried to serialize null to JSON");
			return new JsonPrimitive("");
		}
		if (string.trim().isEmpty()) {
			LOGGER.debug("Tried to serialize empty string to JSON");
			return new JsonPrimitive("");
		}
		return new JsonPrimitive(string);
		
	}
	
	private static final JsonPrimitive toJ(boolean bool) {
		return new JsonPrimitive(bool);
	}
	
	private static final JsonPrimitive toJ(int i) {
		return new JsonPrimitive(i);
	}
	
	
	
	
}

