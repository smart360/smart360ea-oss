/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.presentation;

import java.util.List;
import java.util.Set;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import de.iteratec.iteraplan.model.user.Role;
import de.iteratec.iteraplan.persistence.dao.RoleDAO;


public class SmartLdapUserDetailsService implements UserDetailsService {

  private final FilterBasedLdapUserSearch       ldapSearch;
  private final DefaultLdapAuthoritiesPopulator populator;
  private final RoleDAO                         roleDao;

  public SmartLdapUserDetailsService(FilterBasedLdapUserSearch ldapSearch, DefaultLdapAuthoritiesPopulator populator, RoleDAO roleDao) {
    this.ldapSearch = ldapSearch;
    this.populator = populator;
    this.roleDao = roleDao;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    DirContextOperations ops = ldapSearch.searchForUser(username);
    IteraplanLdapUserDetails.Essence ess = new IteraplanLdapUserDetails.Essence(ops);

    ess.setUsername(username);
    List<GrantedAuthority> auths = Lists.newArrayList();
    Set<String> roleNames = getRoleNames();
    for (GrantedAuthority auth : populator.getGrantedAuthorities(ops, username)) {
      if (roleNames.contains(auth.getAuthority())) {
        auths.add(auth);
      }
    }
    ess.setAuthorities(auths);

    return ess.createUserDetails();
  }

  private Set<String> getRoleNames() {
    Set<String> roleNames = Sets.newHashSet();
    for (Role role : roleDao.loadElementList(null)) {
      roleNames.add(role.getRoleName());
    }
    return roleNames;
  }

}
