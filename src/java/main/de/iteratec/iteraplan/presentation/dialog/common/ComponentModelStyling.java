package de.iteratec.iteraplan.presentation.dialog.common;

import java.io.Serializable;


public abstract class ComponentModelStyling implements Serializable {

  private static final long                 serialVersionUID = -3167937393093345101L;

  public static final ComponentModelStyling NO_STYLING       = new NoStyling();

  private final String                      iconCssClass;

  protected ComponentModelStyling(String iconCssClass) {
    this.iconCssClass = iconCssClass;
  }

  public String getIconCssClass() {
    return this.iconCssClass;
  }

  private static final class NoStyling extends ComponentModelStyling {

    private static final long serialVersionUID = 5171909707684509825L;

    NoStyling() {
      super("");
    }
  }

  public static class SimpleComponentModelStyling extends ComponentModelStyling {

    private static final long serialVersionUID = 3601624694211699284L;

    public SimpleComponentModelStyling(String iconCssClass) {
      super(iconCssClass);
    }
  }

}
