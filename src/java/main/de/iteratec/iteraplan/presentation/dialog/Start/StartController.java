/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.presentation.dialog.Start;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import de.iteratec.iteraplan.businesslogic.service.OverviewService;
import de.iteratec.iteraplan.common.Dialog;
import de.iteratec.iteraplan.presentation.dialog.GuiController;
import de.iteratec.iteraplan.presentation.memory.StartDialogMemory;


/**
 * A very simple controller for the welcome & overview page. Its only purpose is to 
 * supply information to the menu highlighting functionality, i.e. update GUI context.
 * Also, to provide a start dialog memory, containing the counts for different building blocks.
 */
@Controller
public class StartController extends GuiController {

  @Autowired
  private OverviewService overviewService;

  @Override
  protected String getDialogName() {
    return Dialog.START.getDialogName();
  }

  @RequestMapping
  public void start(ModelMap model, HttpSession session, HttpServletRequest request, @ModelAttribute("dialogMemory")
  StartDialogMemory dialogMemory) {
    super.init(model, session, request);

    updateGuiContext(createOrUpdateStartDialogMemory(dialogMemory, request.getContextPath()));
  }

  private StartDialogMemory createOrUpdateStartDialogMemory(StartDialogMemory source, String contextPath) {
    StartDialogMemory sdm;
    if (source == null) {
      sdm = new StartDialogMemory();
    }
    else {
      sdm = source;
    }

    sdm.setEaDataOverviewTiles(overviewService.getEaDataOverviewTiles(contextPath));
    sdm.setVisualizationsOverviewTiles(overviewService.getVisualizationsOverviewTiles(contextPath));

    return sdm;
  }
}
