package de.iteratec.iteraplan.presentation.memory;

import java.util.List;

import de.iteratec.iteraplan.common.Dialog;
import de.iteratec.iteraplan.model.OverviewTile;


public class StartDialogMemory extends DialogMemory {

  private static final long  serialVersionUID = 941030300625136176L;

  private List<OverviewTile> eaDataOverviewTiles;

  private List<OverviewTile> visualizationsOverviewTiles;

  public List<OverviewTile> getEaDataOverviewTiles() {
    return eaDataOverviewTiles;
  }

  public void setEaDataOverviewTiles(List<OverviewTile> eaDataOverviewTiles) {
    this.eaDataOverviewTiles = eaDataOverviewTiles;
  }

  public List<OverviewTile> getVisualizationsOverviewTiles() {
    return visualizationsOverviewTiles;
  }

  public void setVisualizationsOverviewTiles(List<OverviewTile> visualizationsOverviewTiles) {
    this.visualizationsOverviewTiles = visualizationsOverviewTiles;
  }

  @Override
  public String getIconCss() {
    return Dialog.START.getIconCssClass();
  }

}
