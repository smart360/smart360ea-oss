/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util;

import java.util.Calendar;

import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.ViewpointConfiguration;
import de.iteratec.iteraplan.common.UserContext;
import de.iteratec.iteraplan.elasticeam.model.Model;
import de.iteratec.iteraplan.model.RuntimePeriod;
import de.iteratec.visualizationmodel.ALabeledVisualizationObject;
import de.iteratec.visualizationmodel.Color;
import de.iteratec.visualizationmodel.CompositeSymbol;
import de.iteratec.visualizationmodel.Rectangle;
import de.iteratec.visualizationmodel.Text;


/**
 * Inner VBB creating a timeline symbol, i.e. a time axis.
 */
public class CreateTimelineSymbol {

  private static final float                                     UNIT_HEIGHT = 20f;
  private static final float                                     UNIT_WIDTH  = 15f;

  private CreateLabeledPlanarSymbol<ALabeledVisualizationObject> yearCreateSymbol;
  private CreateLabeledPlanarSymbol<ALabeledVisualizationObject> monthCreateSymbol;
  private CreateVisualizationObject<CompositeSymbol>             containerCreateSymbol;

  /**
   * Default constructor.
   */
  public CreateTimelineSymbol() {
    this.yearCreateSymbol = new CreateLabeledPlanarSymbol<ALabeledVisualizationObject>();
    this.yearCreateSymbol.setVObjectClass(Rectangle.class);
    this.yearCreateSymbol.setHeight(UNIT_HEIGHT);
    this.yearCreateSymbol.setFillColor(Color.WHITE);
    this.yearCreateSymbol.setBorderColor(Color.BLACK);

    this.monthCreateSymbol = new CreateLabeledPlanarSymbol<ALabeledVisualizationObject>();
    this.monthCreateSymbol.setVObjectClass(Rectangle.class);
    this.monthCreateSymbol.setHeight(UNIT_HEIGHT);
    this.monthCreateSymbol.setFillColor(Color.WHITE);
    this.monthCreateSymbol.setBorderColor(Color.BLACK);

    this.containerCreateSymbol = new CreateVisualizationObject<CompositeSymbol>();
    this.containerCreateSymbol.setVObjectClass(CompositeSymbol.class);
  }

  public CompositeSymbol transform(RuntimePeriod timespan, Model model, ViewpointConfiguration config) {
    CompositeSymbol container = this.containerCreateSymbol.transform(null, model, config);

    Calendar cal = Calendar.getInstance(UserContext.getCurrentLocale());
    cal.setTime(timespan.getStart());

    Calendar endCal = Calendar.getInstance();
    endCal.setTime(timespan.getEnd());

    int totalMonthCount = 0;
    while (cal.before(endCal)) {
      int year = cal.get(Calendar.YEAR);

      ALabeledVisualizationObject yearSymbol = this.yearCreateSymbol.transform(null, model, config);
      yearSymbol.setText(new Text(Integer.toString(year)));
      yearSymbol.setYpos(UNIT_HEIGHT / 2);
      yearSymbol.setFillColor(Color.WHITE);
      yearSymbol.setBorderColor(Color.BLACK);
      container.getChildren().add(yearSymbol);

      int monthCount = 0;
      for (; cal.get(Calendar.MONTH) <= cal.getActualMaximum(Calendar.MONTH) && cal.get(Calendar.YEAR) == year && cal.before(endCal); cal.add(
          Calendar.MONTH, 1), monthCount++, totalMonthCount++) {
        int month = cal.get(Calendar.MONTH) + 1; // Month starts from 0

        ALabeledVisualizationObject monthSymbol = this.monthCreateSymbol.transform(null, model, config);
        monthSymbol.setText(new Text(Integer.toString(month)));
        monthSymbol.setYpos(UNIT_HEIGHT + UNIT_HEIGHT / 2);
        monthSymbol.setXpos(UNIT_WIDTH * totalMonthCount + UNIT_WIDTH / 2);
        container.getChildren().add(monthSymbol);

        monthSymbol.setWidth(UNIT_WIDTH);
      }
      yearSymbol.setWidth(UNIT_WIDTH * monthCount + 1);
      yearSymbol.setXpos(UNIT_WIDTH * totalMonthCount - UNIT_WIDTH * monthCount / 2);
    }
    return container;
  }
}
