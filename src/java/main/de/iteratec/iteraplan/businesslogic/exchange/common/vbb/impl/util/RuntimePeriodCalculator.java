/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util;

import java.util.Date;

import de.iteratec.iteraplan.common.util.DateUtils;
import de.iteratec.iteraplan.model.RuntimePeriod;


/**
 * Helper used to determine the maximum timespan covered by a collection of runtimePeriods.
 */
public class RuntimePeriodCalculator {

  private Date startDate = new Date(Long.MAX_VALUE);
  private Date endDate   = new Date(Long.MIN_VALUE);

  /**
   * Adds the period to the collection of considered runtime periods.
   * @param period the runtime period to consider.
   */
  public void addRuntimePeriod(RuntimePeriod period) {
    if (period != null && period.getStart() != null) {
      this.startDate = DateUtils.earlier(this.startDate, period.getStart());
      this.endDate = DateUtils.later(this.endDate, period.getStart());
    }
    if (period != null && period.getEnd() != null) {
      this.endDate = DateUtils.later(this.endDate, period.getEnd());
      this.startDate = DateUtils.earlier(this.startDate, period.getEnd());
    }
  }

  /**
   * @return the runtime period covering the maximum timespan covered by the collected runtimePeriods.
   */
  public RuntimePeriod getGlobalRuntimePeriod() {
    return new RuntimePeriod(this.startDate, this.endDate);
  }
}
