/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl;

import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.InnerVBB;
import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.VBBUtil;
import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.ViewpointConfiguration;
import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util.AbstractVBB;
import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util.VisualVariableHelper.VisualVariable;
import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.legend.ColorLegend;
import de.iteratec.iteraplan.elasticeam.metamodel.PropertyExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.model.Model;
import de.iteratec.iteraplan.elasticeam.model.UniversalModelExpression;
import de.iteratec.visualizationmodel.APlanarSymbol;
import de.iteratec.visualizationmodel.Color;


abstract class ADecoratorBase<P extends PropertyExpression<?>> implements InnerVBB<APlanarSymbol> {

  public static final String                      ATTRIBUTE_COLOR = VBBUtil.PREFIX4OPTIONAL + "coloring";

  // decorated VBB
  private final InnerVBB<? extends APlanarSymbol> decoratedVbb;

  private ColorLegend                             colorLegend;

  private boolean                                 initialized     = false;

  //abstract viewmodel objects
  private P                                       colorAttribute;
  private UniversalTypeExpression                 decoratedClass;

  protected ADecoratorBase(InnerVBB<? extends APlanarSymbol> decoratedVbb) {
    this.decoratedVbb = decoratedVbb;
  }

  /**
   * @return the baseUrl used for creating links at symbols in the resulting visualization
   */
  @VisualVariable
  public final String getBaseUrl() {
    if (this.decoratedVbb instanceof AbstractVBB) {
      return ((AbstractVBB) this.decoratedVbb).getBaseUrl();
    }
    return null;
  }

  /**
   * Sets the baseUrl used for creating links at symbols in the resulting visualization.
   * @param baseUrl the baseUrl for links.
   */
  public final void setBaseUrl(String baseUrl) {
    if (this.decoratedVbb instanceof AbstractVBB) {
      ((AbstractVBB) this.decoratedVbb).setBaseUrl(baseUrl);
    }
  }

  /**
   * @return legend the legend
   */
  public ColorLegend getLegend() {
    return colorLegend;
  }

  /**{@inheritDoc}**/
  public final APlanarSymbol transform(UniversalModelExpression instance, Model model, ViewpointConfiguration config) {
    APlanarSymbol symbol = getDecoratedVBB().transform(instance, model, config);

    if (config.hasValueFor(getColorAttribute())) { // If optional colorAttribute is set
      if (!isInitialized()) {
        initialize(model, config);
        setInitialized(true);
      }

      if (symbol != null) {
        Color colorForObject = getColorForObject(instance, model, config);
        if (colorForObject != null) {
          symbol.setFillColor(colorForObject);
        }
      }
    }
    return symbol;
  }

  protected abstract Color getColorForObject(UniversalModelExpression instance, Model model, ViewpointConfiguration config);

  protected abstract void initialize(Model model, ViewpointConfiguration config);

  protected final InnerVBB<? extends APlanarSymbol> getDecoratedVBB() {
    return this.decoratedVbb;
  }

  protected final void setDecoratedClass(UniversalTypeExpression decoratedClass) {
    this.decoratedClass = decoratedClass;
  }

  protected final UniversalTypeExpression getDecoratedClass() {
    return this.decoratedClass;
  }

  protected final P getColorAttribute() {
    return this.colorAttribute;
  }

  protected final void setColorAttribute(P colorAttribute) {
    this.colorAttribute = colorAttribute;
  }

  protected final boolean isInitialized() {
    return this.initialized;
  }

  protected final void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }

  protected final void setColorLegend(ColorLegend colorLegend) {
    this.colorLegend = colorLegend;
  }

}
