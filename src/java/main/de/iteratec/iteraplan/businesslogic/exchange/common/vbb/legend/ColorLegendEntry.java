/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.iteraplan.businesslogic.exchange.common.vbb.legend;

import de.iteratec.visualizationmodel.APlanarSymbol;
import de.iteratec.visualizationmodel.Color;


/**
 * Class representing an entry in a legend
 */
public class ColorLegendEntry implements Comparable<ColorLegendEntry> {
  /** Symbol shown in the first column of the legend for this entry */
  private APlanarSymbol entrySymbol;
  /** Text shown next to the symbol explaining the meaning */
  private final String  entryLabel;
  /** property value this legend entry is based on */
  private final Color   propertyValue;

  public ColorLegendEntry(APlanarSymbol entrySymbol, String entryLabel, Color propertyValue) {
    this.entrySymbol = entrySymbol;
    this.entryLabel = entryLabel;
    this.propertyValue = propertyValue;
  }

  public APlanarSymbol getEntrySymbol() {
    return entrySymbol;
  }

  public void setEntrySymbol(APlanarSymbol entrySymbol) {
    this.entrySymbol = entrySymbol;
  }

  public String getEntryLabel() {
    return entryLabel;
  }

  public Object getPropertyValue() {
    return propertyValue;
  }

  /**{@inheritDoc}**/
  public int compareTo(ColorLegendEntry o) {
    try {
      return Double.valueOf(entryLabel).compareTo(Double.valueOf(o.getEntryLabel()));
    } catch (NumberFormatException nfe) {
      if ("null".equals(entryLabel)) {
        return -1;
      }
      else {
        return 1;
      }
    }
  }

  /**
   * {@inheritDoc}*
   * Auto generated hash method
   */
  @Override
  public int hashCode() {
    int prime = 31;
    int result = 1;
    result = prime * result + ((entryLabel == null) ? 0 : entryLabel.hashCode());
    result = prime * result + ((entrySymbol == null) ? 0 : entrySymbol.hashCode());
    result = prime * result + ((propertyValue == null) ? 0 : propertyValue.hashCode());
    return result;
  }

  /**
   * {@inheritDoc}*
   * 
   * Auto generated euqals method
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ColorLegendEntry other = (ColorLegendEntry) obj;
    if (entryLabel == null) {
      if (other.entryLabel != null) {
        return false;
      }
    }
    else if (!entryLabel.equals(other.entryLabel)) {
      return false;
    }
    if (entrySymbol == null) {
      if (other.entrySymbol != null) {
        return false;
      }
    }
    else if (!entrySymbol.equals(other.entrySymbol)) {
      return false;
    }
    if (propertyValue == null) {
      if (other.propertyValue != null) {
        return false;
      }
    }
    else if (!propertyValue.equals(other.propertyValue)) {
      return false;
    }
    return true;
  }

}
