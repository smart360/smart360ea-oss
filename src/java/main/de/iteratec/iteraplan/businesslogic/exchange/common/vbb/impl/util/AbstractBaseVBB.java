/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util;

import de.iteratec.iteraplan.businesslogic.exchange.common.vbb.impl.util.VisualVariableHelper.VisualVariable;


/**
 * Abstract base class for VBBs that describe the base map of a visualization.
 */
public abstract class AbstractBaseVBB extends AbstractVBB {
  private String title;

  /**
   * @return the title of the resulting visualization.
   */
  @VisualVariable
  public final String getTitle() {
    return title;
  }

  /**
   * Sets the title of the resulting visualization.
   * @param title the title of the resulting visualization.
   */
  public final void setTitle(String title) {
    this.title = title;
  }
}
