package de.iteratec.iteraplan.businesslogic.exchange.elasticmi.read;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import de.iteratec.iteraplan.businesslogic.exchange.common.ImportProcess.CheckPoint;
import de.iteratec.iteraplan.businesslogic.exchange.elasticmi.IteraplanMiLoadTaskFactory;
import de.iteratec.iteraplan.businesslogic.exchange.elasticmi.write.metamodel.AttributeTypeReader;
import de.iteratec.iteraplan.businesslogic.exchange.elasticmi.write.metamodel.impl.RMetamodelAttributeTypeReader;
import de.iteratec.iteraplan.businesslogic.service.AttributeTypeGroupService;
import de.iteratec.iteraplan.businesslogic.service.AttributeTypeService;
import de.iteratec.iteraplan.businesslogic.service.AttributeValueService;
import de.iteratec.iteraplan.businesslogic.service.BuildingBlockServiceLocator;
import de.iteratec.iteraplan.businesslogic.service.BuildingBlockTypeService;
import de.iteratec.iteraplan.common.Logger;
import de.iteratec.iteraplan.common.util.IteraplanProperties;
import de.iteratec.iteraplan.elasticmi.ElasticMiContext;
import de.iteratec.iteraplan.elasticmi.io.ExportInfo;
import de.iteratec.iteraplan.elasticmi.io.mapper.json.JsonModelMapper;
import de.iteratec.iteraplan.elasticmi.model.Model;
import de.iteratec.iteraplan.elasticmi.model.impl.ModelIdProvider;
import de.iteratec.iteraplan.presentation.dialog.ExcelImport.ImportStrategy;

public class LeanJsonImportProcess extends MiImportProcess {
	
	private static final Logger LOGGER = Logger.getIteraplanLogger(LeanJsonImportProcess.class);

	private ExportInfo exportInfo;
	private InputStream jsonIn;
	private JsonArray jsonArray;
	private Model model;
	
	protected LeanJsonImportProcess(
			BuildingBlockServiceLocator bbServiceLocator,
			AttributeValueService avService,
			AttributeTypeGroupService attributeTypeGroupService,
			AttributeTypeService attributeTypeService,
			BuildingBlockTypeService buildingBlockTypeService,
			ImportStrategy importStrategy,
			IteraplanMiLoadTaskFactory loadTaskFactory, InputStream stream) {
		super(bbServiceLocator, avService, attributeTypeGroupService,
				attributeTypeService, buildingBlockTypeService, importStrategy,
				loadTaskFactory);
			this.exportInfo = new ExportInfo();
			this.exportInfo.setVisibleExportTime(new Date());
			this.exportInfo.setHiddenExportTime(new Date());
			this.jsonIn = stream;
	}
	
	@Override
	protected Model getModelToImport() {
		if (model == null) {
			loadModelFromResource();
		}
		return model;
	}

	@Override
	protected void loadModelFromResource() {
		ModelIdProvider idProvider = ElasticMiContext.getCurrentContext().getContextModel().getIdProvider().clone();
		if (jsonArray == null) {
			jsonArray = readJsonFromInputStream(jsonIn);
		}
		JsonModelMapper mapper = new JsonModelMapper(getImportMetamodel(), "heidiho");
		this.model = mapper.read(jsonArray, idProvider);
	}

	@Override
	protected ExportInfo getExportInfo() {
		return exportInfo;
	}

	@Override
	protected AttributeTypeReader getAttributeTypeReader() {		
		return new RMetamodelAttributeTypeReader(getImportMetamodel(), new MiImportProcessMessages());
	}

	@Override
	public boolean importAndCheckFile() throws Exception {
		getCurrentCheckList().pending(CheckPoint.FILE_CHECK);
		jsonArray = readJsonFromInputStream(jsonIn);
		getCurrentCheckList().done(CheckPoint.FILE_CHECK);
		return true;
	}

	@Override
	public boolean isPartial() {
		return false;
	}

	@Override
	public String getFilteredTypeName() {
		return null;
	}

	@Override
	public String getFilteredTypePersistentName() {
		return null;
	}

	@Override
	public String getExtendedFilter() {
		return null;
	}

	@Override
	public String getExportTimestamp() {
		return exportInfo.getHiddenExportTime().toGMTString();
	}
	
	  private JsonArray readJsonFromInputStream(InputStream inputStream) {
	    if (inputStream == null) {
	    	LOGGER.error("Input stream must not be null");
	      return null;
	    }

	    try {
	      JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
	      return new JsonParser().parse(reader).getAsJsonArray();
	    } catch (UnsupportedEncodingException e) {
	      LOGGER.error("Error reading json", e);
	    } catch (IllegalStateException e) {
	      LOGGER.error("Error reading json", e);
	    }
	    return null;
	  }

}
