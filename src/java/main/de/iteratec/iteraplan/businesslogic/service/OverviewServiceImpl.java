/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.businesslogic.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import de.iteratec.iteraplan.common.Dialog;
import de.iteratec.iteraplan.common.UserContext;
import de.iteratec.iteraplan.common.UserContext.Permissions;
import de.iteratec.iteraplan.common.util.IteraplanProperties;
import de.iteratec.iteraplan.model.AbstractHierarchicalEntity;
import de.iteratec.iteraplan.model.InformationSystemRelease;
import de.iteratec.iteraplan.model.OverviewElementLists;
import de.iteratec.iteraplan.model.OverviewElementLists.ElementList;
import de.iteratec.iteraplan.model.OverviewTile;
import de.iteratec.iteraplan.model.TechnicalComponentRelease;
import de.iteratec.iteraplan.model.TypeOfBuildingBlock;
import de.iteratec.iteraplan.model.sorting.OrderedHierarchicalEntityCachingComparator;


/**
 * A default {@link DashboardService} implementation for calculating various properties and 
 * attributes, required to show dashboard graphics.
 */
public class OverviewServiceImpl implements OverviewService {

  private static final int            NUMBER_OF_TOP_NON_HIERARCHICAL_ELEMENTS = 5;

  private BuildingBlockServiceLocator buildingBlockServiceLocator;
  private DashboardService            dashboardService;

  /** {@inheritDoc} */
  public OverviewElementLists getElementLists() {
    OverviewElementLists overview = new OverviewElementLists();

    if (UserContext.getCurrentPerms().getUserHasFuncPermBP()) {
      BusinessProcessService businessProcessService = buildingBlockServiceLocator.getBpService();
      overview.addElementList(getRelevantBbsHierarchical(businessProcessService, TypeOfBuildingBlock.BUSINESSPROCESS, "businessprocess"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermPROD()) {
      ProductService productService = buildingBlockServiceLocator.getProductService();
      overview.addElementList(getRelevantBbsHierarchical(productService, TypeOfBuildingBlock.PRODUCT, "product"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermBU()) {
      BusinessUnitService businessUnitService = buildingBlockServiceLocator.getBuService();
      overview.addElementList(getRelevantBbsHierarchical(businessUnitService, TypeOfBuildingBlock.BUSINESSUNIT, "businessunit"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermBF()) {
      BusinessFunctionService businessFunctionService = buildingBlockServiceLocator.getBfService();
      overview.addElementList(getRelevantBbsHierarchical(businessFunctionService, TypeOfBuildingBlock.BUSINESSFUNCTION, "businessfunction"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermBO()) {
      BusinessObjectService businessObjectService = buildingBlockServiceLocator.getBoService();
      overview.addElementList(getRelevantBbsHierarchical(businessObjectService, TypeOfBuildingBlock.BUSINESSOBJECT, "businessobject"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermPROJ()) {
      ProjectService projectService = buildingBlockServiceLocator.getProjectService();
      overview.addElementList(getRelevantBbsHierarchical(projectService, TypeOfBuildingBlock.PROJECT, "project"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermIS()) {
      overview.addElementList(getRelevantISR());
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermBD()) {
      BusinessDomainService businessDomainService = buildingBlockServiceLocator.getBdService();
      overview.addElementList(getRelevantBbsHierarchical(businessDomainService, TypeOfBuildingBlock.BUSINESSDOMAIN, "businessdomain"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermISD()) {
      InformationSystemDomainService informationSystemDomainService = buildingBlockServiceLocator.getIsdService();
      overview.addElementList(getRelevantBbsHierarchical(informationSystemDomainService, TypeOfBuildingBlock.INFORMATIONSYSTEMDOMAIN,
          "informationsystemdomain"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermAD()) {
      ArchitecturalDomainService architecturalDomainService = buildingBlockServiceLocator.getAdService();
      overview.addElementList(getRelevantBbsHierarchical(architecturalDomainService, TypeOfBuildingBlock.ARCHITECTURALDOMAIN, "architecturaldomain"));
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermTC()) {
      overview.addElementList(getRelevantTCR());
    }

    if (UserContext.getCurrentPerms().getUserHasFuncPermIE()) {
      InfrastructureElementService infrastructureElementService = buildingBlockServiceLocator.getIeService();
      overview.addElementList(getRelevantBbsHierarchical(infrastructureElementService, TypeOfBuildingBlock.INFRASTRUCTUREELEMENT,
          "infrastructureelement"));
    }

    return overview;
  }

  private <T extends AbstractHierarchicalEntity<T>> ElementList<T> getRelevantBbsHierarchical(HierarchicalBuildingBlockService<T, Integer> service,
                                                                                              TypeOfBuildingBlock tobb, String htmlId) {

    List<T> elements = Lists.newArrayList(service.getFirstElement().getChildren());
    Collections.sort(elements, new OrderedHierarchicalEntityCachingComparator<T>());
    return new ElementList<T>(htmlId, tobb.getPluralValue(), Dialog.getDialogIconCssClassForDialogName(htmlId), service.loadElementList().size(),
        elements);
  }

  private ElementList<InformationSystemRelease> getRelevantISR() {
    List<InformationSystemRelease> list = Lists.newArrayList(buildingBlockServiceLocator.getIsrService().loadElementList());
    Map<InformationSystemRelease, Integer> sortedMap = dashboardService.getTopUsedIsr(list);
    List<InformationSystemRelease> result = Lists.newArrayList();
    int count = 0;
    for (InformationSystemRelease isr : sortedMap.keySet()) {
      if (count++ >= NUMBER_OF_TOP_NON_HIERARCHICAL_ELEMENTS) {
        break;
      }
      result.add(isr);
    }
    return new ElementList<InformationSystemRelease>("informationsystem", TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE.getPluralValue(),
        Dialog.INFORMATION_SYSTEM.getIconCssClass(), buildingBlockServiceLocator.getIsrService().loadElementList().size(), result);
  }

  private ElementList<TechnicalComponentRelease> getRelevantTCR() {
    List<TechnicalComponentRelease> list = Lists.newArrayList(buildingBlockServiceLocator.getTcrService().loadElementList());
    Map<TechnicalComponentRelease, Integer> sortedMap = dashboardService.getTopUsedTcr(list);
    List<TechnicalComponentRelease> result = Lists.newArrayList();
    int count = 0;
    for (TechnicalComponentRelease tcr : sortedMap.keySet()) {
      if (count++ >= NUMBER_OF_TOP_NON_HIERARCHICAL_ELEMENTS) {
        break;
      }
      result.add(tcr);
    }
    return new ElementList<TechnicalComponentRelease>("technicalcomponent", TypeOfBuildingBlock.TECHNICALCOMPONENTRELEASE.getPluralValue(),
        Dialog.TECHNICAL_COMPONENT.getIconCssClass(), buildingBlockServiceLocator.getTcrService().loadElementList().size(), result);
  }

  public void setDashboardService(DashboardService dashboardService) {
    this.dashboardService = dashboardService;
  }

  public void setBuildingBlockServiceLocator(BuildingBlockServiceLocator buildingBlockServiceLocator) {
    this.buildingBlockServiceLocator = buildingBlockServiceLocator;
  }

  private OverviewTile createEaDataTile(String dialogName, TypeOfBuildingBlock tobb, BuildingBlockService<?, ?> service, String contextPath) {
    String titleKey = tobb.getPluralValue();
    String url = contextPath + "/" + dialogName.toLowerCase() + "/init.do";
    String icon = Dialog.getDialogIconCssClassForDialogName(dialogName);
    int count = service.loadElementList().size();
    return new OverviewTile(titleKey, url, icon, count);
  }

  @Override
  public List<OverviewTile> getEaDataOverviewTiles(String contextPath) {
    List<OverviewTile> tiles = Lists.newArrayList();

    Permissions permissions = UserContext.getCurrentPerms();

    if (!permissions.getUserHasFuncPermOverview()) {
      return tiles;
    }

    if (permissions.getUserHasFuncPermPROJ()) {
      tiles.add(createEaDataTile(Dialog.PROJECT.getDialogName(), TypeOfBuildingBlock.PROJECT, buildingBlockServiceLocator.getProjectService(),
          contextPath));
    }
    if (permissions.getUserHasFuncPermBD()) {
      tiles.add(createEaDataTile(Dialog.BUSINESS_DOMAIN.getDialogName(), TypeOfBuildingBlock.BUSINESSDOMAIN,
          buildingBlockServiceLocator.getBdService(), contextPath));
    }
    if (permissions.getUserHasFuncPermBU()) {
      tiles.add(createEaDataTile(Dialog.BUSINESS_UNIT.getDialogName(), TypeOfBuildingBlock.BUSINESSUNIT, buildingBlockServiceLocator.getBuService(),
          contextPath));
    }
    if (permissions.getUserHasFuncPermPROD()) {
      tiles.add(createEaDataTile(Dialog.PRODUCT.getDialogName(), TypeOfBuildingBlock.PRODUCT, buildingBlockServiceLocator.getProductService(),
          contextPath));
    }
    if (permissions.getUserHasFuncPermBP()) {
      tiles.add(createEaDataTile(Dialog.BUSINESS_PROCESS.getDialogName(), TypeOfBuildingBlock.BUSINESSPROCESS,
          buildingBlockServiceLocator.getBpService(), contextPath));
    }
    if (permissions.getUserHasFuncPermBF()) {
      tiles.add(createEaDataTile(Dialog.BUSINESS_FUNCTION.getDialogName(), TypeOfBuildingBlock.BUSINESSFUNCTION,
          buildingBlockServiceLocator.getBfService(), contextPath));
    }
    if (permissions.getUserHasFuncPermBM()) {
      tiles.add(new OverviewTile("businessMapping.plural", contextPath + "/show/businessmapping", Dialog.BUSINESS_MAPPING.getIconCssClass(),
          buildingBlockServiceLocator.getBusinessMappingService().loadElementList().size()));
    }
    if (permissions.getUserHasFuncPermISD()) {
      tiles.add(createEaDataTile(Dialog.INFORMATION_SYSTEM_DOMAIN.getDialogName(), TypeOfBuildingBlock.INFORMATIONSYSTEMDOMAIN,
          buildingBlockServiceLocator.getIsdService(), contextPath));
    }
    if (permissions.getUserHasFuncPermIS()) {
      tiles.add(createEaDataTile(Dialog.INFORMATION_SYSTEM.getDialogName(), TypeOfBuildingBlock.INFORMATIONSYSTEMRELEASE,
          buildingBlockServiceLocator.getIsrService(), contextPath));
    }
    if (permissions.getUserHasFuncPermBO()) {
      tiles.add(createEaDataTile(Dialog.BUSINESS_OBJECT.getDialogName(), TypeOfBuildingBlock.BUSINESSOBJECT,
          buildingBlockServiceLocator.getBoService(), contextPath));
    }
    if (permissions.getUserHasFuncPermIE()) {
      tiles.add(createEaDataTile(Dialog.INFRASTRUCTURE_ELEMENT.getDialogName(), TypeOfBuildingBlock.INFRASTRUCTUREELEMENT,
          buildingBlockServiceLocator.getIeService(), contextPath));
    }
    if (permissions.getUserHasFuncPermAD()) {
      tiles.add(createEaDataTile(Dialog.ARCHITECHTURAL_DOMAIN.getDialogName(), TypeOfBuildingBlock.ARCHITECTURALDOMAIN,
          buildingBlockServiceLocator.getAdService(), contextPath));
    }
    if (permissions.getUserHasFuncPermTC()) {
      tiles.add(createEaDataTile(Dialog.TECHNICAL_COMPONENT.getDialogName(), TypeOfBuildingBlock.TECHNICALCOMPONENT,
          buildingBlockServiceLocator.getTcrService(), contextPath));
    }
    if (permissions.getUserHasFuncPermINT()) {
      tiles.add(createEaDataTile(Dialog.INTERFACE.getDialogName(), TypeOfBuildingBlock.INFORMATIONSYSTEMINTERFACE,
          buildingBlockServiceLocator.getIsiService(), contextPath));
    }

    return tiles;
  }

  @Override
  public List<OverviewTile> getVisualizationsOverviewTiles(String contextPath) {
    List<OverviewTile> tiles = Lists.newArrayList();
    Permissions permissions = UserContext.getCurrentPerms();

    if (!permissions.getUserHasFuncPermGraphReporting()) {
      return tiles;
    }

    // landscape
    tiles.add(new OverviewTile("graphicalExport.landscapeDiagram", contextPath + "/show/graphicalreporting/landscapediagram",
        Dialog.GRAPHICAL_REPORTING_LANDSCAPE.getIconCssClass()));

    //cluster
    tiles.add(new OverviewTile("graphicalExport.clusterDiagram", contextPath + "/show/graphicalreporting/clusterdiagram",
        Dialog.GRAPHICAL_REPORTING_CLUSTER.getIconCssClass()));

    //nesting cluster
    tiles.add(new OverviewTile("graphicalExport.vbbClusterDiagram", contextPath + "/show/graphicalreporting/vbbclusterdiagram",
        Dialog.GRAPHICAL_REPORTING_VBBCLUSTER.getIconCssClass()));

    //information flow
    tiles.add(new OverviewTile("graphicalExport.informationFlowDiagram", contextPath + "/show/graphicalreporting/informationflowdiagram",
        Dialog.GRAPHICAL_REPORTING_INFORMATIONFLOW.getIconCssClass()));

    //portfolio
    tiles.add(new OverviewTile("graphicalExport.portfolioDiagram", contextPath + "/show/graphicalreporting/portfoliodiagram",
        Dialog.GRAPHICAL_REPORTING_PORTFOLIO.getIconCssClass()));

    //masterplan
    tiles.add(new OverviewTile("graphicalExport.masterplanDiagram", contextPath + "/show/graphicalreporting/masterplandiagram",
        Dialog.GRAPHICAL_REPORTING_MASTERPLAN.getIconCssClass()));

    //dashboard
    tiles.add(new OverviewTile("global.dashboard", contextPath + "/dashboard/init.do", Dialog.DASHBOARD.getIconCssClass()));

    //bar or pie
    tiles.add(new OverviewTile("graphicalExport.pieBarDiagram", contextPath + "/show/graphicalreporting/piebardiagram",
        Dialog.GRAPHICAL_REPORTING_PIEBAR.getIconCssClass()));

    //composite
    tiles.add(new OverviewTile("graphicalExport.compositeDiagram", contextPath + "/show/graphicalreporting/compositediagram",
        Dialog.GRAPHICAL_REPORTING_COMPOSITE.getIconCssClass()));

    //spreadsheet  
    tiles.add(new OverviewTile("global.report.text", contextPath + "/show/tabularreporting", Dialog.TABULAR_REPORTING.getIconCssClass()));

    //all SQ
    tiles
        .add(new OverviewTile("global.report.graphical.savedqueries", contextPath + "/savedqueries/init.do", Dialog.SAVED_QUERIES.getIconCssClass()));

    //VIZ
    if (IteraplanProperties.getBooleanProperty(IteraplanProperties.SMART_VIZ_ENABLED)) {
      tiles.add(new OverviewTile("global.smartviz", IteraplanProperties.getProperties().getProperty(IteraplanProperties.SMART_VIZ_URL),
          "fa fa-line-chart", true));
    }

    return tiles;
  }
}
