/*
 * SMART360EA is a web-based EAM tool developed by SMART 360 CO
 * SMART360EA is Powered by iteraplan, developed by the iteratec GmbH
 * Copyright (C) 2015 SMART 360 CO
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY SMART 360, SMART 360 DISCLAIMS
 * THE WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact SMART 360 CO at email address info@smart360.biz.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "SMART EA" logo, "SMART 360" logo or the words "Powered by SMART360".
 */
package de.iteratec.iteraplan.businesslogic.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import de.iteratec.iteraplan.elasticeam.derived.QueryableModel;
import de.iteratec.iteraplan.elasticeam.iteraql2.IteraQl2Compiler;
import de.iteratec.iteraplan.elasticeam.iteraql2.IteraQl2Exception;
import de.iteratec.iteraplan.elasticeam.iteraql2.compile.CompileUtil;
import de.iteratec.iteraplan.elasticeam.iteraql2.compile.CompiledQuery;
import de.iteratec.iteraplan.elasticeam.iteraql2.result.QueryResult;
import de.iteratec.iteraplan.elasticeam.iteraql2.result.RelationshipEndResult;
import de.iteratec.iteraplan.elasticeam.iteraql2.result.UniversalTypeResult;
import de.iteratec.iteraplan.elasticeam.metamodel.Metamodel;
import de.iteratec.iteraplan.elasticeam.metamodel.RelationshipEndExpression;
import de.iteratec.iteraplan.elasticeam.metamodel.UniversalTypeExpression;
import de.iteratec.iteraplan.elasticeam.miadapter.MetamodelWrapper;
import de.iteratec.iteraplan.elasticeam.miadapter.ModelWrapper;
import de.iteratec.iteraplan.elasticeam.model.BindingSet;
import de.iteratec.iteraplan.elasticeam.model.Model;
import de.iteratec.iteraplan.elasticeam.model.UniversalModelExpression;
import de.iteratec.iteraplan.presentation.SpringGuiFactory;


public class ElasticeamMiAdapterServiceImpl implements ElasticeamService {

  @Autowired
  private ElasticMiService elasticMiService;

  @Override
  public void initOrReload() {
    // NOOP
  }

  @Override
  public MetamodelWrapper getMetamodel() {
    return new MetamodelWrapper(elasticMiService.getRMetamodel(), SpringGuiFactory.getInstance().getVbbClusterColors());
  }

  @Override
  public Model getModel() {
    return new QueryableModel(new ModelWrapper(getMetamodel(), elasticMiService.getModel()));
  }

  @Override
  public QueryResult executeQuery(String query) {
    return executeStringQuery(query, getMetamodel(), getModel());
  }

  @Override
  public QueryResult executeQuery(CompiledQuery query) {
    return executeCompiledQuery(query, getModel());
  }

  @Override
  public CompiledQuery compile(String queryString) {
    return IteraQl2Compiler.compile(getMetamodel(), queryString);
  }

  private static QueryResult executeStringQuery(String queryString, Metamodel mm, Model m) {
    return executeCompiledQuery(IteraQl2Compiler.compile(mm, queryString), m);
  }

  private static QueryResult executeCompiledQuery(CompiledQuery query, Model model) {
    QueryResult result = null;
    if (CompileUtil.isUniversalTypeCompilationUnit(query)) {
      UniversalTypeExpression type = CompileUtil.asUniversalTypeCompilationUnit(query).getCompilationResult();
      Collection<UniversalModelExpression> results = model.findAll(type);
      result = new UniversalTypeResult(query, type, results);
    }
    else if (CompileUtil.isRelationshipEndCompilationUnit(query)) {
      RelationshipEndExpression relEnd = CompileUtil.asRelationshipEndCompilationUnit(query).getCompilationResult();
      BindingSet results = model.findAll(relEnd);
      result = new RelationshipEndResult(query, results);
    }
    else {
      throw new IteraQl2Exception(IteraQl2Exception.UNKNOWN_RESULT_TYPE, "Unknown kind of compiled query: " + query + ".");
    }

    return result;
  }

  public void setElasticMiService(ElasticMiService elasticMiService) {
    this.elasticMiService = elasticMiService;
  }

}
