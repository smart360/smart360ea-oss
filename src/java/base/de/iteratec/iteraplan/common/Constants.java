/*
 * This file is part of "Smart360 EA".
 *
 * Smart360 EA is a lean IT Management web application developed by Smart360.
 * Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
 * Copyright (C) 2004-2014 iteratec GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "iteraplan" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by iteraplan".
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact iteratec GmbH headquarters at Inselkammerstr. 4
 * 82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 */
package de.iteratec.iteraplan.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;


public final class Constants {
  public static final String       DB_NU1L                                                  = "� nU1l �";
  public static final int          DB_ORACLE_MAX_IN                                         = 950;
  public static final Character    SQL_LIKE_OPERATOR_ESCAPE_CHAR                            = Character.valueOf('|');

  public static final String       NO_DIRECTION                                             = "noDirection";

  public static final String       FIRST_TO_SECOND                                          = "firstToSecond";

  public static final String       SECOND_TO_FIRST                                          = "secondToFirst";

  public static final String       BOTH_DIRECTIONS                                          = "bothDirections";

  public static final String       VERSIONSEP                                               = " # ";

  public static final String       HIERARCHYSEP                                             = " : ";

  public static final String       CONNECTIONSEP                                            = " <--> ";

  public static final String       VERSIONSEPFORCOPY                                        = " ";

  public static final String       TRANSPORTINFOSEP                                         = " ";

  public static final String       BUILDINGBLOCKSEP                                         = ", ";

  public static final String       AUDIT_LOG_UNKNOWN_NAME                                   = "unknown";

  public static final int          COMBOBOX_ENTRY_MAX_LENGTH                                = 30;

  public static final String       COMBOBOX_ENTRY_TAIL                                      = "...";

  public static final String       GUI_WILDCARD                                             = "*";

  public static final int          DAY_AS_MILLISECONDS                                      = 86400000;

  public static final int          PASSWORD_TIMESPAN_IN_DAYS                                = 90;

  public static final int          BUSINESS_MAPPING_TABLE_MAX_ROWS                          = 20;

  public static final int          BUSINESS_MAPPING_TABLE_MAX_COLUMNS                       = 10;

  public static final String       MASS_UPDATE_CACHED_REPORT_MEMBEAN                        = "cachedMassUpdateReportMemBean";

  public static final Integer      OPERATION_CONTAINS_ID                                    = Integer.valueOf(1);
  public static final Integer      OPERATION_CONTAINSNOT_ID                                 = Integer.valueOf(2);
  public static final Integer      OPERATION_STARTSWITH_ID                                  = Integer.valueOf(3);
  public static final Integer      OPERATION_ENDSWITH_ID                                    = Integer.valueOf(4);
  public static final Integer      OPERATION_EQUALS_ID                                      = Integer.valueOf(5);
  public static final Integer      OPERATION_EQUALSNOT_ID                                   = Integer.valueOf(6);
  public static final Integer      OPERATION_NOENTRIES_ID                                   = Integer.valueOf(7);
  public static final Integer      OPERATION_ANYENTRIES_ID                                  = Integer.valueOf(8);
  public static final Integer      OPERATION_GT_ID                                          = Integer.valueOf(9);
  public static final Integer      OPERATION_GEQ_ID                                         = Integer.valueOf(10);
  public static final Integer      OPERATION_EQ_ID                                          = Integer.valueOf(11);
  public static final Integer      OPERATION_LEQ_ID                                         = Integer.valueOf(12);
  public static final Integer      OPERATION_LT_ID                                          = Integer.valueOf(13);
  public static final Integer      OPERATION_ON_ID                                          = Integer.valueOf(14);
  public static final Integer      OPERATION_BEFORE_ID                                      = Integer.valueOf(15);
  public static final Integer      OPERATION_AFTER_ID                                       = Integer.valueOf(16);

  public static final int          MAX_RANGELIST_SIZE                                       = 4;

  public static final int          TEXT_SHORT                                               = 255;

  public static final int          TEXT_LONG                                                = 4000;

  public static final String       OPERATION_CONTAINS_NAME                                  = "reports.contains";

  public static final String       OPERATION_CONTAINSNOT_NAME                               = "reports.containsNot";

  public static final String       OPERATION_STARTSWITH_NAME                                = "reports.startsWith";

  public static final String       OPERATION_ENDSWITH_NAME                                  = "reports.endsWith";

  public static final String       OPERATION_EQUALS_NAME                                    = "reports.equals";

  public static final String       OPERATION_EQUALSNOT_NAME                                 = "reports.equalsNot";

  public static final String       OPERATION_NOENTRIES_NAME                                 = "reports.noEntry";

  public static final String       OPERATION_ANYENTRIES_NAME                                = "reports.anyEntry";

  public static final String       OPERATION_GT_NAME                                        = "reports.gt";

  public static final String       OPERATION_GEQ_NAME                                       = "reports.geq";

  public static final String       OPERATION_EQ_NAME                                        = "reports.eq";

  public static final String       OPERATION_LEQ_NAME                                       = "reports.leq";

  public static final String       OPERATION_LT_NAME                                        = "reports.lt";

  public static final String       OPERATION_ON_NAME                                        = "reports.on";
  public static final String       OPERATION_BEFORE_NAME                                    = "reports.before";
  public static final String       OPERATION_AFTER_NAME                                     = "reports.after";
  public static final String       BB_ARCHITECTURALDOMAIN                                   = "architecturalDomain.singular";
  public static final String       BB_ARCHITECTURALDOMAIN_PLURAL                            = "architecturalDomain.plural";
  public static final String       BB_ARCHITECTURALDOMAIN_BASE                              = "architecturalDomain.";
  public static final String       BB_ARCHITECTURALDOMAIN_INITIALCAP                        = "ArchitecturalDomain";
  public static final String       BB_BUSINESSDOMAIN                                        = "businessDomain.singular";
  public static final String       BB_BUSINESSDOMAIN_PLURAL                                 = "businessDomain.plural";
  public static final String       BB_BUSINESSDOMAIN_BASE                                   = "businessDomain.";
  public static final String       BB_BUSINESSDOMAIN_INITIALCAP                             = "BusinessDomain";
  public static final String       BB_BUSINESSFUNCTION                                      = "global.business_function";
  public static final String       BB_BUSINESSFUNCTION_PLURAL                               = "global.business_functions";
  public static final String       BB_BUSINESSFUNCTION_BASE                                 = "businessFunction.";
  public static final String       BB_BUSINESSFUNCTION_INITIALCAP                           = "BusinessFunction";
  public static final String       BB_BUSINESSMAPPING                                       = "businessMapping.singular";
  public static final String       BB_BUSINESSMAPPING_PLURAL                                = "businessMapping.plural";
  public static final String       BB_BUSINESSMAPPING_BASE                                  = "businessMapping.";
  public static final String       BB_BUSINESSMAPPING_INITIALCAP                            = "BusinessMapping";
  public static final String       BB_BUSINESSOBJECT                                        = "businessObject.singular";
  public static final String       BB_BUSINESSOBJECT_PLURAL                                 = "businessObject.plural";
  public static final String       BB_BUSINESSOBJECT_BASE                                   = "businessObject.";
  public static final String       BB_BUSINESSOBJECT_INITIALCAP                             = "BusinessObject";
  public static final String       BB_BUSINESSPROCESS                                       = "businessProcess.singular";
  public static final String       BB_BUSINESSPROCESS_PLURAL                                = "businessProcess.plural";
  public static final String       BB_BUSINESSPROCESS_BASE                                  = "businessProcess.";
  public static final String       BB_BUSINESSPROCESS_INITIALCAP                            = "BusinessProcess";
  public static final String       BB_BUSINESSUNIT                                          = "businessUnit.singular";
  public static final String       BB_BUSINESSUNIT_PLURAL                                   = "businessUnit.plural";
  public static final String       BB_BUSINESSUNIT_BASE                                     = "businessUnit.";
  public static final String       BB_BUSINESSUNIT_INITIALCAP                               = "BusinessUnit";
  public static final String       BB_INFORMATIONSYSTEMDOMAIN                               = "informationSystemDomain.singular";
  public static final String       BB_INFORMATIONSYSTEMDOMAIN_PLURAL                        = "informationSystemDomain.plural";
  public static final String       BB_INFORMATIONSYSTEMDOMAIN_BASE                          = "informationSystemDomain.";
  public static final String       BB_INFORMATIONSYSTEMDOMAIN_INITIALCAP                    = "InformationSystemDomain";
  public static final String       BB_INFORMATIONSYSTEMINTERFACE                            = "interface.singular";
  public static final String       BB_INFORMATIONSYSTEMINTERFACE_PLURAL                     = "interface.plural";
  public static final String       BB_INFORMATIONSYSTEMINTERFACE_BASE                       = "interface.";
  public static final String       BB_INFORMATIONSYSTEMINTERFACE_INITIALCAP                 = "InformationSystemInterface";
  public static final String       BB_INFORMATIONSYSTEMRELEASE                              = "informationSystemRelease.singular";
  public static final String       BB_INFORMATIONSYSTEMRELEASE_PLURAL                       = "informationSystemRelease.plural";
  public static final String       BB_INFORMATIONSYSTEMRELEASE_BASE                         = "informationSystemRelease.";
  public static final String       BB_INFORMATIONSYSTEMRELEASE_INITIALCAP                   = "InformationSystem";
  public static final String       BB_INFRASTRUCTUREELEMENT                                 = "infrastructureElement.singular";
  public static final String       BB_INFRASTRUCTUREELEMENT_PLURAL                          = "infrastructureElement.plural";
  public static final String       BB_INFRASTRUCTUREELEMENT_BASE                            = "infrastructureElement.";
  public static final String       BB_INFRASTRUCTUREELEMENT_INITIALCAP                      = "InfrastructureElement";
  public static final String       BB_PRODUCT                                               = "global.product";
  public static final String       BB_PRODUCT_PLURAL                                        = "global.products";
  public static final String       BB_PRODUCT_BASE                                          = "product.";
  public static final String       BB_PRODUCT_INITIALCAP                                    = "Product";
  public static final String       BB_PROJECT                                               = "project.singular";
  public static final String       BB_PROJECT_PLURAL                                        = "project.plural";
  public static final String       BB_PROJECT_BASE                                          = "project.";
  public static final String       BB_PROJECT_INITIALCAP                                    = "Project";
  public static final String       BB_TECHNICALCOMPONENTRELEASE                             = "technicalComponentRelease.singular";
  public static final String       BB_TECHNICALCOMPONENTRELEASE_PLURAL                      = "technicalComponentRelease.plural";
  public static final String       BB_TECHNICALCOMPONENTRELEASE_PLURAL2                     = "technicalComponentRelease.plural2";
  public static final String       BB_TECHNICALCOMPONENTRELEASE_BASE                        = "technicalComponentRelease.";
  public static final String       BB_TECHNICALCOMPONENTRELEASE_INITIALCAP                  = "TechnicalComponent";
  public static final String       ASSOC_TECHNICALCOMPONENTRELEASE_TO_INFRASTRUCTUREELEMENT = "technicalComponentRelease.association.infrastructureElement";
  public static final String       ASSOC_INFORMATIONSYSTEMRELEASE_TO_BUSINESSOBJECT         = "informationSystemRelease.association.businessObject";
  public static final String       ASSOC_SPECIALISATION                                     = "global.specialisation";
  public static final String       ASSOC_GENERALIZATION                                     = "global.generalization";
  public static final String       ASSOC_USES                                               = "global.uses";
  public static final String       ASSOC_USEDBY                                             = "global.usedBy";
  public static final String       ASSOC_PREDECESSORS                                       = "global.predecessors";
  public static final String       ASSOC_SUCCESSORS                                         = "global.successors";
  public static final String       BB_INTERFACE_INFORMATIONSYSTEMRELEASE_A                  = "interface.releaseA";
  public static final String       BB_INTERFACE_INFORMATIONSYSTEMRELEASE_B                  = "interface.releaseB";
  public static final String       DEFAULT_GRAPHICAL_EXOPORT_COLOR                          = "d3cfd1";
  public static final List<String> ALL_TYPES_FOR_DISPLAY                                    = Arrays.asList(new String[] {
      "architecturalDomain.plural", "global.business_functions", "businessObject.plural", "businessProcess.plural", "infrastructureElement.plural",
      "interface.plural", "businessDomain.plural", "informationSystemDomain.plural", "informationSystemRelease.plural", "project.plural",
      "businessUnit.plural", "global.products", "technicalComponentRelease.plural"         });

  public static final List<String> BUSINESSMAPPING_TYPES                                    = Arrays.asList(new String[] {
      "informationSystemRelease.plural", "businessUnit.plural", "global.products", "businessProcess.plural" });

  public static final int          LENGTH_LABEL_TRUNCATED_BUILDINGBLOCK                     = 22;

  public static final List<String> ALL_TYPES_WITH_TIMESPAN_FOR_DISPLAY                      = Collections.unmodifiableList(Arrays
                                                                                                .asList(new String[] {
      "informationSystemRelease.plural", "project.plural", "technicalComponentRelease.plural"  }));

  public static final String       ATTRIBUTE_BLANK                                          = "reports.selectAttribute";

  public static final String       ATTRIBUTE_ID                                             = "global.id";

  public static final String       ATTRIBUTE_NAME                                           = "global.name";

  public static final String       ATTRIBUTE_TRANSPORT                                      = "global.direction";

  public static final String       ATTRIBUTE_DESCRIPTION                                    = "global.description";

  public static final String       ATTRIBUTE_VERSION                                        = "global.version";

  public static final String       ATTRIBUTE_TYPEOFSTATUS                                   = "global.type_of_status";

  public static final String       ATTRIBUTE_STARTDATE                                      = "global.from";

  public static final String       ATTRIBUTE_ENDDATE                                        = "global.to";

  public static final String       ATTRIBUTE_LAST_USER                                      = "global.lastModificationUser";
  public static final String       ATTRIBUTE_LAST_MODIFICATION_DATE                         = "global.lastModificationTime";
  public static final String       ATTRIBUTE_HIERARCHICAL                                   = "global.hierarchical";
  public static final String       ATTRIBUTE_SUBSCRIBED_USERS                               = "global.subscribed.users";
  public static final String       SUBSCRIBED_USERS                                         = "global.subscribed.users";
  public static final String       EXTENSION_AD                                             = "reporting.extension.ArchitecturalDomain";
  public static final String       EXTENSION_BD                                             = "reporting.extension.BusinessDomain";
  public static final String       EXTENSION_BM                                             = "reporting.extension.BusinessMapping";
  public static final String       EXTENSION_BO                                             = "reporting.extension.BusinessObject";
  public static final String       EXTENSION_BP                                             = "reporting.extension.BusinessProcess";
  public static final String       EXTENSION_BU                                             = "reporting.extension.BusinessUnit";
  public static final String       EXTENSION_IE                                             = "reporting.extension.InfrastructureElement";
  public static final String       EXTENSION_IE_PARENT_KEY                                  = "reporting.extension.infrastructureElement.parent";
  public static final String       EXTENSION_IE_CHILDREN_KEY                                = "reporting.extension.infrastructureElement.children";
  public static final String       EXTENSION_IE_BASE_COMPONENTS                             = "reporting.extension.infrastructureElement.baseComponents";
  public static final String       EXTENSION_IE_PARENT_COMPONENTS                           = "reporting.extension.infrastructureElement.parentComponents";
  public static final String       EXTENSION_ISR                                            = "reporting.extension.InformationSystemRelease";
  public static final String       EXTENSION_ISR_ISI                                        = "reporting.extension.informationSystemRelease.interfaces";
  public static final String       EXTENSION_ISR_AD_VIA_TCR                                 = "reporting.extension.informationSystemRelease.ad.via.tcr";
  public static final String       EXTENSION_ISR_BO_VIA_ISI                                 = "reporting.extension.informationSystemRelease.bo.via.isi";
  public static final String       EXTENSION_ISR_ISR_VIA_ISI                                = "reporting.extension.informationSystemRelease.isr.via.isi";
  public static final String       EXTENSION_ISR_PARENT                                     = "reporting.extension.informationSystemRelease.parent";
  public static final String       EXTENSION_ISR_CHILDREN                                   = "reporting.extension.informationSystemRelease.children";
  public static final String       EXTENSION_ISR_PRED                                       = "reporting.extension.informationSystemRelease.predecessors";
  public static final String       EXTENSION_ISR_SUCC                                       = "reporting.extension.informationSystemRelease.successors";
  public static final String       EXTENSION_ISR_BASE_COMP                                  = "reporting.extension.informationSystemRelease.baseComponents";
  public static final String       EXTENSION_ISR_PARENT_COMP                                = "reporting.extension.informationSystemRelease.parentComponents";
  public static final String       EXTENSION_ISD                                            = "reporting.extension.InformationSystemDomain";
  public static final String       EXTENSION_ISI_BO                                         = "reporting.extension.interface.businessObjects";
  public static final String       EXTENSION_ISI_ISR                                        = "reporting.extension.interface.informationSystemReleases";
  public static final String       EXTENSION_PROJ                                           = "reporting.extension.Project";
  public static final String       EXTENSION_TCR                                            = "reporting.extension.TechnicalComponentRelease";
  public static final String       EXTENSION_TCR_ISI                                        = "reporting.extension.technicalComponentRelease.interfaces";
  public static final String       EXTENSION_TCR_PRED                                       = "reporting.extension.technicalComponentRelease.predecessors";
  public static final String       EXTENSION_TCR_SUCC                                       = "reporting.extension.technicalComponentRelease.successors";
  public static final String       EXTENSION_TCR_BASE                                       = "reporting.extension.technicalComponentRelease.baseComponents";
  public static final String       EXTENSION_TCR_PARENT                                     = "reporting.extension.technicalComponentRelease.parentComponents";
  public static final String       EXTENSION_BUSINESSFUNCTION                               = "reports.extension.businessfunction";
  public static final String       EXTENSION_PRODUCT                                        = "reports.extension.product";
  public static final String       EXTENSION_ISR2BOASSOCIATION                              = "reporting.extension.Isr2BoAssociation";
  public static final List<String> EXTENSIONS_SORTED                                        = Arrays.asList(new String[] {
      "reporting.extension.BusinessDomain", "reporting.extension.BusinessMapping", "reporting.extension.BusinessProcess",
      "reports.extension.businessfunction", "reports.extension.product", "reporting.extension.BusinessUnit", "reporting.extension.BusinessObject",
      "reporting.extension.InformationSystemDomain", "reporting.extension.InformationSystemRelease",
      "reporting.extension.informationSystemRelease.bo.via.isi", "reporting.extension.informationSystemRelease.children",
      "reporting.extension.informationSystemRelease.parent", "reporting.extension.informationSystemRelease.predecessors",
      "reporting.extension.informationSystemRelease.successors", "reporting.extension.informationSystemRelease.interfaces",
      "reporting.extension.informationSystemRelease.isr.via.isi", "reporting.extension.informationSystemRelease.baseComponents",
      "reporting.extension.informationSystemRelease.parentComponents", "reporting.extension.informationSystemRelease.ad.via.tcr",
      "reporting.extension.interface.businessObjects", "reporting.extension.interface.informationSystemReleases",
      "reporting.extension.ArchitecturalDomain", "reporting.extension.TechnicalComponentRelease",
      "reporting.extension.technicalComponentRelease.baseComponents", "reporting.extension.technicalComponentRelease.interfaces",
      "reporting.extension.technicalComponentRelease.parentComponents", "reporting.extension.technicalComponentRelease.predecessors",
      "reporting.extension.technicalComponentRelease.successors", "reporting.extension.InfrastructureElement",
      "reporting.extension.infrastructureElement.parent", "reporting.extension.infrastructureElement.children",
      "reporting.extension.infrastructureElement.baseComponents", "reporting.extension.infrastructureElement.parentComponents",
      "reporting.extension.Project"                                                        });

  public static final String       PERMISSION_QUERY_BUILDING_BLOCK_WRITE                    = "permission_query_building_block_write";

  public static final String       PERMISSION_QUERY_FUNCTIONAL_PERMISSIONS                  = "permission_query_functional_permissions";

  public static final String       REPORTS_EXPORT_HTML                                      = "reports_exportHTML";

  public static final String       REPORTS_EXPORT_EXCEL_2003                                = "reports_export_Excel2003";

  public static final String       REPORTS_EXPORT_EXCEL_2007                                = "reports_export_Excel2007";

  public static final String       REPORTS_EXPORT_CSV                                       = "reports_export_Csv";

  public static final String       REPORTS_EXPORT_GRAPHICAL_VISIO                           = "reports_export_Visio";

  public static final String       REPORTS_EXPORT_GRAPHICAL_SVG                             = "reports_export_SVG";

  public static final String       REPORTS_EXPORT_GRAPHICAL_JPEG                            = "reports_export_JPEG";

  public static final String       REPORTS_EXPORT_GRAPHICAL_PNG                             = "reports_export_PNG";

  public static final String       REPORTS_EXPORT_GRAPHICAL_PDF                             = "reports_export_PDF";

  public static final String       REPORTS_EXPORT_MSPROJECT_MSPDI                           = "reports_export_MsProject_MSPDI";

  public static final String       REPORTS_EXPORT_MSPROJECT_MSPDI_INCLUDING_SUBS            = "reports_export_MsProject_MSPDI_Subs";

  public static final String       REPORTS_EXPORT_MSPROJECT_MPX                             = "reports_export_MsProject_MPX";

  public static final String       REPORTS_EXPORT_MSPROJECT_MPX_INCLUDING_SUBS              = "reports_export_MsProject_MPX_Subs";

  public static final String       REPORTS_EXPORT_XMI                                       = "reports_export_XMI";

  public static final String       REPORTS_EXPORT_GRAPHICAL_INFORMATIONFLOW                 = "reports_export_graphical_InformationFlow";

  public static final String       REPORTS_EXPORT_GRAPHICAL_PORTFOLIO                       = "reports_export_graphical_Portfolio";

  public static final String       REPORTS_EXPORT_GRAPHICAL_CLUSTER                         = "reports_export_graphical_Cluster";

  public static final String       REPORTS_EXPORT_GRAPHICAL_MASTERPLAN                      = "reports_export_graphical_Masterplan";

  public static final String       REPORTS_EXPORT_GRAPHICAL_LANDSCAPE                       = "reports_export_graphical_Landscape";

  public static final String       REPORTS_EXPORT_GRAPHICAL_COMPOSITE                       = "reports_export_graphical_Composite";

  public static final String       REPORTS_EXPORT_GRAPHICAL_PIE                             = "reports_export_graphical_Pie";

  public static final String       REPORTS_EXPORT_GRAPHICAL_BAR                             = "reports_export_graphical_Bar";

  public static final String       REPORTS_EXPORT_GRAPHICAL_VBB_CLUSTER                     = "reports_export_graphical_VbbCluster";

  public static final String       REPORTS_EXPORT_GRAPHICAL_TIMELINE                        = "reports_export_graphical_Timeline";

  public static final String       REPORTS_EXPORT_GRAPHICAL_LINE                            = "reports_export_graphical_Line";

  public static final String       REPORTS_EXPORT_GRAPHICAL_MATRIX                          = "reports_export_graphical_Matrix";

  public static final String       REPORTS_EXPORT_GRAPHICAL_NEIGHBORHOOD                    = "reports_export_graphical_Neighborhood";

  public static final String       REPORTS_EXPORT_TABVIEW                                   = "reports_exportTabularReporting";

  public static final String       REPORTS_EXPORT_SELECT_RELATION                           = "reports_export_select_relation";

  public static final String       REPORTS_EXPORT_INFORMATIONFLOW_LAYOUT_STANDARD           = "reports_export_informationflow_layout_standard";

  public static final String       REPORTS_EXPORT_INFORMATIONFLOW_LAYOUT_KK                 = "reports_export_informationflow_layout_kk";

  public static final String       REPORTS_EXPORT_INFORMATIONFLOW_LAYOUT_CIRCLE             = "reports_export_informationflow_layout_circle";

  public static final String       REPORTS_EXPORT_CLUSTER_BB_SHAPE_RECTANGLE                = "graphicalExport.cluster.form.option.rectangle";

  public static final String       REPORTS_EXPORT_CLUSTER_BB_SHAPE_ROUNDED                  = "graphicalExport.cluster.form.option.rounded";
  public static final String       REPORTS_EXPORT_CLUSTER_BB_SHAPE_ARROW                    = "graphicalExport.cluster.form.option.arrow";
  public static final String       REPORTS_EXPORT_CLUSTER_ORIENTATION_HORIZONTAL            = "graphicalExport.cluster.direction.option.horizontal";
  public static final String       REPORTS_EXPORT_CLUSTER_ORIENTATION_VERTICAL              = "graphicalExport.cluster.direction.option.vertical";
  public static final String       REPORTS_EXPORT_CLUSTER_MODE_BB                           = "graphicalExport.cluster.mode.buildingBlocks";
  public static final String       REPORTS_EXPORT_CLUSTER_MODE_ATTRIBUTE                    = "graphicalExport.cluster.mode.attributes";
  public static final String       REPORTS_EXPORT_INFORMATION_FLOW                          = "graphicalExport.informationFlowDiagram";
  public static final String       SCHEMA_QUERY                                             = "/QueryXML.xsd";
  public static final String       SCHEMA_GRAPHICAL_LANDSCAPE                               = "/LandscapeDiagramXML.xsd";
  public static final String       SCHEMA_COMPOSITE_DIAGRAM                                 = "/CompositeDiagramXML.xsd";
  public static final String       TIMESPAN_PRODUCTIVE                                      = "global.productive";
  public static final String       TIMESPAN_PRODUCTIVE_FROM                                 = "global.productive_from";
  public static final String       TIMESPAN_PRODUCTIVE_TO                                   = "global.productive_to";
  public static final String       TIMESPAN_LIFETIME                                        = "global.lifeTime";
  public static final String       TIMESPAN_LIFETIME_FROM                                   = "global.lifeTime_from";
  public static final String       TIMESPAN_LIFETIME_TO                                     = "global.lifeTime_to";
  public static final String       POSTPROCESSINGSTRATEGY_ADD_INTERFACED_ISR                = "reporting.postprocessing.informationSystemRelease.addInterfacedReleases";
  public static final String       POSTPROCESSINGSTRATEGY_HIDE_CHILDREN                     = "reporting.postprocessing.informationSystemRelease.hideChildren";
  public static final String       POSTPROCESSINGSTRATEGY_HIDE_CHILDREN_MERGE               = "reporting.postprocessing.informationSystemRelease.hideChildrenAndMerge";
  public static final String       MU_NAME_VERSION                                          = "massUpdates.name_version";
  public static final String       MU_TIMESPAN                                              = "massUpdates.timespan";
  public static final String       MU_LIFETIME                                              = "global.lifeTime";
  public static final String       MU_TYPE_OF_STATUS                                        = "global.type_of_status";
  public static final String       MU_DESCRIPTION                                           = "global.description";
  public static final String       MU_AVAILABLE_FOR_CONNECTIONS                             = "global.available_for_interfaces";
  public static final String       MASTER_DATA_SOURCE                                       = "MASTER";
  public static final String       HIBERNATE_SEARCH_FILTER_VIRTUAL_ELEMENTS                 = "hibernate.search.filterVirtualElements";
  public static final String       JSP_ATTRIBUTE_EXCEPTION_MESSAGE                          = "_iteraplan_exception_message";
  public static final String       TEMPLATE_EXCEL_2003_EXTENDED_INFO                        = "templates.excel.2003.extended.info";
  public static final String       TEMPLATE_EXCEL_2007_EXTENDED_INFO                        = "templates.excel.2007.extended.info";
  public static final String       TEMPLATE_INFO_FLOW_EXTENDED_INFO                         = "templates.info.flow.extended.info";
  public static final String       GRAPHICAL_EXPORT_COMPOSITE_DIAGRAM_TITLE                 = "graphicalExport.compositeDiagram";
  public static final String       GRAPHICAL_EXPORT_LANDSCAPE_DIAGRAM_TITLE                 = "graphicalExport.landscapeDiagram";
  public static final String       GRAPHICAL_EXPORT_CLUSTER_DIAGRAM_TITLE                   = "graphicalExport.clusterDiagram";
  public static final String       GRAPHICAL_EXPORT_VBBCLUSTER_DIAGRAM_TITLE                = "graphicalExport.vbbClusterDiagram";
  public static final String       GRAPHICAL_EXPORT_INFORMATIONFLOW_DIAGRAM_TITLE           = "graphicalExport.informationFlowDiagram";
  public static final String       GRAPHICAL_EXPORT_PORTFOLIO_DIAGRAM_TITLE                 = "graphicalExport.portfolioDiagram";
  public static final String       GRAPHICAL_EXPORT_MASTERPLAN_DIAGRAM_TITLE                = "graphicalExport.masterplanDiagram";
  public static final String       GRAPHICAL_EXPORT_BAR_DIAGRAM_TITLE                       = "graphicalExport.barDiagram";
  public static final String       GRAPHICAL_EXPORT_PIE_DIAGRAM_TITLE                       = "graphicalExport.pieDiagram";
  public static final String       GRAPHICAL_EXPORT_TIMELINE_DIAGRAM_TITLE                  = "graphicalExport.timelineDiagram";
  public static final String       GRAPHICAL_EXPORT_LINE_DIAGRAM_TITLE                      = "graphicalExport.lineDiagram";
  public static final String       GRAPHICAL_EXPORT_CONTEXT_VISUALIZATION_TITLE             = "graphicalExport.contextVisualization";
  public static final List<String> LOCALES                                                  = ImmutableList.of("en", "de", "fr", "bg", "es");

  private Constants() {
  }
}
