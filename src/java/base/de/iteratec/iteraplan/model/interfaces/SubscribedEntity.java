package de.iteratec.iteraplan.model.interfaces;

import java.io.Serializable;

import de.iteratec.iteraplan.model.BuildingBlock;


public final class SubscribedEntity implements Serializable {

  private static final long   serialVersionUID = 8119074333100868704L;

  private final BuildingBlock buildingBlock;
  private final String        iconCssClass;

  public SubscribedEntity(BuildingBlock buildingBlock, String iconCssClass) {
    this.buildingBlock = buildingBlock;
    this.iconCssClass = iconCssClass;
  }

  public BuildingBlock getBuildingBlock() {
    return buildingBlock;
  }

  public String getIconCssClass() {
    return iconCssClass;
  }

}
