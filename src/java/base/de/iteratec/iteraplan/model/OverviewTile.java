package de.iteratec.iteraplan.model;

import java.io.Serializable;


public class OverviewTile implements Serializable {

  private static final long serialVersionUID = -4413198672475103163L;

  private final String      icon;
  private final String      titleKey;
  private final String      url;
  private String            count            = null;
  private final boolean     newPage;

  public OverviewTile(String titleKey, String url, String icon, int count) {
    this.titleKey = titleKey;
    this.url = url;
    this.icon = icon;
    this.count = String.valueOf(count);
    this.newPage = false;
  }

  public OverviewTile(String titleKey, String url, String icon) {
    this.titleKey = titleKey;
    this.url = url;
    this.icon = icon;
    this.newPage = false;
  }

  public OverviewTile(String titleKey, String url, String icon, boolean newPage) {
    this.titleKey = titleKey;
    this.url = url;
    this.icon = icon;
    this.newPage = newPage;
  }

  public String getIcon() {
    return icon;
  }

  public String getUrl() {
    return url;
  }

  public String getTitleKey() {
    return titleKey;
  }

  public String getCount() {
    return count;
  }

  public boolean isNewPage() {
    return newPage;
  }

}
