/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.visualizationmodel;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import org.apache.fop.render.RenderingContext;


public final class TextHelper {

  public static final FontRenderContext DEFAULT_FONT_RENDER_CONTEXT = new FontRenderContext(new AffineTransform(), false, false);

  private TextHelper() {
    // empty private constructor
  }

  /**
   * Helper method for determining text bounds
   * 
   * @param text the Text whose bounds are to be determined
   * @param ctx the {@link RenderingContext} to determine the actual text length
   * @return the {@link Text}s bounds
   */
  public static Rectangle2D getTextBounds(Text text, FontRenderContext ctx) {
    return getTextBounds(text.getText(), getFont(text), ctx);
  }

  /**
   * Helper method for determining text bounds
   * 
   * @param text The String whose bounds are to be determined
   * @param font the font used to calculate the bounds of the string
   * @param ctx The rendering context to determine the actual text length.
   * @return the bounds of the String rendered with the specified {@link Font}
   */
  public static Rectangle2D getTextBounds(String text, Font font, FontRenderContext ctx) {
    Rectangle2D stringBounds = font.getStringBounds(text, ctx);
    return new Rectangle2D.Double(stringBounds.getCenterX(), stringBounds.getCenterY(), stringBounds.getWidth() * 1.05 + 2, stringBounds.getHeight());
  }

  /**
   * Helper method for determining the font for a given text element.
   * 
   * @param t The text to be processed.
   * 
   * @return The corresponding font.
   */
  private static Font getFont(Text t) {
    Font f = Font.decode(t.getFontName());
    int result = 0;
    if (t.getTextStyle() != null) {
      result |= (t.getTextStyle().length > 0 && t.getTextStyle()[0]) ? Font.BOLD : 0;
      result |= (t.getTextStyle().length > 1 && t.getTextStyle()[1]) ? Font.ITALIC : 0;
    }
    f = f.deriveFont(result, t.getTextSize());
    return f;
  }
}
