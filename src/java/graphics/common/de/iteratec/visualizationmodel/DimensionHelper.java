/*
 * Copyright 2011-2014 Christian M. Schweda & iteratec
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.iteratec.visualizationmodel;

import java.awt.geom.Rectangle2D;
import java.util.Collection;


final class DimensionHelper {

  private DimensionHelper() {
    //Do not instantiate
  }

  static Rectangle2D.Float computeBoundingBox(Collection<ASymbol> symbols) {
    Rectangle2D.Float result = new Rectangle2D.Float();
    if (symbols == null || symbols.isEmpty()) {
      return result;
    }

    float xMin = Float.POSITIVE_INFINITY;
    float xMax = Float.NEGATIVE_INFINITY;
    float yMin = Float.POSITIVE_INFINITY;
    float yMax = Float.NEGATIVE_INFINITY;
    for (ASymbol child : symbols) {
      float childMinX = child.getXpos() - child.getWidth() / 2;
      if (childMinX < xMin) {
        xMin = childMinX;
      }

      float childMaxX = child.getXpos() + child.getWidth() / 2;
      if (childMaxX > xMax) {
        xMax = childMaxX;
      }

      float childMinY = child.getYpos() - child.getHeight() / 2;
      if (childMinY < yMin) {
        yMin = childMinY;
      }

      float childMaxY = child.getYpos() + child.getHeight() / 2;
      if (childMaxY > yMax) {
        yMax = childMaxY;
      }
    }
    result.setRect(xMin, yMin, xMax - xMin, yMax - yMin);

    return result;
  }
}
