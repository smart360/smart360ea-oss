<%-- 
	This file is part of "Smart360 EA".

	Smart360 EA is a lean IT Management web application developed by Smart360.
	Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
	Copyright (C) 2004-2014 iteratec GmbH

	This program is free software; you can redistribute it and/or modify it under
	the terms of the GNU Affero General Public License version 3 as published by
	the Free Software Foundation with the addition of the following permission
	added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
	details.

	The interactive user interfaces in modified source and object code versions
	of this program must display Appropriate Legal Notices, as required under
	Section 5 of the GNU Affero General Public License version 3.

	In accordance with Section 7(b) of the GNU Affero General Public License
	version 3, these Appropriate Legal Notices must retain the display of the
	"iteraplan" logo. If the display of the logo is not reasonably
	feasible for technical reasons, the Appropriate Legal Notices must display
	the words "Powered by iteraplan".

	You should have received a copy of the GNU Affero General Public License
	along with this program; if not, see http://www.gnu.org/licenses or write to
	the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301 USA.

	You can contact iteratec GmbH headquarters at Inselkammerstr. 4
	82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tags.iteratec.de/iteratec-tags" prefix="itera"%>

<c:if test="${not empty _iteraplan_exception_message}">
	<div class="alert alert-error">
		<div class="errorHeader">
		  <c:forEach var="error" items="${flowRequestContext.messageContext.allMessages}">
		    <span id="*.errors"><c:out value="${fn:replace(fn:replace(error.text, '<', '&lt;'), '>', '&gt;')}" escapeXml="false" /></span>
		  </c:forEach>
		  <%-- insert an exception message provided by one of our exception handlers --%>
		  <c:if test="${not empty _iteraplan_exception_message}">
		    <span id="*.errors"><c:out value="${fn:replace(fn:replace(_iteraplan_exception_message, '<', '&lt;'), '>', '&gt;')}" escapeXml="false"/></span>
		  </c:if>
		</div>
	</div>
</c:if>

<c:set var="functionalPermission" value="${userContext.perms.userHasFuncPermSearch}" scope="request" />
<br />
<%-- Displays the search results --%>
<c:choose>
	<c:when test="${dialogMemory.searchDTO != null && dialogMemory.numberOfResults > 0 && functionalPermission}">
		<div id="SearchResultsContainer" class="row-fluid module">
			<div class="module-heading">
			    <fmt:message key="search.results" /> "<c:out value="${dialogMemory.searchField}" />"
				<div style="float: right; margin-right: 10px; font-weight: normal;">
					<fmt:message key="search.resultsFromTO" /> 
					<c:out value=" ${dialogMemory.numberOfResults} " /> 
					<fmt:message key="search.resultsOfAbout" /> 
					<c:out value=" ${dialogMemory.numberOfResults}" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="module-body-table">
					<div class="row-fluid">
						<table class="table table-striped table-condensed tableInModule">
							<thead>
								<tr>
									<th><fmt:message key="search.resultsTitleNameVersion" /></th>
									<th><fmt:message key="global.description" /></th>
									<th><fmt:message key="manageSearch.foundIn" /></th>
								</tr>
							</thead>
							<c:set var="linkJavaScript" value="" />
							<c:set var="linkAHref" value="" />
							<c:set var="linkStyle" value="link" />
							<tbody>
								<c:forEach items="${dialogMemory.searchDTO.availableBBE}" var="bbe">
									<tr>
								    	<th colspan="3"><fmt:message key="${bbe}" /></th>
									</tr>
									<c:forEach items="${dialogMemory.searchDTO.searchMap[bbe]}" var="container">
					
										<c:set var="linkJavaScript">
											<itera:linkToElement name="container" type="js"/>
										</c:set>
										
										<c:set var="linkAHref">
											<itera:linkToElement name="container" type="html"/>
										</c:set>
										
										<tr>
											<td class="<c:out value="${linkStyle}" />" onclick="<c:out value="${linkJavaScript}" />" >					
												<itera:htmlLinkToElement link="${linkAHref}" isLinked="true">
													<c:out value="${container.name}" /> <c:out value="${container.version}" />
												</itera:htmlLinkToElement>						
											</td>
											<td class="<c:out value="${linkStyle}" />" onclick="<c:out value="${linkJavaScript}" />" >
												<itera:htmlLinkToElement link="${linkAHref}" isLinked="true">
													<itera:write name="container" property="description" plainText="true" escapeXml="true" />
												</itera:htmlLinkToElement>
											</td>
											<td>
												<%-- Output found in as list. Might be decorated by list-styles 'decimal', 'lower-roman' etc.  --%>
												<ul class="compact" style="list-style-type: none;">
													<c:forEach items="${container.foundIn}" var="foundIn">
														<li><c:out value="${foundIn}" escapeXml="false" /></li>
													</c:forEach>
												</ul>
											</td>
										</tr>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</c:when>

	<c:otherwise>
		<c:choose>
			<c:when test="${functionalPermission == true}">
				<p><fmt:message key="search.results.resultListIsEmtpy" /></p>
				
				<c:if test="${dialogMemory.searchDTO.numberOfAlternativeResults != null && dialogMemory.searchDTO.numberOfAlternativeResults != 0}">
					<c:set var="alternativeQueryWithLink">
					<a href="<c:url value="/search/globalsearch.do?q=" /><c:out value="${dialogMemory.searchDTO.alternativeQueryString}" />">
						<c:out value="${dialogMemory.searchDTO.alternativeQueryString}" />
					</a>
					</c:set>
					
					<fmt:message key="search.suggestion">
						<fmt:param value="${alternativeQueryWithLink}"/>
						<fmt:param value="${dialogMemory.searchDTO.numberOfAlternativeResults}" />
					</fmt:message>
				</c:if>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>