<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tags.iteratec.de/iteratec-tags" prefix="itera"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
	/* <![CDATA[ */
	#mmModal { width: auto; height: auto;}
	#mmModalBody { width: auto; height: auto; max-height: 800px;}
	/* ]]> */
</style>

<div id="startpage" class="clearfix">
	<div class="span6" id="eadata">
		<p class="startpage-tilegroup-heading"><fmt:message key="startview.tilegroup.elements" />&nbsp;(<a data-toggle="modal" data-target="#mmModal" href="#"><fmt:message key="startscreen.seemetamodel" />)</a></p>
		<ul class="clearfix">
			<c:forEach items="${dialogMemory.eaDataOverviewTiles}" var="eaDataTile" varStatus="status">
				<tiles:insertTemplate template="/jsp/OverviewTile.jsp">
					<tiles:putAttribute name="tile" value="${eaDataTile}" />
				</tiles:insertTemplate>
			</c:forEach>
		</ul>
	</div>
	<div class="span6" id="reports">
		<p class="startpage-tilegroup-heading"><fmt:message key="startview.tilegroup.visualizations" /></p>
		<ul class="clearfix">
			<c:forEach items="${dialogMemory.visualizationsOverviewTiles}" var="vizTile" varStatus="status">
				<tiles:insertTemplate template="/jsp/OverviewTile.jsp">
					<tiles:putAttribute name="tile" value="${vizTile}" />
				</tiles:insertTemplate>
			</c:forEach>
		</ul>
	</div>
</div>

<!-- Modal for the metamdoel -->
<div id="mmModal" class="modal hide">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><fmt:message key="startscreen.metamodel" /></h4>
      </div>
      <div id="mmModalBody" class="modal-body">
        <tiles:insertTemplate template="/jsp/StartContent.jsp" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="button.close" /></button>
      </div>
    </div>

  </div>
</div>