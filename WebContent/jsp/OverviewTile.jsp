<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tags.iteratec.de/iteratec-tags" prefix="itera"%>

<tiles:useAttribute name="tile"/>

<%-- TODO: target="_tab" on tile.newPage --%>

<li>
		<c:choose>
			<c:when test="${tile.newPage}">
				<a class="startpage-tile" href="${tile.url}" target="_tab" >
			</c:when>
			<c:otherwise>
				<a class="startpage-tile" href="${tile.url}" >
			</c:otherwise>
		</c:choose>
		<i class="${tile.icon} startpage-tile-icon"></i>
		<c:choose>
			<c:when test="${not empty  tile.count}">
				<span class="startpage-tile-span"><c:out value="${tile.count}" />&nbsp;<fmt:message key="${tile.titleKey}" /></span>
			</c:when>
			<c:otherwise>
				<span class="startpage-tile-span"><fmt:message key="${tile.titleKey}" /></span>
			</c:otherwise>
		</c:choose>
	</a>
</li>