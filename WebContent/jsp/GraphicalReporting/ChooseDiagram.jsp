<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tags.iteratec.de/iteratec-tags" prefix="itera"%>

<c:set var="functionalPermission">
	<c:set var="GraphicalReporting" value="GraphicalReporting" />
	<itera:write name="userContext" property="perms.userHasDialogPermission(${GraphicalReporting})" escapeXml="false" />
</c:set>

<c:choose>
	<c:when test="${functionalPermission == true}">
		
		<div id="startpage" class="clearfix">
			<div class="span12" id="reports">
				<p class="startpage-tilegroup-heading"><fmt:message key="startview.tilegroup.visualizations" /></p>
				<ul class="clearfix">
					<c:forEach items="${memBean.vizOverviewTiles}" var="vizTile" varStatus="status">
						<tiles:insertTemplate template="/jsp/OverviewTile.jsp">
							<tiles:putAttribute name="tile" value="${vizTile}" />
						</tiles:insertTemplate>
					</c:forEach>
				</ul>
			</div>
		</div>
		
		<div class="module-body">
			<br></br>
			<p>			
				<a  
					href="<c:url value="/visio/vbaCert.cer" />"
					class="link btn btn-primary" >
					<i class="icon-download-alt icon-white"></i>
 					<fmt:message key="graphicalExport.visioCertificate" />
				</a> 
			</p>
		</div>
	</c:when>
	<c:otherwise>
		<tiles:insertTemplate template="/jsp/common/AccessDenied.jsp" />
	</c:otherwise>
</c:choose>