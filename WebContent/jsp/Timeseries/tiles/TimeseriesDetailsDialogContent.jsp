<%-- 
	This file is part of "Smart360 EA".

	Smart360 EA is a lean IT Management web application developed by Smart360.
	Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
	Copyright (C) 2004-2014 iteratec GmbH

	This program is free software; you can redistribute it and/or modify it under
	the terms of the GNU Affero General Public License version 3 as published by
	the Free Software Foundation with the addition of the following permission
	added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
	details.

	The interactive user interfaces in modified source and object code versions
	of this program must display Appropriate Legal Notices, as required under
	Section 5 of the GNU Affero General Public License version 3.

	In accordance with Section 7(b) of the GNU Affero General Public License
	version 3, these Appropriate Legal Notices must retain the display of the
	"iteraplan" logo. If the display of the logo is not reasonably
	feasible for technical reasons, the Appropriate Legal Notices must display
	the words "Powered by iteraplan".

	You should have received a copy of the GNU Affero General Public License
	along with this program; if not, see http://www.gnu.org/licenses or write to
	the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301 USA.

	You can contact iteratec GmbH headquarters at Inselkammerstr. 4
	82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 --%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script>
	$(document).ready(function() {
		$('#timeseriesDialog').on('hidden', function() {
			closeTimeseriesDialogAndUpdate('${memBean.currentTimeseriesComponentModel.componentMode}');
		});
	});
</script>
		
<div class="modal-header">
	<h4>${memBean.currentTimeseriesComponentModel.attributeName}</h4>
</div>
<div class="modal-body">
	<c:if test="${memBean.currentTimeseriesAttributeId != null}">
	
	<c:if test="${not empty memBean.currentTimeseriesComponentModel.errorMessages}">
		<div class="alert alert-error">
			<ul>
			<c:forEach items="${memBean.currentTimeseriesComponentModel.errorMessages}" var="message">
				<li><c:out value="${fn:replace(fn:replace(message, '<', '&lt;'), '>', '&gt;')}" escapeXml="false" /></li>
			</c:forEach>
			</ul>
		</div>
	</c:if>

	<table class="table table-condensed table-striped">
		<tr>
			<th></th>
			<th>
				<fmt:message key="global.date"/>
			</th>
			<th>
				<fmt:message key="global.attributevalue"/>
			</th>
		</tr>
		<c:if test="${memBean.currentTimeseriesComponentModel.componentMode != 'READ'}">
			<tiles:insertTemplate template="TimeseriesEntryRow.jsp">
				<tiles:putAttribute name="timeseriesEntryCMPath" value="currentTimeseriesComponentModel.newEntryComponentModel"/>
				<tiles:putAttribute name="rowIndex" value=""/>
			</tiles:insertTemplate>
		</c:if>
		<c:forEach items="${memBean.currentTimeseriesComponentModel.entryComponentModels}" var="entryCM" varStatus="status">
			<tiles:insertTemplate template="TimeseriesEntryRow.jsp">
				<tiles:putAttribute name="timeseriesEntryCMPath" value="currentTimeseriesComponentModel.entryComponentModels[${status.index}]"/>
				<tiles:putAttribute name="rowIndex" value="${status.index}"/>
			</tiles:insertTemplate>
		</c:forEach>
		<c:if test="${empty memBean.currentTimeseriesComponentModel.entryComponentModels && memBean.currentTimeseriesComponentModel.componentMode == 'READ'}">
			<tr>
				<td></td>
				<td><fmt:message key="attribute.novalue"/></td>
				<td><fmt:message key="attribute.novalue"/></td>
			</tr>
		</c:if>
	</table>
	
	</c:if>
</div>
<div class="modal-footer">
	<a class="btn btn-primary" data-dismiss="modal">OK</a>
</div>