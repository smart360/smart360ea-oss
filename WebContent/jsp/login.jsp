<%-- 
	This file is part of "Smart360 EA".

	Smart360 EA is a lean IT Management web application developed by Smart360.
	Smart360 EA is based on and powered by iteraplan (an IT Governance web application) developed by iteratec GmbH.
	Copyright (C) 2004-2014 iteratec GmbH

	This program is free software; you can redistribute it and/or modify it under
	the terms of the GNU Affero General Public License version 3 as published by
	the Free Software Foundation with the addition of the following permission
	added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY ITERATEC, ITERATEC DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS. FOR ANY PART OF THE COVERED
	WORK IN WHICH THE COPYRIGHT IS OWNED BY Smart360, Smart360 DISCLAIMS THE
	WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
	details.

	The interactive user interfaces in modified source and object code versions
	of this program must display Appropriate Legal Notices, as required under
	Section 5 of the GNU Affero General Public License version 3.

	In accordance with Section 7(b) of the GNU Affero General Public License
	version 3, these Appropriate Legal Notices must retain the display of the
	"iteraplan" logo. If the display of the logo is not reasonably
	feasible for technical reasons, the Appropriate Legal Notices must display
	the words "Powered by iteraplan".

	You should have received a copy of the GNU Affero General Public License
	along with this program; if not, see http://www.gnu.org/licenses or write to
	the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301 USA.

	You can contact iteratec GmbH headquarters at Inselkammerstr. 4
	82008 Munich - Unterhaching, Germany, or at email address info@iteratec.de.
 --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%
  // effectively disable browser caching, enforce complete roundtrip on every reload 
  response.setHeader("Expires", "Tue, 15 Nov 1994 12:45:26 GMT");
  response.setHeader("Last-Modified", "Tue, 15 Nov 1994 12:45:26 GMT");
  response.setHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  response.addHeader("Cache-Control", "post-check=0, pre-check=0");
%>

<spring:eval var="buildVersion" expression="@applicationProperties.getProperty('build.version')" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="refresh" content="<%=session.getMaxInactiveInterval()-10 %>" />
        <meta http-equiv="expires" content="0" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="X-UA-Compatible" content="IE=8,9,10" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
		<link rel="icon"          href="<c:url value="/images/favicon.ico"/>" type="image/x-icon" />
		<link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon" />
		<link href="<c:url value="/ui/login.cxx" />" media="screen" type="text/css" rel="stylesheet" />
		
		<title>
		  <fmt:message key="global.madeby" /> - <fmt:message key="global.applicationname" />
		</title>
	</head>

<body onload="document.getElementById('login_form').j_username.focus();">

	<div id="outerbox">
	 <div id="innerbox">
	   <div id="logo">
	     <img id="iteraplan_header_image" border="0" src="<c:url value="/images/blank.gif"/>" alt="iteraplan Logo"/>
	   </div>
	   <h1><fmt:message key="global.applicationname" />
	        <c:out value=" " />
	   		${buildVersion}
	   </h1>
	   <form method="post" action="<c:url value="/j_iteraplan_security_check"/>" id="login_form">
	     <table class="table">
	       <tr>
	         <td class="label"><label for="j_username">
	           	<fmt:message key="login.name" /> :</label>
	         </td>
	         <td><input class="input" type="text" name="j_username" id="j_username" /></td>
	       </tr>       
	       <tr>
	         <td class="label">
	         	<label for="j_password"><fmt:message key="global.password" /> :</label>
	         </td>
	         <td><input class="input" type="password" name="j_password" id="j_password" /></td>
	       </tr>
	     </table>
	     <div id="buttons">
	       <input type="submit" class="btn btn-primary" id="loginButtonCursor" name="login_button" value="<fmt:message key="login.submit" />" />
	     </div>
	   </form>
	   <c:if test="${not empty param.errorKey}">
		   <c:choose>
		     <c:when test="${'login.error.badCredentials' eq param.errorKey}">
		     	<p class="errorMsg"><fmt:message key="login.error.badCredentials"/></p>
		     </c:when>
		     <c:when test="${'login.error.datasourceNotAvailable' eq param.errorKey}">
		     	<p class="errorMsg"><fmt:message key="login.error.datasourceNotAvailable"/></p>
		     </c:when>
		     
		     <c:when test="${'login.error.passwordExpired' eq param.errorKey}">
		     	<p class="errorMsg"><fmt:message key="login.error.passwordExpired"/></p>
		     </c:when>
		     
		     <c:when test="${'login.error' eq param.errorKey}">
		     	<p class="errorMsg"><fmt:message key="login.error"/></p>
		     </c:when>	 
		     <c:otherwise>
		         <p class="errorMsg">Invalid Error code</p>
		     </c:otherwise>  
		   </c:choose>	   
	   </c:if>
	  
	   <div id="links">
	     <table class="table">
	       <tr>
	  		 <td align="left">
	  		   <div id="iteratecLink" class = "Link">
	  			
	  		     <a href="<fmt:message key ="login.link.iteratec.url" />">
	   					<fmt:message key ="login.link.iteratec.text" />
	  			 </a>
	  		   </div>
			 </td>
	  		 <td align="right">
	  		   <div id="iteraplanLink" class="Link">  				
	  			<a href="<fmt:message key ="login.link.iteraplan.url" />">
				  		<fmt:message key ="login.link.iteraplan.text" />
	  		    </a>
	  		  </div>
	  		</td>
	   	  </tr>
	     </table>
	   </div>
     <!-- innerbox -->
     </div>
    </div>
</body>
</html>