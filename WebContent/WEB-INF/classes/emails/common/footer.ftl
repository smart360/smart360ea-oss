User: ${user}

Time: ${time?datetime?string.medium}

-- 
<#if ilink??>
Element URL: <#if link??><#import "link.ftl" as l><@l.createLink url=link/></#if>
Smart360 EA <#if applicationLink??><#import "link.ftl" as l><@l.createLink url=applicationLink/></#if>
</#if>
Smart360 EA